#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "libtokamak-client.h"

struct tokamak_client_s
{
  char *cookiejar;
  char *url_api_0_player_new;
  char *url_api_0_player_games;
  char *url_api_0_game_new;
  char *url_api_0_game_join;
  char *url_api_0_game_join_ai;
  char *url_api_0_game_get;
  char *url_api_0_game_post;
};
typedef struct tokamak_client_s tokamak_client;
typedef void (*tokamak_client_player_games_t)(void *, const char *);

static char *strdupcat(const char *a, const char *b)
{
  size_t bytes = strlen(a) + strlen(b) + 1;
  char *c = malloc(bytes);
  if (! c) return 0;
  *c = 0;
  strncat(c, a, bytes);
  strncat(c, b, bytes);
  return c;
}

struct memory
{
  char *response;
  size_t size;
};

static size_t memorycb(void *data, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct memory *mem = userp;
  char *ptr = realloc(mem->response, mem->size + realsize + 1);
  if (! ptr)
  {
    return 0;
  }
  mem->response = ptr;
  memcpy(&(mem->response[mem->size]), data, realsize);
  mem->size += realsize;
  mem->response[mem->size] = 0;
  return realsize;
}

tokamak_client *tokamak_client_new(const char *server, const char *cookiejar)
{
  tokamak_client *client = calloc(1, sizeof(*client));
  client->cookiejar = strdup(cookiejar);
  client->url_api_0_player_new = strdupcat(server, "/api/0/player/new");
  client->url_api_0_player_games = strdupcat(server, "/api/0/player/games");
  client->url_api_0_game_new = strdupcat(server, "/api/0/game/new");
  client->url_api_0_game_join = strdupcat(server, "/api/0/game/join");
  client->url_api_0_game_join_ai = strdupcat(server, "/api/0/game/join_ai");
  client->url_api_0_game_get = strdupcat(server, "/api/0/game?game=");
  client->url_api_0_game_post = strdupcat(server, "/api/0/game");
  return client;
}

void tokamak_client_delete(tokamak_client *client)
{
  free(client->cookiejar);
  free(client->url_api_0_player_new);
  free(client->url_api_0_player_games);
  free(client->url_api_0_game_new);
  free(client->url_api_0_game_join);
  free(client->url_api_0_game_join_ai);
  free(client->url_api_0_game_get);
  free(client->url_api_0_game_post);
  free(client);
}

bool tokamak_client_player_new(tokamak_client *client, const char *name)
{
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, client->url_api_0_player_new);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, client->cookiejar); 
  curl_easy_setopt(curl, CURLOPT_COOKIEJAR, client->cookiejar); 
  curl_mime *multipart = curl_mime_init(curl);
  curl_mimepart *part = curl_mime_addpart(multipart);
  curl_mime_name(part, "name");
  curl_mime_data(part, name, CURL_ZERO_TERMINATED);
  curl_easy_setopt(curl, CURLOPT_MIMEPOST, multipart);
  curl_easy_perform(curl);
  long status = 0;
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status);
  curl_mime_free(multipart);
  curl_easy_cleanup(curl);
  return status == 200;
}

bool tokamak_client_player_games(tokamak_client *client, tokamak_client_player_games_t callback, void *cbdata)
{
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, client->url_api_0_player_games);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, client->cookiejar);
  curl_easy_setopt(curl, CURLOPT_COOKIEJAR, client->cookiejar);
  struct memory mem = {0};
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, memorycb);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &mem);
  curl_easy_perform(curl);
  long status = 0;
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status);
  curl_easy_cleanup(curl);
  if (status == 200)
  {
    char *str = mem.response;
    char *eol = strchr(str, '\r');
    while (eol)
    {
      *eol++ = 0;
      if (*eol == '\n')
      {
        *eol++ = 0;
      }
      callback(cbdata, str);
      str = eol;
      eol = strchr(str, '\r');
    }
  }
  free(mem.response);
  return status == 200;
}

bool tokamak_client_game_new(tokamak_client *client, const char *game, tokamak_options *O)
{
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, client->url_api_0_player_new);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, client->cookiejar); 
  curl_easy_setopt(curl, CURLOPT_COOKIEJAR, client->cookiejar); 
  curl_mime *multipart = curl_mime_init(curl);
  curl_mimepart *part = curl_mime_addpart(multipart);
  curl_mime_name(part, "game");
  curl_mime_data(part, game, CURL_ZERO_TERMINATED);
  char ranks[32], suits[32], jokers[32], pile[32], deal[32];
  snprintf(ranks, 32, "%d", O->ranks); part = curl_mime_addpart(multipart); curl_mime_name(part, "ranks"); curl_mime_data(part, ranks, CURL_ZERO_TERMINATED);
  snprintf(suits, 32, "%d", O->suits); part = curl_mime_addpart(multipart); curl_mime_name(part, "suits"); curl_mime_data(part, suits, CURL_ZERO_TERMINATED);
  snprintf(jokers, 32, "%d", O->jokers); part = curl_mime_addpart(multipart); curl_mime_name(part, "jokers"); curl_mime_data(part, jokers, CURL_ZERO_TERMINATED);
  snprintf(pile, 32, "%d", O->pile); part = curl_mime_addpart(multipart); curl_mime_name(part, "pile"); curl_mime_data(part, pile, CURL_ZERO_TERMINATED);
  snprintf(deal, 32, "%d", O->deal); part = curl_mime_addpart(multipart); curl_mime_name(part, "deal"); curl_mime_data(part, deal, CURL_ZERO_TERMINATED);
  curl_easy_setopt(curl, CURLOPT_MIMEPOST, multipart);
  curl_easy_perform(curl);
  long status = 0;
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status);
  curl_mime_free(multipart);
  curl_easy_cleanup(curl);
  return status == 200;
}

bool tokamak_client_game_join(tokamak_client *client, const char *game)
{
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, client->url_api_0_game_join);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, client->cookiejar); 
  curl_easy_setopt(curl, CURLOPT_COOKIEJAR, client->cookiejar); 
  curl_mime *multipart = curl_mime_init(curl);
  curl_mimepart *part = curl_mime_addpart(multipart);
  curl_mime_name(part, "game");
  curl_mime_data(part, game, CURL_ZERO_TERMINATED);
  curl_easy_setopt(curl, CURLOPT_MIMEPOST, multipart);
  curl_easy_perform(curl);
  long status = 0;
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status);
  curl_mime_free(multipart);
  curl_easy_cleanup(curl);
  return status == 200;
}

bool tokamak_client_game_join_ai(tokamak_client *client, const char *game)
{
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, client->url_api_0_game_join_ai);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, client->cookiejar); 
  curl_easy_setopt(curl, CURLOPT_COOKIEJAR, client->cookiejar); 
  curl_mime *multipart = curl_mime_init(curl);
  curl_mimepart *part = curl_mime_addpart(multipart);
  curl_mime_name(part, "game");
  curl_mime_data(part, game, CURL_ZERO_TERMINATED);
  curl_easy_setopt(curl, CURLOPT_MIMEPOST, multipart);
  curl_easy_perform(curl);
  long status = 0;
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status);
  curl_mime_free(multipart);
  curl_easy_cleanup(curl);
  return status == 200;
}

typedef void (*tokamak_client_game_get_t)(void *, int, int, int, int, int, const char *);

bool tokamak_client_game_get(tokamak_client *client, const char *game, tokamak **T, int *my_turn, tokamak_client_game_get_t callback, void *cbdata)
{
  CURL *curl = curl_easy_init();
  char *egame = curl_easy_escape(curl, game, 0);
  char *url = strdupcat(client->url_api_0_game_get, egame);
  curl_free(egame);
  curl_easy_setopt(curl, CURLOPT_URL, url);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, client->cookiejar); 
  curl_easy_setopt(curl, CURLOPT_COOKIEJAR, client->cookiejar); 
  struct memory mem = {0};
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, memorycb);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &mem);
  curl_easy_perform(curl);
  long status = 0;
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status);
  curl_easy_cleanup(curl);
  if (status == 200)
  {
    int game_status = 0;
    tokamak_options O = {0};
    int ndeck = 0;
    int player = 0;
    int64_t rank_regular = 0, rank_joker = 0, suit_regular = 0, suit_joker = 0;
    char *str = mem.response;
    int len = -1;
    sscanf
      ( str
      , "%d,%d,%d,%d,%d,%d,%d,%d,%d,%" SCNi64 ",%" SCNi64 ",%" SCNi64 ",%" SCNi64 "\r\n\r\n%n"
      , &game_status
      , &O.ranks, &O.suits, &O.jokers, &O.pile, &O.deal
      , &ndeck, &O.players, &player
      , &rank_regular, &rank_joker, &suit_regular, &suit_joker
      , &len
      );
    if (len >= 0)
    {
      str += len;
    }
    int hand_sizes[TOKAMAK_BITS] = {0};
    for (int p = 0; p < O.players; ++p)
    {
      int sequence = 0, score = 0, type = 0, hand_size = 0;
      len = -1;
      sscanf
        ( str
        , "%d,%d,%d,%d,%n"
        , &sequence, &score, &type, &hand_size, &len
        );
      str += len;
      char *eol = strchr(str, '\r');
      if (eol)
      {
        *eol++ = 0;
        if (*eol == '\n')
        {
          *eol++ = 0;
        }
      }
      const char *name = str;
      str = eol;
      hand_sizes[p] = hand_size;
      callback(cbdata, O.players, sequence, score, type, hand_size, name);
    }
    *my_turn = 0;
    int64_t hand_regular = 0;
    int hand_njokers = 0;
    if (*str++ == '\r')
    {
      if (*str++ == '\n')
      {
        sscanf(str, "%" SCNi64 ",%d\r\n", &hand_regular, &hand_njokers);
        *my_turn = 1;
      }
    }
    *T = tokamak_new(&O);
    uint64_t deck = ((~(rank_regular | suit_regular | hand_regular)) & ((1ull << tokamak_get_regular(*T)) - 1)) | (((1ull << (O.jokers - hand_njokers - tokamak_cardset_size(*T, rank_joker) - tokamak_cardset_size(*T, suit_joker))) - 1) << tokamak_get_regular(*T));
    tokamak_restore(*T, deck, rank_regular, rank_joker, suit_regular, suit_joker, player);
    for (int p = 0; p < O.players; ++p)
    {
      if (*my_turn && p == player)
      {
        tokamak_restore_hand(*T, p, hand_regular, hand_njokers);
      }
      else
      {
        tokamak_restore_hand_size(*T, p, hand_sizes[p]);
      }
    }
  }
  return status == 200;
}

bool tokamak_client_game_post(tokamak_client *client, const char *game, const tokamak *T)
{
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, client->url_api_0_player_new);
  curl_easy_setopt(curl, CURLOPT_COOKIEFILE, client->cookiejar); 
  curl_easy_setopt(curl, CURLOPT_COOKIEJAR, client->cookiejar); 
  curl_mime *multipart = curl_mime_init(curl);
  curl_mimepart *part = curl_mime_addpart(multipart);
  curl_mime_name(part, "game");
  curl_mime_data(part, game, CURL_ZERO_TERMINATED);
  char rank_regular[32], rank_joker[32], suit_regular[32], suit_joker[32], hand_regular[32], hand_njoker[32];
  snprintf(rank_regular, 32, "%" PRId64, tokamak_move_get_rank_regular(T)); part = curl_mime_addpart(multipart); curl_mime_name(part, "rank_regular"); curl_mime_data(part, rank_regular, CURL_ZERO_TERMINATED);
  snprintf(rank_joker,   32, "%" PRId64, tokamak_move_get_rank_joker(T));   part = curl_mime_addpart(multipart); curl_mime_name(part, "rank_joker");   curl_mime_data(part, rank_joker,   CURL_ZERO_TERMINATED);
  snprintf(suit_regular, 32, "%" PRId64, tokamak_move_get_suit_regular(T)); part = curl_mime_addpart(multipart); curl_mime_name(part, "suit_regular"); curl_mime_data(part, suit_regular, CURL_ZERO_TERMINATED);
  snprintf(suit_joker,   32, "%" PRId64, tokamak_move_get_suit_joker(T));   part = curl_mime_addpart(multipart); curl_mime_name(part, "suit_joker");   curl_mime_data(part, suit_joker,   CURL_ZERO_TERMINATED);
  snprintf(hand_regular, 32, "%" PRId64, tokamak_move_get_hand_regular(T)); part = curl_mime_addpart(multipart); curl_mime_name(part, "hand_regular"); curl_mime_data(part, hand_regular, CURL_ZERO_TERMINATED);
  snprintf(hand_njoker,  32, "%d", tokamak_move_get_hand_njokers(T)); part = curl_mime_addpart(multipart); curl_mime_name(part, "hand_njoker"); curl_mime_data(part, hand_njoker, CURL_ZERO_TERMINATED);
  curl_easy_setopt(curl, CURLOPT_MIMEPOST, multipart);
  curl_easy_perform(curl);
  long status = 0;
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &status);
  curl_mime_free(multipart);
  curl_easy_cleanup(curl);
  return status == 200;
}
