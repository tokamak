#ifndef LIBTOKAMAK_CLIENT_H
#define LIBTOKAMAK_CLIENT_H 1

#include "libtokamak.h"

struct tokamak_client_s;
typedef struct tokamak_client_s tokamak_client;
tokamak_client *tokamak_client_new(const char *server, const char *cookiejar);
void tokamak_client_delete(tokamak_client *client);
bool tokamak_client_player_new(tokamak_client *client, const char *name);
typedef void (*tokamak_client_player_games_t)(void *, const char *);
bool tokamak_client_player_games(tokamak_client *client, tokamak_client_player_games_t callback, void *cbdata);
bool tokamak_client_game_new(tokamak_client *client, const char *game, tokamak_options *O);
bool tokamak_client_game_join(tokamak_client *client, const char *game);
bool tokamak_client_game_join_ai(tokamak_client *client, const char *game);
typedef void (*tokamak_client_game_get_t)(void *, int, int, int, int, int, const char *);
bool tokamak_client_game_get(tokamak_client *client, const char *game, tokamak **T, int *my_turn, tokamak_client_game_get_t callback, void *cbdata);
bool tokamak_client_game_post(tokamak_client *client, const char *game, const tokamak *T);

#endif
