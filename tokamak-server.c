/*
GET /game_name
- has no player id cookie?
  - display registration form
  - offer to register player: input: name; button: POST /game_name/register -> set player id cookie, GET /game_name
    - should validate that name is not empty
- has player id cookie?
  - game_name exists?
    - game_name is waiting for players?
      - player is waiting?
        - list players in game
        - space for more players?
          - offer to invite AI: button: POST /game_name/joinai -> GET /game_name
            - should validate that at least 1 human player is waiting
        - offer to start game: button: POST /game_name/start -> GET /game_name
          - form for rule selection?
          - should validate that rules are valid
          - should validate that at least 1 human player is waiting
      - player is not waiting?
        - list players in game
        - space for more players?
          - offer to join game: input (player name), button: POST /game_name/join -> GET /game_name
            - should validate that there is sufficient space for players
    - game_name is playing?
      - player is playing?
        - is player's turn?
          - display game state
          - offer to compose move: button: POST /game_name/move;movedata -> GET /game_name
            - should validate that game is playing
            - should validate that player is in player
            - should validate that it is player's turn
            - should validate move
        - is not player's turn?
          - display game state
          - sleep and retry
      - player is not playing?
        - display game state
        - sleep and retry
*/

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <threads.h>
#include <unistd.h>

#include <sqlite3.h>
#include <civetweb.h>

#include "libtokamak.h"

static volatile bool running = true;

#define TOKAMAK_COOKIE "tokamak_player_id"
#define TOKAMAK_COOKIE_OPTIONS "; Max-Age=31557600; Secure; HttpOnly"

static int HTTP_forbidden(struct mg_connection *conn)
{
  mg_printf(conn,
    "HTTP/1.1 403 Forbidden\r\n"
    "Content-Type: text/plain\r\n"
    "\r\n"
    "forbidden\n"
  );
  return 403;
}

static int HTTP_not_found(struct mg_connection *conn)
{
  mg_printf(conn,
    "HTTP/1.1 404 Not Found\r\n"
    "Content-Type: text/plain\r\n"
    "\r\n"
    "not found\n"
  );
  return 404;
}

static void HTTP_set_cookie(struct mg_connection *conn, int64_t player_id)
{
  if (player_id)
  {
    mg_printf(conn, "Set-Cookie: " TOKAMAK_COOKIE "=\"%" PRId64 "\"" TOKAMAK_COOKIE_OPTIONS "\r\n", player_id);
  }
}

// ---------------------------------------------------------------------

typedef struct
{
  mtx_t mutex;
  sqlite3 *db;
  sqlite3_stmt *sql_begin;
  sqlite3_stmt *sql_commit;
  sqlite3_stmt *sql_rollback;
  sqlite3_stmt *sql_random;
  sqlite3_stmt *sql_new_player;
  sqlite3_stmt *sql_new_game;
  sqlite3_stmt *sql_new_participant;
  sqlite3_stmt *sql_get_player;
  sqlite3_stmt *sql_player_games;
  sqlite3_stmt *sql_game_participants;
  sqlite3_stmt *sql_get_game;
  sqlite3_stmt *sql_in_game;
  sqlite3_stmt *sql_put_game;
  sqlite3_stmt *sql_put_participant;
  struct mg_callbacks callbacks;
  struct mg_context *ctx;
} tokamak_server;

// ---------------------------------------------------------------------

int64_t DB_player_register(tokamak_server *s, const char *name)
{
  sqlite3_reset(s->sql_begin);
  sqlite3_step(s->sql_begin);
  while (true)
  {
    sqlite_int64 player_id = 0;
    while (player_id <= 0)
    {
      sqlite3_reset(s->sql_random);
      sqlite3_step(s->sql_random);
      player_id = sqlite3_column_int64(s->sql_random, 0);
    }
    sqlite3_reset(s->sql_new_player);
    sqlite3_bind_int64(s->sql_new_player, 0, player_id);
    sqlite3_bind_text(s->sql_new_player, 1, name, -1, SQLITE_TRANSIENT);
    sqlite3_step(s->sql_new_player);
    if (sqlite3_changes(s->db))
    {
      sqlite3_reset(s->sql_commit);
      sqlite3_step(s->sql_commit);
      return player_id;
    }
  }
  // unreachable
}

// ---------------------------------------------------------------------

tokamak *DB_get_tokamak(tokamak_server *s, const char *game, int64_t player_id)
{
  sqlite3_reset(s->sql_get_game);
  sqlite3_bind_text(s->sql_get_game, 0, game, -1, SQLITE_TRANSIENT);
  if (SQLITE_ROW == sqlite3_step(s->sql_get_game))
  {
    const int64_t gstatus = sqlite3_column_int64(s->sql_get_game, 0);
    const int64_t ranks = sqlite3_column_int64(s->sql_get_game, 1);
    const int64_t suits = sqlite3_column_int64(s->sql_get_game, 2);
    const int64_t jokers = sqlite3_column_int64(s->sql_get_game, 3);
    const int64_t pile = sqlite3_column_int64(s->sql_get_game, 4);
    const int64_t deal = sqlite3_column_int64(s->sql_get_game, 5);
    const int64_t deck = sqlite3_column_int64(s->sql_get_game, 6);
    const int64_t players = sqlite3_column_int64(s->sql_get_game, 7);
    const int64_t starter = sqlite3_column_int64(s->sql_get_game, 8);
    const int64_t player = sqlite3_column_int64(s->sql_get_game, 9);
    const int64_t rank_regular = sqlite3_column_int64(s->sql_get_game, 10);
    const int64_t rank_joker = sqlite3_column_int64(s->sql_get_game, 11);
    const int64_t suit_regular = sqlite3_column_int64(s->sql_get_game, 12);
    const int64_t suit_joker = sqlite3_column_int64(s->sql_get_game, 13);
    const int64_t ndeck = __builtin_popcountll(deck);
    sqlite3_reset(s->sql_game_participants);
    sqlite3_bind_text(s->sql_game_participants, 0, game, -1, SQLITE_TRANSIENT);
    bool my_turn = false;
    int64_t new_hand_regular[TOKAMAK_BITS] = {0};
    int64_t new_hand_njoker[TOKAMAK_BITS] = {0};
    while (SQLITE_ROW == sqlite3_step(s->sql_game_participants))
    {
      const int64_t other_player_id = sqlite3_column_int64(s->sql_game_participants, 0);
      const int64_t sequence = sqlite3_column_int64(s->sql_game_participants, 1);
      const int64_t score = sqlite3_column_int64(s->sql_game_participants, 2);
      const int64_t hand_regular = sqlite3_column_int64(s->sql_game_participants, 3);
      const int64_t hand_njoker = sqlite3_column_int64(s->sql_game_participants, 4);
      const unsigned char *name = sqlite3_column_text(s->sql_game_participants, 6);
      if (other_player_id == player_id && sequence == player)
      {
        my_turn = true;
      }
      if (0 <= sequence && sequence < TOKAMAK_BITS)
      {
        new_hand_regular[sequence] = hand_regular;
        new_hand_njoker[sequence] = hand_njoker;
      }
    }
    if (my_turn)
    {
      tokamak_options O = { ranks, suits, jokers, pile, deal, players, starter };
      tokamak *T = tokamak_new(&O);
      tokamak_restore(T, deck, rank_regular, rank_joker, suit_regular, suit_joker, player);
      for (int p = 0; p < players; ++p)
      {
        tokamak_restore_hand(T, p, new_hand_regular[p], new_hand_njoker[p]);
      }
      tokamak_move_rollback(T);
      return T;
    }
    else
    {
      return 0;
    }
  }
  else
  {
    return 0;
  }
}

void DB_put_tokamak(tokamak_server *s, const char *game, int64_t player_id, const tokamak *T, int64_t new_status)
{
  sqlite3_reset(s->sql_put_game);
  sqlite3_bind_int64(s->sql_put_game, 0, new_status);
  sqlite3_bind_int64(s->sql_put_game, 1, tokamak_get_player(T));
  sqlite3_bind_int64(s->sql_put_game, 2, tokamak_get_deck(T));
  sqlite3_bind_int64(s->sql_put_game, 3, tokamak_get_rank_regular(T));
  sqlite3_bind_int64(s->sql_put_game, 4, tokamak_get_rank_joker(T));
  sqlite3_bind_int64(s->sql_put_game, 5, tokamak_get_suit_regular(T));
  sqlite3_bind_int64(s->sql_put_game, 6, tokamak_get_suit_joker(T));
  sqlite3_bind_text(s->sql_put_game, 7, game, -1, SQLITE_TRANSIENT);
  sqlite3_step(s->sql_put_game);
  sqlite3_reset(s->sql_put_participant);
  for (int player = 0; player < tokamak_get_players(T); ++player)
  {
    sqlite3_bind_int64(s->sql_put_participant, 0, tokamak_get_hand(T, player) & ((1ull << tokamak_get_regular(T)) - 1));
    sqlite3_bind_int64(s->sql_put_participant, 1, tokamak_cardset_size(T, tokamak_get_hand(T, player) & (((1ull << tokamak_get_jokers(T)) - 1) << tokamak_get_regular(T))));
    sqlite3_bind_text(s->sql_put_participant, 2, game, -1, SQLITE_TRANSIENT);
    sqlite3_bind_int64(s->sql_put_participant, 3, player_id);
    sqlite3_step(s->sql_put_participant);
  }
}

// ---------------------------------------------------------------------
// "POST /api/0/player"

typedef struct
{
  tokamak_server *server;
  int64_t player_id;
} POST_api_0_player_new_t;

static int POST_api_0_player_new_field_found(const char *key, const char *filename, char *path, size_t pathlen, void *user_data)
{
  if (0 == strcmp("name", key))
  {
    return MG_FORM_FIELD_STORAGE_GET;
  }
  else
  {
    return MG_FORM_FIELD_STORAGE_SKIP;
  }
}

static int POST_api_0_player_new_field_get(const char *key, const char *value, size_t valuelen, void *user_data)
{
  POST_api_0_player_new_t *r = user_data;
  if (! key || ! *key || ! valuelen || ! value || ! *value || ! r || ! r->server || ! r->player_id)
  {
    return MG_FORM_FIELD_HANDLE_ABORT;
	}
  r->player_id = DB_player_register(r->server, value);
  return MG_FORM_FIELD_HANDLE_ABORT;
}

static int POST_api_0_player_new(struct mg_connection *conn, tokamak_server *s, int64_t player_id)
{
  int status = 500;
  if (player_id == 0)
  {
    POST_api_0_player_new_t r = { s, 0 };
    struct mg_form_data_handler f =
      { POST_api_0_player_new_field_found
      , POST_api_0_player_new_field_get
      , 0
      , &r
      };
    mg_handle_form_request(conn, &f);
    if (r.player_id)
    {
      mg_printf(conn, "HTTP/1.1 200 OK\r\n");
      HTTP_set_cookie(conn, r.player_id);
      mg_printf(conn, "Content-Type: text/plain; charset=UTF-8\r\n\r\nok\r\n");
      status = 200;
    }
    else
    {
      status = HTTP_forbidden(conn);
    }
  }
  else
  {
    status = HTTP_forbidden(conn);
  }
  return status;
}

// ---------------------------------------------------------------------
// "GET /api/0/player/games"

static int GET_api_0_player_games(struct mg_connection *conn, tokamak_server *s, int64_t player_id)
{
  int status = 500;
  if (player_id == 0)
  {
    status = HTTP_forbidden(conn);
  }
  else
  {
    mg_printf(conn, "HTTP/1.1 200 OK\r\n");
    HTTP_set_cookie(conn, player_id);
    mg_printf(conn, "Content-Type: text/plain; charset=UTF-8\r\n\r\n");
    sqlite3_reset(s->sql_player_games);
    sqlite3_bind_int64(s->sql_player_games, 0, player_id);
    while (SQLITE_ROW == sqlite3_step(s->sql_player_games))
    {
      const unsigned char *game = sqlite3_column_text(s->sql_player_games, 0);
      mg_printf(conn, "%s\r\n", game);
    }
    status = 200;
  }
  return status;
}

// ---------------------------------------------------------------------
// "GET /api/0/game"

typedef struct
{
  char *game;
} GET_api_0_game_t;

static int GET_api_0_game_field_found(const char *key, const char *filename, char *path, size_t pathlen, void *user_data)
{
  if (0 == strcmp("game", key))
  {
    return MG_FORM_FIELD_STORAGE_GET;
  }
  else
  {
    return MG_FORM_FIELD_STORAGE_SKIP;
  }
}

static int GET_api_0_game_field_get(const char *key, const char *value, size_t valuelen, void *user_data)
{
  GET_api_0_game_t *r = user_data;
  if (! key || ! *key || ! valuelen || ! value || ! *value || ! r)
  {
    return MG_FORM_FIELD_HANDLE_ABORT;
	}
  if (0 == strcmp("game", key))
  {
    if (r->game)
    {
      free(r->game);
    }
    r->game = strdup(value);
  }
  return MG_FORM_FIELD_HANDLE_NEXT;
}

static int GET_api_0_game(struct mg_connection *conn, tokamak_server *s, int64_t player_id)
{
  int status = 500;
  GET_api_0_game_t r = {0};
  struct mg_form_data_handler f =
    { GET_api_0_game_field_found
    , GET_api_0_game_field_get
    , 0
    , &r
    };
  mg_handle_form_request(conn, &f);
  if (r.game && *r.game)
  {
    sqlite3_reset(s->sql_begin);
    sqlite3_step(s->sql_begin);
    sqlite3_reset(s->sql_get_game);
    sqlite3_bind_text(s->sql_get_game, 0, r.game, -1, SQLITE_TRANSIENT);
    if (SQLITE_ROW == sqlite3_step(s->sql_get_game))
    {
      mg_printf(conn, "HTTP/1.1 200 OK\r\n");
      HTTP_set_cookie(conn, player_id);
      mg_printf(conn, "Content-Type: text/plain; charset=UTF-8\r\n\r\n");
      const int64_t gstatus = sqlite3_column_int64(s->sql_get_game, 0);
      const int64_t ranks = sqlite3_column_int64(s->sql_get_game, 1);
      const int64_t suits = sqlite3_column_int64(s->sql_get_game, 2);
      const int64_t jokers = sqlite3_column_int64(s->sql_get_game, 3);
      const int64_t pile = sqlite3_column_int64(s->sql_get_game, 4);
      const int64_t deal = sqlite3_column_int64(s->sql_get_game, 5);
      const int64_t players = sqlite3_column_int64(s->sql_get_game, 6);
      const int64_t player = sqlite3_column_int64(s->sql_get_game, 7);
      const int64_t deck = sqlite3_column_int64(s->sql_get_game, 8);
      const int64_t rank_regular = sqlite3_column_int64(s->sql_get_game, 9);
      const int64_t rank_joker = sqlite3_column_int64(s->sql_get_game, 10);
      const int64_t suit_regular = sqlite3_column_int64(s->sql_get_game, 11);
      const int64_t suit_joker = sqlite3_column_int64(s->sql_get_game, 12);
      const int64_t ndeck = __builtin_popcountll(deck);
      mg_printf(conn,
        "%" PRId64 ",%" PRId64 ",%" PRId64  ",%" PRId64
        ",%" PRId64 ",%" PRId64 ",%" PRId64 ",%" PRId64
        ",%" PRId64 ",%" PRId64 ",%" PRId64 ",%" PRId64
        ",%" PRId64 "\r\n\r\n"
      , gstatus, ranks, suits, jokers
      , pile, deal, ndeck, players
      , player, rank_regular, rank_joker, suit_regular
      , suit_joker
      );
      sqlite3_reset(s->sql_game_participants);
      sqlite3_bind_text(s->sql_game_participants, 0, r.game, -1, SQLITE_TRANSIENT);
      int64_t my_sequence = -1;
      int64_t my_hand_regular = 0;
      int64_t my_nhand_joker = 0;
      while (SQLITE_ROW == sqlite3_step(s->sql_game_participants))
      {
        const int64_t other_player_id = sqlite3_column_int64(s->sql_game_participants, 0);
        const int64_t sequence = sqlite3_column_int64(s->sql_game_participants, 1);
        const int64_t score = sqlite3_column_int64(s->sql_game_participants, 2);
        const int64_t hand_regular = sqlite3_column_int64(s->sql_game_participants, 3);
        const int64_t hand_joker = sqlite3_column_int64(s->sql_game_participants, 4);
        const int64_t type = sqlite3_column_int64(s->sql_game_participants, 5);
        const unsigned char *name = sqlite3_column_text(s->sql_game_participants, 6);
        const int64_t nhand_regular = __builtin_popcountll(hand_regular);
        const int64_t nhand_joker = __builtin_popcountll(hand_joker);
        const int64_t hand_size = nhand_regular + nhand_joker;
        mg_printf(conn, "%" PRId64 ",%" PRId64 ",%" PRId64  ",%" PRId64 ",%s\r\n", sequence, score, type, hand_size, name);
        if (other_player_id == player_id)
        {
          my_sequence = sequence;
          my_hand_regular = hand_regular;
          my_nhand_joker = nhand_joker;
        }
      }
      if (my_sequence >= 0)
      {
        mg_printf(conn, "\r\n%" PRId64 ",%" PRId64 "\r\n", my_hand_regular, my_nhand_joker);
      }
      status = 200;
    }
    else
    {
      status = HTTP_not_found(conn);
    }
    sqlite3_reset(s->sql_commit);
    sqlite3_step(s->sql_commit);
    free(r.game);
  }
  else
  {
    status = HTTP_forbidden(conn);
  }
  return status;
}

// ---------------------------------------------------------------------
// "POST /api/0/game/new"

typedef struct
{
  char *game;
  int64_t ranks;
  int64_t suits;
  int64_t jokers;
  int64_t pile;
  int64_t deal;
} POST_api_0_game_new_t;

static int POST_api_0_game_new_field_found(const char *key, const char *filename, char *path, size_t pathlen, void *user_data)
{
  if (0 == strcmp("game", key) ||
      0 == strcmp("ranks", key) ||
      0 == strcmp("suits", key) ||
      0 == strcmp("jokers", key) ||
      0 == strcmp("pile", key) ||
      0 == strcmp("deal", key))
  {
    return MG_FORM_FIELD_STORAGE_GET;
  }
  else
  {
    return MG_FORM_FIELD_STORAGE_SKIP;
  }
}

static int POST_api_0_game_new_field_get(const char *key, const char *value, size_t valuelen, void *user_data)
{
  POST_api_0_game_new_t *r = user_data;
  if (! key || ! *key || ! valuelen || ! value || ! *value || ! r)
  {
    return MG_FORM_FIELD_HANDLE_ABORT;
	}
  if (0 == strcmp("game", key))
  {
    if (r->game)
    {
      free(r->game);
    }
    r->game = strdup(value);
  }
  else if (0 == strcmp("ranks", key))
  {
    r->ranks = atoll(value);
  }
  else if (0 == strcmp("suits", key))
  {
    r->suits = atoll(value);
  }
  else if (0 == strcmp("jokers", key))
  {
    r->jokers = atoll(value);
  }
  else if (0 == strcmp("pile", key))
  {
    r->pile = atoll(value);
  }
  else if (0 == strcmp("deal", key))
  {
    r->deal = atoll(value);
  }
  return MG_FORM_FIELD_HANDLE_NEXT;
}

static int POST_api_0_game_new(struct mg_connection *conn, tokamak_server *s, int64_t player_id)
{
  int status = 500;
  if (player_id)
  {
    POST_api_0_game_new_t r = {0};
    struct mg_form_data_handler f =
      { POST_api_0_game_new_field_found
      , POST_api_0_game_new_field_get
      , 0
      , &r
      };
    mg_handle_form_request(conn, &f);
    if (r.game)
    {
      sqlite3_reset(s->sql_new_game);
      sqlite3_bind_text(s->sql_new_game, 0, r.game, -1, SQLITE_TRANSIENT);
      free(r.game);
      sqlite3_bind_int64(s->sql_new_game, 1, r.ranks);
      sqlite3_bind_int64(s->sql_new_game, 2, r.suits);
      sqlite3_bind_int64(s->sql_new_game, 3, r.jokers);
      sqlite3_bind_int64(s->sql_new_game, 4, r.pile);
      sqlite3_bind_int64(s->sql_new_game, 5, r.deal);
      sqlite3_step(s->sql_new_game);
      mg_printf(conn, "HTTP/1.1 200 OK\r\n");
      HTTP_set_cookie(conn, player_id);
      mg_printf(conn, "Content-Type: text/plain; charset=UTF-8\r\n\r\nok\r\n");
      status = 200;
    }
  }
  else
  {
    status = HTTP_forbidden(conn);
  }
  return status;
}

// ---------------------------------------------------------------------
// "POST /api/0/game/join"

typedef struct
{
  char *game;
} POST_api_0_game_join_t;

static int POST_api_0_game_join_field_found(const char *key, const char *filename, char *path, size_t pathlen, void *user_data)
{
  if (0 == strcmp("game", key))
  {
    return MG_FORM_FIELD_STORAGE_GET;
  }
  else
  {
    return MG_FORM_FIELD_STORAGE_SKIP;
  }
}

static int POST_api_0_game_join_field_get(const char *key, const char *value, size_t valuelen, void *user_data)
{
  POST_api_0_game_join_t *r = user_data;
  if (! key || ! *key || ! valuelen || ! value || ! *value || ! r)
  {
    return MG_FORM_FIELD_HANDLE_ABORT;
	}
  if (0 == strcmp("game", key))
  {
    if (r->game)
    {
      free(r->game);
    }
    r->game = strdup(value);
  }
  return MG_FORM_FIELD_HANDLE_NEXT;
}

static int POST_api_0_game_join(struct mg_connection *conn, tokamak_server *s, int64_t player_id)
{
  int status = 500;
  if (player_id)
  {
    POST_api_0_game_join_t r = {0};
    struct mg_form_data_handler f =
      { POST_api_0_game_join_field_found
      , POST_api_0_game_join_field_get
      , 0
      , &r
      };
    mg_handle_form_request(conn, &f);
    if (r.game)
    {
      sqlite3_reset(s->sql_new_participant);
      sqlite3_bind_text(s->sql_new_participant, 0, r.game, -1, SQLITE_TRANSIENT);
      sqlite3_bind_int64(s->sql_new_participant, 1, player_id);
      sqlite3_bind_text(s->sql_new_participant, 2, r.game, -1, SQLITE_TRANSIENT);
      if (SQLITE_ROW == sqlite3_step(s->sql_new_participant))
      {
        mg_printf(conn, "HTTP/1.1 200 OK\r\n");
        HTTP_set_cookie(conn, player_id);
        mg_printf(conn, "Content-Type: text/plain; charset=UTF-8\r\n\r\nok\r\n");
        status = 200;
      }
      else
      {
        status = HTTP_forbidden(conn);
      }
      free(r.game);
    }
    else
    {
      status = HTTP_forbidden(conn);
    }
  }
  else
  {
    status = HTTP_forbidden(conn);
  }
  return status;
}

static int POST_api_0_game_join_ai(struct mg_connection *conn, tokamak_server *s, int64_t player_id)
{
  int status = 500;
  if (player_id)
  {
    POST_api_0_game_join_t r = {0};
    struct mg_form_data_handler f =
      { POST_api_0_game_join_field_found
      , POST_api_0_game_join_field_get
      , 0
      , &r
      };
    mg_handle_form_request(conn, &f);
    if (r.game)
    {
      sqlite3_reset(s->sql_begin);
      sqlite3_step(s->sql_begin);
      sqlite3_reset(s->sql_in_game);
      sqlite3_bind_text(s->sql_in_game, 0, r.game, -1, SQLITE_TRANSIENT);
      sqlite3_bind_int64(s->sql_in_game, 1, player_id);
      sqlite3_step(s->sql_in_game);
      if (sqlite3_column_int64(s->sql_in_game, 0))
      {
        sqlite3_reset(s->sql_new_participant);
        sqlite3_bind_text(s->sql_new_participant, 0, r.game, -1, SQLITE_TRANSIENT);
        sqlite3_bind_int64(s->sql_new_participant, 1, 1);
        sqlite3_bind_text(s->sql_new_participant, 2, r.game, -1, SQLITE_TRANSIENT);
        if (SQLITE_ROW == sqlite3_step(s->sql_new_participant))
        {
          mg_printf(conn, "HTTP/1.1 200 OK\r\n");
          HTTP_set_cookie(conn, player_id);
          mg_printf(conn, "Content-Type: text/plain; charset=UTF-8\r\n\r\nok\r\n");
          status = 200;
        }
        else
        {
          status = HTTP_forbidden(conn);
        }
      }
      else
      {
        status = HTTP_forbidden(conn);
      }
      sqlite3_reset(s->sql_commit);
      sqlite3_step(s->sql_commit);
      free(r.game);
    }
    else
    {
      status = HTTP_forbidden(conn);
    }
  }
  else
  {
    status = HTTP_forbidden(conn);
  }
  return status;
}

// ---------------------------------------------------------------------
// "POST /api/0/game"

typedef struct
{
  int64_t rank_regular;
  int64_t rank_joker;
  int64_t suit_regular;
  int64_t suit_joker;
  int64_t hand_regular;
  int64_t hand_njoker;
  char *game;
} POST_api_0_game_t;

static int POST_api_0_game_field_found(const char *key, const char *filename, char *path, size_t pathlen, void *user_data)
{
  if (0 == strcmp("rank_regular", key) ||
      0 == strcmp("rank_joker", key) ||
      0 == strcmp("suit_regular", key) ||
      0 == strcmp("suit_joker", key) ||
      0 == strcmp("hand_regular", key) ||
      0 == strcmp("hand_njoker", key) ||
      0 == strcmp("game", key))
  {
    return MG_FORM_FIELD_STORAGE_GET;
  }
  else
  {
    return MG_FORM_FIELD_STORAGE_SKIP;
  }
}

static int POST_api_0_game_field_get(const char *key, const char *value, size_t valuelen, void *user_data)
{
  POST_api_0_game_t *r = user_data;
  if (! key || ! *key || ! valuelen || ! value || ! *value || ! r)
  {
    return MG_FORM_FIELD_HANDLE_ABORT;
	}
  if (0 == strcmp("rank_regular", key))
  {
    r->rank_regular = atoll(value);
  }
  else if (0 == strcmp("rank_joker", key))
  {
    r->rank_joker = atoll(value);
  }
  else if (0 == strcmp("suit_regular", key))
  {
    r->suit_regular = atoll(value);
  }
  else if (0 == strcmp("suit_joker", key))
  {
    r->suit_joker = atoll(value);
  }
  else if (0 == strcmp("hand_regular", key))
  {
    r->hand_regular = atoll(value);
  }
  else if (0 == strcmp("hand_njoker", key))
  {
    r->hand_njoker = atoll(value);
  }
  else if (0 == strcmp("game", key))
  {
    if (r->game)
    {
      free(r->game);
    }
    r->game = strdup(value);
  }
  return MG_FORM_FIELD_HANDLE_NEXT;
}

static int POST_api_0_game(struct mg_connection *conn, tokamak_server *s, int64_t player_id)
{
  int status = 500;
  if (player_id)
  {
    POST_api_0_game_t r = {0};
    struct mg_form_data_handler f =
      { POST_api_0_game_field_found
      , POST_api_0_game_field_get
      , 0
      , &r
      };
    mg_handle_form_request(conn, &f);
    if (r.game)
    {
      sqlite3_reset(s->sql_begin);
      sqlite3_step(s->sql_begin);
      tokamak *T = 0;
      int new_status = 0;
      if ((T = DB_get_tokamak(s, r.game, player_id)))
      {
        tokamak_move_set_hand_regular(T, r.hand_regular);
        tokamak_move_set_hand_njokers(T, r.hand_njoker);
        tokamak_move_set_rank_regular(T, r.rank_regular);
        tokamak_move_set_rank_joker(T, r.rank_joker);
        tokamak_move_set_suit_regular(T, r.suit_regular);
        tokamak_move_set_suit_joker(T, r.suit_joker);
        tokamak_commit commit = tokamak_move_commit(T);
        switch (commit)
        {
          case tokamak_commit_ok:
            new_status = 1;
            break;
          case tokamak_commit_stalemate:
            new_status = 2;
            break;
          case tokamak_commit_winner:
            new_status = 3;
            break;
          default:
            sqlite3_reset(s->sql_rollback);
            sqlite3_step(s->sql_rollback);
            tokamak_delete(T);
            status = HTTP_forbidden(conn);
            free(r.game);
            return status;
        }
        DB_put_tokamak(s, r.game, player_id, T, new_status);
        sqlite3_reset(s->sql_commit);
        sqlite3_step(s->sql_commit);
        mg_printf(conn, "HTTP/1.1 200 OK\r\n");
        HTTP_set_cookie(conn, player_id);
        mg_printf(conn, "Content-Type: text/plain; charset=UTF-8\r\n\r\n");
        switch (commit)
        {
          case tokamak_commit_ok:
            mg_printf(conn, "ok\r\n");
            break;
          case tokamak_commit_winner:
            mg_printf(conn, "winner %d\r\n", tokamak_get_player(T));
            break;
          case tokamak_commit_stalemate:
            mg_printf(conn, "stalemate\r\n");
            break;
        }
        tokamak_delete(T);
        status = 200;
      }
      else
      {
        sqlite3_reset(s->sql_rollback);
        sqlite3_step(s->sql_rollback);
        status = HTTP_forbidden(conn);
      }
      free(r.game);
    }
    else
    {
      status = HTTP_forbidden(conn);
    }
  }
  else
  {
    status = HTTP_forbidden(conn);
  }
  return status;
}

static int HTTP_api_0(struct mg_connection *conn, void *cbdata)
{
  int status = 500;
  tokamak_server *s = cbdata;
  mtx_lock(&s->mutex);
  const struct mg_request_info *ri = mg_get_request_info(conn);
  int64_t player_id = 0;
  const char *cookie = mg_get_header(conn, "Cookie");
  if (cookie)
  {
    char player_id_str[17] = {0};
    if (0 < mg_get_cookie(cookie, TOKAMAK_COOKIE, &player_id_str[0], sizeof(player_id_str)))
    {
      int64_t player_id_scn = 0;
      if (1 == sscanf(&player_id_str[0], "%" SCNi64, &player_id_scn))
      {
        if (0 < player_id_scn)
        {
          sqlite3_reset(s->sql_get_player);
          sqlite3_bind_int64(s->sql_get_player, 0, player_id_scn);
          if (SQLITE_ROW == sqlite3_step(s->sql_get_player))
          {
            player_id = player_id_scn;
          }
        }
      }
    }
  }
  // dispatch
       if (0 == strcmp("POST", ri->request_method) && 0 == strcmp("/api/0/player/new", ri->local_uri))
  {
    status = POST_api_0_player_new(conn, s, player_id);
  }
  else if (0 == strcmp("GET",  ri->request_method) && 0 == strcmp("/api/0/player/games", ri->local_uri))
  {
    status = GET_api_0_player_games(conn, s, player_id);
  }
  else if (0 == strcmp("POST", ri->request_method) && 0 == strcmp("/api/0/game/new", ri->local_uri))
  {
    status = POST_api_0_game_new(conn, s, player_id);
  }
  else if (0 == strcmp("POST", ri->request_method) && 0 == strcmp("/api/0/game/join", ri->local_uri))
  {
    status = POST_api_0_game_join(conn, s, player_id);
  }
  else if (0 == strcmp("POST", ri->request_method) && 0 == strcmp("/api/0/game/join_ai", ri->local_uri))
  {
    status = POST_api_0_game_join_ai(conn, s, player_id);
  }
  else if (0 == strcmp("GET",  ri->request_method) && 0 == strcmp("/api/0/game", ri->local_uri))
  {
    status = GET_api_0_game(conn, s, player_id);
  }
  else if (0 == strcmp("POST",  ri->request_method) && 0 == strcmp("/api/0/game", ri->local_uri))
  {
    status = POST_api_0_game(conn, s, player_id);
  }
  else
  {
    status = HTTP_not_found(conn);
  }
  mtx_unlock(&s->mutex);
  return status;
}

int main(int argc, char **argv)
{
  const char *program = "tokamak-server";
  const char *port = "8080";
  if (argc > 0)
  {
    program = argv[0];
  }
  if (argc > 1)
  {
    port = argv[1];
  }
  const char *database = "tokamak.sqlite";
  if (argc > 2)
  {
    database = argv[2];
  }
  fprintf(stderr, "INFO: %s starting on port %s with database %s\n", program, port, database);

  tokamak_server server;
  memset(&server, 0, sizeof(server));
  mg_init_library(MG_FEATURES_DEFAULT);
  int retval = 2;
  const char *sql = 0;
  if (thrd_success == mtx_init(&server.mutex, mtx_plain)) {
  if (SQLITE_OK == sqlite3_open(database, &server.db)) {
  char *err = 0;
  if (SQLITE_OK == sqlite3_exec
      ( server.db
      , "CREATE TABLE IF NOT EXISTS games (game TEXT NOT NULL PRIMARY KEY, ranks INTEGER NOT NULL, suits INTEGER NOT NULL, jokers INTEGER NOT NULL, pile INTEGER NOT NULL, deal INTEGER NOT NULL, status INTEGER NOT NULL, deck INTEGER NOT NULL, player INTEGER NOT NULL, rank_regular INTEGER NOT NULL, rank_joker INTEGER NOT NULL, suit_regular INTEGER NOT NULL, suit_joker INTEGER NOT NULL);\n"
        "CREATE TABLE IF NOT EXISTS players (player_id INTEGER NOT NULL PRIMARY KEY, name TEXT NOT NULL, type INTEGER NOT NULL);\n"
        "CREATE TABLE IF NOT EXISTS participants (game TEXT NOT NULL, player_id INTEGER NOT NULL, sequence INTEGER NOT NULL, score INTEGER NOT NULL, hand_regular INTEGER NOT NULL, hand_njoker INTEGER NOT NULL, PRIMARY KEY (game, player_id), FOREIGN KEY (game) REFERENCES games(game), FOREIGN KEY (player_id) REFERENCES players(player_id));\n"
        "INSERT OR IGNORE INTO players(player_id, name, type) VALUES (1, 'Andrei', 1);\n"
      , 0, 0, &err
      )) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="BEGIN;", -1, &server.sql_begin, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="COMMIT;", -1, &server.sql_commit, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="ROLLBACK;", -1, &server.sql_rollback, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="SELECT random();", -1, &server.sql_random, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="INSERT INTO players(player_id, name, type) VALUES (?, ?, 0);", -1, &server.sql_new_player, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="INSERT INTO games(game, ranks, suits, jokers, pile, deal, status, deck, player, rank_regular, rank_joker, suit_regular, suit_joker) VALUES (?, ?, ?, ?, ?, ?, 0, 0, 0, 0, 0, 0, 0);", -1, &server.sql_new_game, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="INSERT INTO participants(game, player_id, sequence, score, hand_regular, hand_njoker) VALUES (?, ?, (SELECT count(*) FROM participants WHERE game = ?), 0, 0, 0);", -1, &server.sql_new_participant, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="SELECT name, type FROM players WHERE player_id = ?;", -1, &server.sql_get_player, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="SELECT game FROM participants WHERE player_id = ? ORDER BY game ASC;", -1, &server.sql_player_games, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="SELECT participants.player_id, participants.sequence, participants.score, participants.hand_regular, participants.hand_njoker, players.type, players.name FROM participants INNER JOIN players ON participants.player_id = players.player_id WHERE participants.game = ?; ORDER BY participants.sequence ASC", -1, &server.sql_game_participants, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="SELECT games.status, games.ranks, games.suits, games.jokers, games.pile, games.deal, count(participants.player_id), games.player, games.deck, games.rank_regular, games.rank_joker, games.suit_regular, games.suit_joker FROM games INNER JOIN participants ON games.game = participants.game WHERE games.game = ? LIMIT 1;", -1, &server.sql_get_game, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="SELECT count(*) FROM participants WHERE game = ? AND player_id = ?;", -1, &server.sql_in_game, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="UPDATE games SET status = ?, player = ?, deck = ?, rank_regular = ?, rank_joker = ?, suit_regular = ?, suit_joker = ? WHERE game = ?;", -1, &server.sql_put_game, 0)) {
  if (SQLITE_OK == sqlite3_prepare_v2(server.db, sql="UPDATE participants SET hand_regular = ?, hand_njoker = ? WHERE game = ? AND player_id = ?;", -1, &server.sql_put_participant, 0)) {

  const char *options[] =
    { "listening_ports"
    , port
    , "request_timeout_ms"
    , "10000"
    , "error_log_file"
    , "error.log"
    , 0
    };
  if ((server.ctx = mg_start(&server.callbacks, 0, options))) {
  mg_set_request_handler(server.ctx, "/api/0/", HTTP_api_0, &server);

  fprintf(stderr, "INFO: %s started\n", program);
  while (running) sleep(1);
  retval = 0;
  fprintf(stderr, "INFO: %s stopping\n", program);

  mg_stop(server.ctx);
  } else { retval = 1; fprintf(stderr, "ERROR: could not start web server\n"); }
  
  sqlite3_finalize(server.sql_put_participant); }
  sqlite3_finalize(server.sql_put_game); }
  sqlite3_finalize(server.sql_in_game); }
  sqlite3_finalize(server.sql_get_game); }
  sqlite3_finalize(server.sql_game_participants); }
  sqlite3_finalize(server.sql_player_games); }
  sqlite3_finalize(server.sql_get_player); }
  sqlite3_finalize(server.sql_new_participant); }
  sqlite3_finalize(server.sql_new_game); }
  sqlite3_finalize(server.sql_new_player); }
  sqlite3_finalize(server.sql_random); }
  sqlite3_finalize(server.sql_rollback); }
  sqlite3_finalize(server.sql_commit); }
  sqlite3_finalize(server.sql_begin); }
  } else { retval = 1; fprintf(stderr, "ERROR: could not initialize sqlite3 database: %s\n", err ? err : "unknown error"); }
  sqlite3_close(server.db);
  } else { retval = 1; fprintf(stderr, "ERROR: could not open sqlite3 database\n"); }
  mtx_destroy(&server.mutex);
  } else { retval = 1; fprintf(stderr, "ERROR: could not initialize mutex\n"); }
  if (retval == 2)
  {
    fprintf(stderr, "ERROR: could not prepare sqlite3 statement: %s\n", sql);
    retval = 1;
  }
  mg_exit_library();
  fprintf(stderr, "INFO: %s stopped\n", program);
  return retval;
}
