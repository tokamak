#include <ctype.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <civetweb.h>

#include "libtokamak.h"

int64_t tokamak_random64(void)
{
  static int64_t counter = 0;
  if (counter == 0)
  {
    counter = time(0);
  }
  return ++counter; // FIXME
}

char *tokamak_encrypt_password(const char *plaintext)
{
  return strdup(plaintext); // FIXME
}

char *tokamak_URI_unescape(const char *in)
{
  if (! in)
  {
    return 0;
  }
  size_t bytes = 1; // null termination
  for (const char *s = in; *s; ++s)
  {
    if (*s == '%')
    {
      s++;
      if (isxdigit(*s))
      {
        s++;
        if (isxdigit(*s))
        {
          bytes += 1;
        }
        else
        {
          return 0;
        }
      }
      else
      {
        return 0;
      }
    }
    else
    {
      bytes += 1;
    }
  }
  char *out = malloc(bytes);
  if (! out)
  {
    return 0;
  }
  char *o = out;
  for (const char *s = in; *s; ++s)
  {
    if (*s == '%')
    {
      s++;
      char nibble[5];
      nibble[0] = '0';
      nibble[1] = 'x';
      nibble[2] = *s++;
      nibble[3] = *s;
      nibble[4] = 0;
      int c = atoi(nibble);
      if (c)
      {
        *o++ = c;
      }
      else
      {
        free(out);
        return 0;
      }
    }
    else
    {
      *o++ = *s;
    }
  }
  *o = 0;
  return out;
}

char *tokamak_URI_escape(const char *in)
{
  if (! in)
  {
    return 0;
  }
  size_t bytes = 1; // null termination
  for (const char *s = in; *s; ++s)
  {
    if (('0' <= *s && *s <= '9') ||
        ('A' <= *s && *s <= 'Z') ||
        ('a' <= *s && *s <= 'z'))
    {
      bytes += 1;
    }
    else
    {
      bytes += 3;
    }
  }
  char *out = malloc(bytes);
  if (! out)
  {
    return 0;
  }
  char *o = out;
  for (const char *s = in; *s; ++s)
  {
    if (('0' <= *s && *s <= '9') ||
        ('A' <= *s && *s <= 'Z') ||
        ('a' <= *s && *s <= 'z'))
    {
      *o++ = *s;
    }
    else
    {
      const char *hex = "0123456789ABCDEF";
      *o++ = '%';
      *o++ = hex[((unsigned char) *s >> 4) & 0xF];
      *o++ = hex[*s & 0xF];
    }
  }
  *o = 0;
  return out;
}

char *tokamak_HTML_escape(const char *in)
{
  if (! in)
  {
    return 0;
  }
  size_t bytes = 1; // null termination
  for (const char *s = in; *s; ++s)
  {
    switch (*s)
    {
      case '\'': case '"': bytes += 6; break;
      case '&': bytes += 5; break;
      case '<': case '>': bytes += 4; break;
      default: bytes += 1; break;
    }
  }
  char *out = malloc(bytes);
  if (! out)
  {
    return 0;
  }
  char *o = out;
  for (const char *s = in; *s; ++s)
  {
    switch (*s)
    {
      case '\'': *o++ = '&'; *o++ = 'a'; *o++ = 'p'; *o++ = 'o'; *o++ = 's'; *o++ = ';'; break;
      case '"':  *o++ = '&'; *o++ = 'q'; *o++ = 'u'; *o++ = 'o'; *o++ = 't'; *o++ = ';'; break;
      case '&':  *o++ = '&'; *o++ = 'a'; *o++ = 'm'; *o++ = 'p'; *o++ = ';'; break;
      case '<':  *o++ = '&'; *o++ = 'l'; *o++ = 't'; *o++ = ';'; break;
      case '>':  *o++ = '&'; *o++ = 'g'; *o++ = 't'; *o++ = ';'; break;
      default: *o++ = *s; break;
    }
  }
  *o = 0;
  return out;
}

struct tokamak_player;
struct tokamak_client;
struct tokamak_room;

struct tokamak_player
{
  struct tokamak_player *next;
  struct tokamak_room *room;
  struct tokamak_client *client;
  char *name;
  char *password;
  int64_t auth;
  int host;
  bool playing;
  int order;
  int played;
  int won;
  time_t created;
  time_t last_seen;
};

struct tokamak_player_order
{
  struct tokamak_player *player;
  int64_t order;
};

int tokamak_player_order_cmp(const void *a, const void *b)
{
  const struct tokamak_player_order *p = a;
  const struct tokamak_player_order *q = b;
  const int64_t x = p->order;
  const int64_t y = q->order;
  return (x > y) - (x < y);
}

struct tokamak_room
{
  struct tokamak_room *next;
  struct tokamak_player *players;
  char *name;
  char *password;
  tokamak_options options;
  tokamak *game;
  bool gameover;
  time_t created;
  time_t last_seen;
};

enum tokamak_room_state
{
  tokamak_room_state_invalid = 0,
  tokamak_room_state_valid = 1,
  tokamak_room_state_gaming = 2
};

int tokamak_room_get_state(const struct tokamak_room *room)
{
  if (room->game && ! room->gameover)
  {
    return tokamak_room_state_gaming;
  }
  else if (tokamak_options_valid(&room->options))
  {
    return tokamak_room_state_valid;
  }
  else
  {
    return tokamak_room_state_invalid;
  }
}

struct tokamak_room *tokamak_server = 0;
volatile bool running = true;

struct tokamak_POST
{
  char *room;
  char *key;
  char *name;
  char *password;
};

int tokamak_POST_found(const char *key, const char *filename, char *path, size_t pathlen, void *user_data)
{
  if (0 == strcmp("room", key) ||
      0 == strcmp("key", key) ||
      0 == strcmp("name", key) ||
      0 == strcmp("password", key))
  {
    return MG_FORM_FIELD_STORAGE_GET;
  }
  else
  {
    return MG_FORM_FIELD_STORAGE_SKIP;
  }
}

int tokamak_POST_get(const char *key, const char *value, size_t valuelen, void *user_data)
{
  struct tokamak_POST *r = user_data;
  if (! key || ! value || ! r)
  {
    return MG_FORM_FIELD_HANDLE_ABORT;
	}
  if (0 == strcmp("room", key))
  {
    if (r->room)
    {
      free(r->room);
    }
    r->room = strdup(value);
  }
  else if (0 == strcmp("key", key))
  {
    if (r->key)
    {
      free(r->key);
    }
    r->key = tokamak_encrypt_password(value);
  }
  else if (0 == strcmp("name", key))
  {
    if (r->name)
    {
      free(r->name);
    }
    r->name = strdup(value);
  }
  else if (0 == strcmp("password", key))
  {
    if (r->password)
    {
      free(r->password);
    }
    r->password = tokamak_encrypt_password(value);
  }
  else
  {
    return MG_FORM_FIELD_HANDLE_ABORT;
  }
  return 0;
}

int tokamak_handler(struct mg_connection *conn, void *userdata)
{
  time_t now = time(0);
  struct mg_context *ctx = mg_get_context(conn);
  mg_lock_context(ctx);
  int status = 500;
  const struct mg_request_info *ri = mg_get_request_info(conn);
  struct tokamak_POST POST = { 0, 0, 0, 0 };
  if (0 == strcmp("POST", ri->request_method))
  {
    struct mg_form_data_handler f =
      { tokamak_POST_found
      , tokamak_POST_get
      , 0
      , &POST
      };
    mg_handle_form_request(conn, &f);
  }
  if (0 == strcmp("/", ri->local_uri))
  {
    mg_send_mime_file(conn, "index.html", "text/html");
    status = 200;
    goto end;
  }
  else if (0 == strcmp("/tokamak.js", ri->local_uri))
  {
    mg_send_mime_file(conn, "tokamak.js", "text/javascript");
    status = 200;
    goto end;
  }
  else if (0 == strcmp("/tokamak.css", ri->local_uri))
  {
    mg_send_mime_file(conn, "tokamak.css", "text/css");
    status = 200;
    goto end;
  }
  else if (0 == strcmp("/tokamak.png", ri->local_uri))
  {
    mg_send_mime_file(conn, "tokamak.png", "image/png");
    status = 200;
    goto end;
  }
  else if (0 == strcmp("/play", ri->local_uri))
  {
    if (0 != strcmp("POST", ri->request_method) || ! POST.room || ! POST.name)
    {
      mg_send_mime_file(conn, "play.html", "text/html");
      status = 200;
      goto end;
    }
    struct tokamak_room *room = 0;
    for (room = tokamak_server; room; room = room->next)
    {
      if (0 == strcmp(room->name, POST.room))
      {
        break;
      }
    }
    if (! room)
    {
      room = calloc(1, sizeof(*room));
      if (! room)
      {
        status = 507;
        goto end;
      }
      room->players = 0;
      room->name = POST.room; POST.room = 0; // move
      room->password = POST.key ? strdup(POST.key) : 0;
      if (POST.key && ! room->password)
      {
        free(room->name);
        free(room);
        status = 507;
        goto end;
      }
      tokamak_options_default(&room->options);
      room->options.players = 0;
      room->game = 0;
      room->gameover = false;
      room->created = now;
      room->last_seen = now;
      room->next = tokamak_server;
      tokamak_server = room;
    }
    if (room->password)
    {
      if (! (POST.key && 0 == strcmp(room->password, POST.key)))
      {
        mg_send_mime_file(conn, "403.html", "text/html");
        status = 403;
        goto end;
      }
    }
    // room is valid and authenticated
    struct tokamak_player *player = 0;
    for (player = room->players; player; player = player->next)
    {
      if (0 == strcmp(player->name, POST.name))
      {
        break;
      }
    }
    if (! player)
    {
      if (POST.name)
      {
        player = calloc(1, sizeof(*player));
        if (! player)
        {
          status = 507;
          goto end;
        }
        player->room = room;
        player->client = 0;
        player->name = POST.name; POST.name = 0; // move
        player->password = POST.password ? strdup(POST.password) : 0;
        player->auth = tokamak_random64();
        player->host = ! room->players;
        player->playing = false;
        player->order = -1;
        player->played = 0;
        player->won = 0;
        player->created = now;
        player->last_seen = now;
        player->next = room->players;
        room->players = player;
      }
      else
      {
        mg_send_mime_file(conn, "play.html", "text/html");
        status = 200;
        goto end;
      }
    }
    if (player->password)
    {
      if (! (POST.password && 0 == strcmp(player->password, POST.password)))
      {
        mg_send_mime_file(conn, "403.html", "text/html");
        status = 403;
        goto end;
      }
    }
    // player is valid and authenticated
    room->last_seen = now;
    player->last_seen = now;
    char *room_name_uri_escaped = tokamak_URI_escape(room->name);
    if (! room_name_uri_escaped)
    {
      status = 507;
      goto end;
    }
    char *room_name_html_escaped = tokamak_HTML_escape(room->name);
    if (! room_name_html_escaped)
    {
      free(room_name_uri_escaped);
      status = 507;
      goto end;
    }
    char *player_name_uri_escaped = tokamak_URI_escape(player->name);
    if (! player_name_uri_escaped)
    {
      free(room_name_uri_escaped);
      free(room_name_html_escaped);
      status = 507;
      goto end;
    }
    char *player_name_html_escaped = tokamak_HTML_escape(player->name);
    if (! player_name_html_escaped)
    {
      free(room_name_uri_escaped);
      free(room_name_html_escaped);
      free(player_name_uri_escaped);
      status = 507;
      goto end;
    }
    mg_printf
      ( conn
      ,
"HTTP/1.1 200 OK\r\n"
"Content-Type: text/html; charset=UTF-8\r\n"
"\r\n"
"<!DOCTYPE html>\r\n"
"<html><head>\r\n"
"<neta charset='utf-8'>\r\n"
"<title>tokamak</title>\r\n"
"<link rel='stylesheet' href='/tokamak.css'>\r\n"
"<link rel='icon' href='/tokamak.png' type='image/png'>\r\n"
"<script>\r\n"
"tokamak_room = '%s';\r\n"
"tokamak_name = '%s';\r\n"
"tokamak_auth = '%" PRId64 "';\r\n"
"</script>\r\n"
"<script src='/tokamak.js'></script>\r\n"
"</head><body onload='tokamak_load()'>\r\n"
"<h1><a href='/' title='tokamak'>tokamak</a></h1>\r\n"
"<h2><span id='name'>%s</span> @ <span id='room'>%s</span></h2>\r\n"
"<noscript><p>sorry, you need javascript enabled to play</p></noscript>\r\n"
"<div id='tokamak'>loading...</div>\r\n"
"</body></html>\r\n"
      , room_name_uri_escaped
      , player_name_uri_escaped
      , player->auth
      , player_name_html_escaped
      , room_name_html_escaped
      );
    free(room_name_uri_escaped);
    free(room_name_html_escaped);
    free(player_name_uri_escaped);
    free(player_name_html_escaped);
    status = 200;
    goto end;
  }
  else
  {
    mg_send_mime_file(conn, "404.html", "text/html");
    status = 404;
    goto end;
  }
end:
  if (POST.name)
  {
    free(POST.name);
  }
  if (POST.room)
  {
    free(POST.room);
  }
  if (POST.password)
  {
    free(POST.password);
  }
  if (status == 507)
  {
    mg_send_mime_file(conn, "507.html", "text/html");
  }
  mg_unlock_context(ctx);
  return status;
}

enum tokamak_connection_state
{
  tokamak_connection_closing,
  tokamak_connection_connected,
  tokamak_connection_ready
};

struct tokamak_client
{
  struct tokamak_client *next;
  struct mg_connection *conn;
  enum tokamak_connection_state state;
  struct tokamak_player *player;
};

struct tokamak_client *tokamak_clients = 0;

void tokamak_client_disconnect(struct tokamak_client *client)
{
  if (client->state != tokamak_connection_closing)
  {
    client->state = tokamak_connection_closing;
    const char *text = "Connected: 0\r\n";
    mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, text, strlen(text));
    client->conn = 0;
    if (client->player && client->player->room)
    {
      time_t now = time(0);
      client->player->last_seen = now;
      client->player->room->last_seen = now;
      char *player_name_uri_escaped = tokamak_URI_escape(client->player->name);
      if (player_name_uri_escaped)
      {
        size_t part_bytes = strlen(player_name_uri_escaped) + 9;
        char *part_message = calloc(1, part_bytes);
        if (part_message)
        {
          snprintf(part_message, part_bytes, "Part: %s\r\n", player_name_uri_escaped);
          for (struct tokamak_player *player = client->player->room->players; player; player = player->next)
          {
            if (player->client == client)
            {
              continue;
            }
            if (player->client && player->client->state == tokamak_connection_ready)
            {
              mg_websocket_write(player->client->conn, MG_WEBSOCKET_OPCODE_TEXT, part_message, part_bytes - 1);
            }
          }
          free(part_message);
        }
        free(player_name_uri_escaped);
      }
      client->player->client = 0;
      client->player = 0;
    }
  }
}

void tokamak_client_broadcast(struct tokamak_client *client, struct tokamak_player *exclude, const char *message, size_t bytes)
{
  for (struct tokamak_player *player = client->player->room->players; player; player = player->next)
  {
    if (player == exclude)
    {
      continue;
    }
    if (player->client)
    {
      mg_websocket_write(player->client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, bytes);
    }
  }
}

void tokamak_room_broadcast_game(struct tokamak_room *room)
{
  if (room->game)
  {
    int nplayers = tokamak_get_players(room->game);
    int active_player = tokamak_get_player(room->game);
    tokamak_cardset rank_regular = tokamak_move_get_rank_regular(room->game);
    tokamak_cardset rank_joker = tokamak_move_get_rank_joker(room->game);
    tokamak_cardset suit_regular = tokamak_move_get_suit_regular(room->game);
    tokamak_cardset suit_joker = tokamak_move_get_suit_joker(room->game);
    int32_t rank_regular_hi = (rank_regular >> 32) & 0xFFFFffff;
    int32_t rank_regular_lo = rank_regular & 0xFFFFffff;
    int32_t rank_joker_hi = (rank_joker >> 32) & 0xFFFFffff;
    int32_t rank_joker_lo = rank_joker & 0xFFFFffff;
    int32_t suit_regular_hi = (suit_regular >> 32) & 0xFFFFffff;
    int32_t suit_regular_lo = suit_regular & 0xFFFFffff;
    int32_t suit_joker_hi = (suit_joker >> 32) & 0xFFFFffff;
    int32_t suit_joker_lo = suit_joker & 0xFFFFffff;
    char board[128];
    snprintf(board, 128, "Board: X %" PRId32 " %" PRId32 " %" PRId32 " %" PRId32 " %" PRId32 " %" PRId32 " %" PRId32 " %" PRId32 "\r\n", rank_regular_hi, rank_regular_lo, rank_joker_hi, rank_joker_lo, suit_regular_hi, suit_regular_lo, suit_joker_hi, suit_joker_lo);
    size_t board_len = strlen(board);
    for (struct tokamak_player *player = room->players; player; player = player->next)
    {
      if (player->client && player->client->conn)
      {
        char message[64];
        snprintf(message, 64, "Deck: %d\r\n", tokamak_get_deck(room->game));
        mg_websocket_write(player->client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
        for (int p = 0; p < nplayers; ++p)
        {
          if (player->order == p && p == tokamak_get_player(room->game))
          {
            tokamak_cardset regular = tokamak_move_get_hand_regular(room->game);
            int32_t hand_hi = (regular >> 32) & 0xFFFFffff;
            int32_t hand_lo = regular & 0xFFFFffff;
            int hand_jokers = tokamak_move_get_hand_njokers(room->game);
            snprintf(message, 64, "Hand: %d %" PRId32 " %" PRId32 " %d\r\n", p, hand_hi, hand_lo, hand_jokers);
            mg_websocket_write(player->client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
          }
          else
          {
            snprintf(message, 64, "HandSize: %d %d\r\n", p, tokamak_get_hand_size(room->game, p));
            mg_websocket_write(player->client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
          }
        }
        board[7]
          = room->gameover ? '4'
          : player->order != active_player ? '0'
          : tokamak_move_is_unchanged(room->game) ? '1'
          : tokamak_move_is_valid(room->game) ? '3'
          : '2';
        mg_websocket_write(player->client->conn, MG_WEBSOCKET_OPCODE_TEXT, board, board_len);
      }
    }
  }
}

int tokamak_ws_connect(const struct mg_connection *conn, void *userdata)
{
  struct mg_context *ctx = mg_get_context(conn);
  mg_lock_context(ctx);
  struct tokamak_client *client = calloc(1, sizeof(*client));
  if (! client)
  {
    mg_unlock_context(ctx);
    return 1; // reject
  }
  client->next = tokamak_clients;
  client->conn = (struct mg_connection *) conn; // const cast
  client->state = tokamak_connection_connected;
  client->player = 0;
  mg_set_user_connection_data(client->conn, client);
  tokamak_clients = client;
  mg_unlock_context(ctx);
  return 0; // accept
}

void tokamak_ws_ready(struct mg_connection *conn, void *userdata)
{
	const char *text = "Ready: 1\r\n";
	struct tokamak_client *client = mg_get_user_connection_data(conn);
  if (client && client->conn == conn)
  {
    mg_websocket_write(conn, MG_WEBSOCKET_OPCODE_TEXT, text, strlen(text));
    client->state = tokamak_connection_ready;
  }
}

int tokamak_ws_data(struct mg_connection *conn, int bits, char *data, size_t len, void *userdata)
{
  time_t now = time(0);
  struct mg_context *ctx = mg_get_context(conn);
  mg_lock_context(ctx);
	struct tokamak_client *client = mg_get_user_connection_data(conn);
  if (client && client->conn == conn && client->state != tokamak_connection_closing)
  {
    switch (((unsigned char)bits) & 0x0F) {
    case MG_WEBSOCKET_OPCODE_CONTINUATION:
//      fprintf(stdout, "continuation");
      break;
    case MG_WEBSOCKET_OPCODE_TEXT:
      {
        bool ok = false;
        for (size_t o = 0; o < len; ++o)
        {
          if (data[o] == '\r')
          {
            data[o] = 0;
            ok = true;
            break;
          }
        }
        if (ok)
        {
          if (len > 9 && 0 == strncmp("Connect: ", data, 9))
          {
            char *room_name_uri_escaped = data + 9;
            char *sep;
            char *player_name_uri_escaped = 0;
            for (sep = room_name_uri_escaped; *sep; ++sep)
            {
              if (*sep == ' ')
              {
                *sep = 0;
                player_name_uri_escaped = sep + 1;
                break;
              }
            }
            if (! player_name_uri_escaped)
            {
              tokamak_client_disconnect(client);
              break;
            }
            char *auth_str = 0;
            for (sep = player_name_uri_escaped; *sep; ++sep)
            {
              if (*sep == ' ')
              {
                *sep = 0;
                auth_str = sep + 1;
                break;
              }
            }
            if (! auth_str)
            {
              tokamak_client_disconnect(client);
              break;
            }
            char *eol = 0;
            int64_t auth = strtoll(auth_str, &eol, 10);
            if (! (eol && *eol == 0))
            {
              break;
            }
            char *room_name = tokamak_URI_unescape(room_name_uri_escaped);
            if (! room_name)
            {
              tokamak_client_disconnect(client);
              break;
            }
            char *player_name = tokamak_URI_unescape(player_name_uri_escaped);
            if (! player_name)
            {
              free(room_name);
              tokamak_client_disconnect(client);
              break;
            }
            struct tokamak_room *room = 0;
            for (room = tokamak_server; room; room = room->next)
            {
              if (0 == strcmp(room->name, room_name))
              {
                break;
              }
            }
            free(room_name);
            if (! room)
            {
              free(player_name);
              tokamak_client_disconnect(client);
              break;
            }
            struct tokamak_player *player = 0;
            for (player = room->players; player; player = player->next)
            {
              if (0 == strcmp(player->name, player_name))
              {
                break;
              }
            }
            free(player_name);
            if (! player)
            {
              tokamak_client_disconnect(client);
              break;
            }
            if (player->auth != auth)
            {
              tokamak_client_disconnect(client);
              break;
            }
            if (player->client)
            {
              tokamak_client_disconnect(player->client);
            }
            player->client = client;
            client->player = player;
            player->last_seen = now;
            room->last_seen = now;
            const char *text = "Connected: 1\r\n";
            mg_websocket_write(conn, MG_WEBSOCKET_OPCODE_TEXT, text, strlen(text));
            // tell new player room options
            {
              char message[64];
              snprintf(message, 64, "Ranks: %d\r\n", room->options.ranks);
              mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
              snprintf(message, 64, "Suits: %d\r\n", room->options.suits);
              mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
              snprintf(message, 64, "Jokers: %d\r\n", room->options.jokers);
              mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
              snprintf(message, 64, "Pile: %d\r\n", room->options.pile);
              mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
              snprintf(message, 64, "Deal: %d\r\n", room->options.deal);
              mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
            }
            // tell new player all players (apart from new player)
            for (player = room->players; player; player = player->next)
            {
              if (player->client == client)
              {
                continue;
              }
              char *player_name_uri_escaped = tokamak_URI_escape(player->name);
              if (player_name_uri_escaped)
              {
                size_t bytes = strlen(player_name_uri_escaped) + 13;
                char *message = calloc(1, bytes);
                if (message)
                {
                  snprintf(message, bytes, "Join: %s %c %c\r\n", player_name_uri_escaped, player->host ? '1' : '0', player->playing ? '1' : '0');
                  mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, bytes - 1);
                  if (! player->client)
                  {
                    snprintf(message, bytes, "Part: %s\r\n", player_name_uri_escaped);
                    mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, bytes - 5);
                  }
                  free(message);
                }
                free(player_name_uri_escaped);
              }
            }
            // tell all players (including new player) new player
            char *player_name_uri_escaped2 = tokamak_URI_escape(client->player->name);
            if (player_name_uri_escaped2)
            {
              size_t join_bytes = strlen(player_name_uri_escaped2) + 13;
              char *join_message = calloc(1, join_bytes);
              if (join_message)
              {
                snprintf(join_message, join_bytes, "Join: %s %c %c\r\n", player_name_uri_escaped2, client->player->host ? '1' : '0', client->player->playing ? '1' : '0');
                for (player = room->players; player; player = player->next)
                {
                  if (player->client)
                  {
                    mg_websocket_write(player->client->conn, MG_WEBSOCKET_OPCODE_TEXT, join_message, join_bytes - 1);
                  }
                }
                free(join_message);
              }
              free(player_name_uri_escaped2);
            }
            {
              char message[64];
              snprintf(message, 64, "State: %d\r\n", (int) tokamak_room_get_state(room));
              mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, message, strlen(message));
            }
            if (tokamak_room_get_state(room) == tokamak_room_state_gaming)
            {
              tokamak_room_broadcast_game(room);
            }
          }
          else if (len > 6 && 0 == strncmp("Play: ", data, 6) && client->player && client->player->room && tokamak_room_get_state(client->player->room) != tokamak_room_state_gaming)
          {
            char *end = 0;
            int64_t playing = strtoll(data + 6, &end, 10);
            struct tokamak_player *player = 0;
            if (end && *end == ' ')
            {
              char *player_name = tokamak_URI_unescape(end + 1);
              if (player_name)
              {
                for (player = client->player->room->players; player; player = player->next)
                {
                  if (0 == strcmp(player->name, player_name))
                  {
                    break;
                  }
                }
                free(player_name);
              }
            }
            else if (end && *end == 0)
            {
              player = client->player;
            }
            if (player)
            {
              if (playing && ! player->playing)
              {
                player->room->options.players += 1;
              }
              if (! playing && player->playing)
              {
                player->room->options.players -= 1;
              }
              player->playing = playing;
              // tell all players (not including client) player state
              char *player_name_uri_escaped = tokamak_URI_escape(player->name);
              if (player_name_uri_escaped)
              {
                size_t play_bytes = strlen(player_name_uri_escaped) + 11;
                char *play_message = calloc(1, play_bytes);
                if (play_message)
                {
                  snprintf(play_message, play_bytes, "Play: %s %c\r\n", player_name_uri_escaped, player->playing ? '1' : '0');
                  tokamak_client_broadcast(client, client->player, play_message, play_bytes - 1);
                  free(play_message);
                }
                free(player_name_uri_escaped);
              }
              char message[64];
              snprintf(message, 64, "State: %d\r\n", (int) tokamak_room_get_state(player->room));
              tokamak_client_broadcast(client, 0, message, strlen(message));
              if (tokamak_room_get_state(player->room) == tokamak_room_state_gaming)
              {
                tokamak_room_broadcast_game(player->room);
              }
            }
          }
          else if (len > 7 && 0 == strncmp("Ranks: ", data, 7) && client->player && client->player->room && tokamak_room_get_state(client->player->room) != tokamak_room_state_gaming)
          {
            char *end = 0;
            int64_t ranks = strtoll(data + 7, &end, 10);
            if (end && *end == 0 && 2 <= ranks && ranks <= 32)
            {
              client->player->room->options.ranks = ranks;
              char message[64];
              snprintf(message, 64, "Ranks: %d\r\n", client->player->room->options.ranks);
              tokamak_client_broadcast(client, client->player, message, strlen(message));
              snprintf(message, 64, "State: %d\r\n", (int) tokamak_room_get_state(client->player->room));
              tokamak_client_broadcast(client, 0, message, strlen(message));
            }
          }
          else if (len > 7 && 0 == strncmp("Suits: ", data, 7) && client->player && client->player->room && tokamak_room_get_state(client->player->room) != tokamak_room_state_gaming)
          {
            char *end = 0;
            int64_t suits = strtoll(data + 7, &end, 10);
            if (end && *end == 0 && 2 <= suits && suits <= 32)
            {
              client->player->room->options.suits = suits;
              char message[64];
              snprintf(message, 64, "Suits: %d\r\n", client->player->room->options.suits);
              tokamak_client_broadcast(client, client->player, message, strlen(message));
              snprintf(message, 64, "State: %d\r\n", (int) tokamak_room_get_state(client->player->room));
              tokamak_client_broadcast(client, 0, message, strlen(message));
            }
          }
          else if (len > 8 && 0 == strncmp("Jokers: ", data, 8) && client->player && client->player->room && tokamak_room_get_state(client->player->room) != tokamak_room_state_gaming)
          {
            char *end = 0;
            int64_t jokers = strtoll(data + 8, &end, 10);
            if (end && *end == 0 && 0 <= jokers && jokers <= 60)
            {
              client->player->room->options.jokers = jokers;
              char message[64];
              snprintf(message, 64, "Jokers: %d\r\n", client->player->room->options.jokers);
              tokamak_client_broadcast(client, client->player, message, strlen(message));
              snprintf(message, 64, "State: %d\r\n", (int) tokamak_room_get_state(client->player->room));
              tokamak_client_broadcast(client, 0, message, strlen(message));
            }
          }
          else if (len > 6 && 0 == strncmp("Pile: ", data, 6) && client->player && client->player->room && tokamak_room_get_state(client->player->room) != tokamak_room_state_gaming)
          {
            char *end = 0;
            int64_t pile = strtoll(data + 6, &end, 10);
            if (end && *end == 0 && 2 <= pile && pile <= 32)
            {
              client->player->room->options.pile = pile;
              char message[64];
              snprintf(message, 64, "Pile: %d\r\n", client->player->room->options.pile);
              tokamak_client_broadcast(client, client->player, message, strlen(message));
              snprintf(message, 64, "State: %d\r\n", (int) tokamak_room_get_state(client->player->room));
              tokamak_client_broadcast(client, 0, message, strlen(message));
            }
          }
          else if (len > 6 && 0 == strncmp("Deal: ", data, 6) && client->player && client->player->room && tokamak_room_get_state(client->player->room) != tokamak_room_state_gaming)
          {
            char *end = 0;
            int64_t deal = strtoll(data + 6, &end, 10);
            if (end && *end == 0 && 0 <= deal && deal <= 32)
            {
              client->player->room->options.deal = deal;
              char message[64];
              snprintf(message, 64, "Deal: %d\r\n", client->player->room->options.deal);
              tokamak_client_broadcast(client, client->player, message, strlen(message));
              snprintf(message, 64, "State: %d\r\n", (int) tokamak_room_get_state(client->player->room));
              tokamak_client_broadcast(client, 0, message, strlen(message));
            }
          }
          else if (0 == strcmp("Start: 1", data) && client->player && client->player->room && tokamak_room_get_state(client->player->room) == tokamak_room_state_valid)
          {
            if (client->player->room->game)
            {
              tokamak_delete(client->player->room->game);
              client->player->room->gameover = false;
            }
            client->player->room->game = tokamak_new(&client->player->room->options);
            if (client->player->room->game)
            {
              struct tokamak_player_order *order = calloc(1, client->player->room->options.players * sizeof(*order));
              if (order)
              {
                struct tokamak_player_order *o = order;
                for (struct tokamak_player *player = client->player->room->players; player; player = player->next)
                {
                  if (! player->playing)
                  {
                    player->order = -1;
                    continue;
                  }
                  o->player = player;
                  o->order = tokamak_random64();
                  o++;
                }
                qsort(order, client->player->room->options.players, sizeof(*order), tokamak_player_order_cmp);
                for (int p = 0; p < client->player->room->options.players; ++p)
                {
                  order[p].player->order = p;
                }
                free(order);
                tokamak_shuffle(client->player->room->game);
                tokamak_deal(client->player->room->game);
                const char *message = "State: 2\r\n";
                tokamak_client_broadcast(client, 0, message, strlen(message));
                tokamak_room_broadcast_game(client->player->room);
              }
            }
          }
          else if (0 == strcmp("Stop: 1", data) && client->player && client->player->room && tokamak_room_get_state(client->player->room) == tokamak_room_state_gaming)
          {
            if (client->player->host || client->player->order >= 0)
            {
              client->player->room->gameover = true;
              const char *message = "State: 1\r\n";
              tokamak_client_broadcast(client, 0, message, strlen(message));
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (len > 23 && 0 == strncmp("MoveRankRegularToHand: ", data, 23) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            char *end = 0;
            int64_t card = strtoll(data + 23, &end, 10);
            if (end && *end == 0 && tokamak_move_rank_regular_to_hand(client->player->room->game, card))
            {
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (len > 23 && 0 == strncmp("MoveSuitRegularToHand: ", data, 23) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            char *end = 0;
            int64_t card = strtoll(data + 23, &end, 10);
            if (end && *end == 0 && tokamak_move_suit_regular_to_hand(client->player->room->game, card))
            {
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (len > 21 && 0 == strncmp("MoveRankJokerToHand: ", data, 21) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            char *end = 0;
            int64_t card = strtoll(data + 21, &end, 10);
            if (end && *end == 0 && tokamak_move_rank_joker_to_hand(client->player->room->game, card))
            {
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (len > 21 && 0 == strncmp("MoveSuitJokerToHand: ", data, 21) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            char *end = 0;
            int64_t card = strtoll(data + 21, &end, 10);
            if (end && *end == 0 && tokamak_move_suit_joker_to_hand(client->player->room->game, card))
            {
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (len > 23 && 0 == strncmp("MoveHandToRankRegular: ", data, 23) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            char *end = 0;
            int64_t card = strtoll(data + 23, &end, 10);
            if (end && *end == 0 && tokamak_move_hand_to_rank_regular(client->player->room->game, card))
            {
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (len > 23 && 0 == strncmp("MoveHandToSuitRegular: ", data, 23) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            char *end = 0;
            int64_t card = strtoll(data + 23, &end, 10);
            if (end && *end == 0 && tokamak_move_hand_to_suit_regular(client->player->room->game, card))
            {
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (len > 21 && 0 == strncmp("MoveHandToRankJoker: ", data, 21) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            char *end = 0;
            int64_t card = strtoll(data + 21, &end, 10);
            if (end && *end == 0 && tokamak_move_hand_to_rank_joker(client->player->room->game, card))
            {
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (len > 21 && 0 == strncmp("MoveHandToSuitJoker: ", data, 21) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            char *end = 0;
            int64_t card = strtoll(data + 21, &end, 10);
            if (end && *end == 0 && tokamak_move_hand_to_suit_joker(client->player->room->game, card))
            {
              tokamak_room_broadcast_game(client->player->room);
            }
          }
          else if (0 == strcmp("Rollback: 1", data) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            tokamak_move_rollback(client->player->room->game);
            tokamak_room_broadcast_game(client->player->room);
          }
          else if (0 == strcmp("Commit: 1", data) && client->player && client->player->room && client->player->room->game && ! client->player->room->gameover && client->player->order == tokamak_get_player(client->player->room->game))
          {
            switch (tokamak_move_commit(client->player->room->game))
            {
              case tokamak_commit_winner:
                client->player->won++;
                /* fall-through */
              case tokamak_commit_stalemate:
                for (struct tokamak_player *player = client->player->room->players; player; player = player->next)
                {
                  if (player->order >= 0)
                  {
                    player->played++;
                  }
                }
                client->player->room->gameover = true;
                break;
              case tokamak_commit_ok:
                break;
              case tokamak_commit_invalid:
                break;
            }
            tokamak_room_broadcast_game(client->player->room);
            char message[64];
            snprintf(message, 64, "State: %d\r\n", (int) tokamak_room_get_state(client->player->room));
            tokamak_client_broadcast(client, 0, message, strlen(message));
          }
        }
      }
//      fprintf(stdout, "text");
      break;
    case MG_WEBSOCKET_OPCODE_BINARY:
//      fprintf(stdout, "binary");
      break;
    case MG_WEBSOCKET_OPCODE_CONNECTION_CLOSE:
//      fprintf(stdout, "close");
      break;
    case MG_WEBSOCKET_OPCODE_PING:
//      fprintf(stdout, "ping");
      break;
    case MG_WEBSOCKET_OPCODE_PONG:
//      fprintf(stdout, "pong");
      break;
    default:
//      fprintf(stdout, "unknown(%1xh)", ((unsigned char)bits) & 0x0F);
      break;
    }
//    fprintf(stdout, " data:[");
//    fwrite(data, len, 1, stdout);
//    fprintf(stdout, "]\n");
  }
  mg_unlock_context(ctx);
	return 1;
}

void tokamak_ws_close(const struct mg_connection *conn, void *userdata)
{
	struct mg_context *ctx = mg_get_context(conn);
  mg_lock_context(ctx);
  struct tokamak_client *client = mg_get_user_connection_data(conn);
  if (client && client->conn == conn)
  {
    tokamak_client_disconnect(client);
  }
  mg_unlock_context(ctx);
}

void tokamak_ws_update(struct mg_context *ctx)
{
  static int64_t counter = 0;
  char text[64];
  snprintf(text, sizeof(text), "Uptime: %" PRId64, ++counter);
  const size_t textlen = strlen(text);
  mg_lock_context(ctx);
  for (struct tokamak_client *client = tokamak_clients; client; client = client->next)
  {
    if (client->state == tokamak_connection_ready)
    {
			mg_websocket_write(client->conn, MG_WEBSOCKET_OPCODE_TEXT, text, textlen);
    }
  }
  mg_unlock_context(ctx);
}

#if 0
/
/play
/play/room
  - if no name cookie
    - display name form
    - post form -> set name cookie, 303 to /play/room
  - if name cookie
    - if room locked and no auth cookie
      - display password form
      - post form -> if correct then set auth cookie, 303 to /play/room else 403
    - insert if not exists name into room
      - first participant gets host role
      - host can lock room with password
      - host can eject people
      - host can ban people ?
    - if no game in progress
      - list room participants
      - game configuration menu, all room participants can modify
      - participants can opt in to playing the next game
      - option to add any number of AndreI players
      - button to start game
    - if game in progress
      - show game board visible to each player
      - if my turn
        - allow interaction with the board
      - if not my turn
        - show board updates
    - if game finished
      - show scoreboard, wait for all players to confirm or timeout
#endif

int main(int argc, char **argv)
{
  const char *program = "tokamak-server";
  const char *port = "9002";
  if (argc > 0)
  {
    program = argv[0];
  }
  if (argc > 1)
  {
    port = argv[1];
  }
  fprintf(stderr, "INFO: %s starting on port %s\n", program, port);
  mg_init_library(MG_FEATURES_DEFAULT);
  int retval = 2;
  const char *options[] =
    { "listening_ports"
    , port
    , "request_timeout_ms"
    , "10000"
    , "websocket_timeout_ms"
    , "3600000" // 1 hour
    , "error_log_file"
    , "error.log"
    , 0
    };
  struct mg_callbacks callbacks = {0};
  struct mg_context *ctx = 0;
  if ((ctx = mg_start(&callbacks, 0, options)))
  {
    mg_set_request_handler(ctx, "/", tokamak_handler, 0);
    mg_set_websocket_handler(ctx, "/ws", tokamak_ws_connect, tokamak_ws_ready, tokamak_ws_data, tokamak_ws_close, 0);
    fprintf(stderr, "INFO: %s started\n", program);
    while (running)
    {
      tokamak_ws_update(ctx);
      sleep(60);
    }
    retval = 0;
    fprintf(stderr, "INFO: %s stopping\n", program);
    mg_stop(ctx);
  }
  else
  {
    retval = 1;
    fprintf(stderr, "ERROR: could not start web server\n");
  }
  mg_exit_library();
  fprintf(stderr, "INFO: %s stopped\n", program);
  return retval;
}
