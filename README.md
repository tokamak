# tokamak

Copyright (c) 2021

idea by Laura Netz

code by Claude Heiland-Allen

# cards

0. 54 cards in the deck (4 suits of 13 ranks, plus 2 jokers)

1. the table holds any number of piles of cards

2. each pile must have at least 3 cards

3. each pile must be related

    - a run of adjacent cards in the same suit;

      aces can be high and low simultaneously;

      e.g.: 🃎 🃁 🃂

    - the same rank in different suits;

      e.g.: 🂮 🂾 🃎

    - jokers can be used to stand in for any card;

      even cards that are already on the table;

      e.g. 🃒 🂢 🃟 + 🂡 🃟 🂣

4. players make moves in turn

    - add any number of cards from their hand to the table,
      re-arranging cards between piles in any way such that the
      table is still valid at the end of the turn;

      jokers may be moved from the table to the player's hand as part of
      the rearrangement provided the table is valid at the end of the turn

    - draw a card from the deck if no cards are played on the table

5. the game starts with each player dealt 5 cards

6. the game ends

    - when the first player has no cards left in their hand (winner)

    - when the deck is exhausted and no move can be made (stalemate)

# ncurses

ncurses is a library for making interactive programs using text terminals.
`tokamak-ncurses` is an implementation of tokamak using ncurses.

## build

```
make clean
make
```

### in english { #build-en }

```
make clean
make DEFAULT_LANGUAGE="en"
```

### en català { #build-ca }

```
make clean
make DEFAULT_LANGUAGE="ca"
```

## run

```
./tokamak-ncurses
```

### in english { #run-en }

```
LANG="en_GB.UTF-8" ./tokamak-ncurses
```

### en català { #run-ca }

```
LANG="ca_ES.UTF-8" ./tokamak-ncurses
```

## title screen

- press any key to continue

## main menu

- `Q` key exits the program.

- cursor arrow keys move the selection shown with `**`

- any other key activates the selection

## friendly

play a single round

## tournament

play rounds continuously, keeping score

## players menu

- cursor arrow keys move the selection shown with `**`

- any other key activates the selection

### solo

single player

### versus

two players

### trio

three players

### quartet

four players

## player type menu

- cursor keys move the selection shown with `**`

- `Enter` key activates the selection

- any other key changes the option

### human

interactive play

### random

an artificial intelligence that searches for moves at random

### brute

an artificial intelligence that searches for moves methodically

### andrei

an artificial intelligence that searches for moves heuristically

### mcts

an artificial intelligence using Monte-Carlo Tree Search algorithm

## arena menu

- cursor arrow keys move the selection shown with `**`

- any other key activates the selection

### classic

classic tokamak playable with a regular deck of 52 cards plus 2 jokers

two player games last up to 10 minutes

### wild

classic tokamak with 10 extra jokers

### tiny

tiny tokamak with a minimum pile size of 2

winning this quick game is more a matter of luck than skill

### beast

medium tokamak with a minimum pile size of 4

## playing

- the tokamak is arranged in a grid

  - vertical piles (same rank) are shown in red

    (top left subcell of each grid cell)

  - horizontal piles (same suit) are shown in blue

    (bottom right subcell of each grid cell)

  - current player's hand is shown in green

    (bottom left subcell of each grid cell)

  - empty subcells are shown with `::`

  - regular cards are shown with suit and rank

    e.g. 🂢 🂷 🃍 🃑 `6@` `X#`

  - jokers are shown with 🃟

- the cursor is shown with `**`

  - all actions take place at the cursor

  - cursor arrow keys move the cursor, wrapping at the edges

  - jokers in the hand follow the cursor

    (it is not visible if there is more than one joker in the hand,
    but you can count visible hand cards and compare with the total)

- `PageDown` key moves a card from the hand to the piles

  - this may reclaim a joker back to the hand as a side effect

- `PageUp` key moves a card from the piles to the hand

- `Space` key swaps the suit wise cell with the rank wise cell

- `Enter` key commits the move if it is valid

  - if nothing has changed then the player draws a card

  - control passes to the next player in turn

- `Backspace` key rolls back all changes

- `P` key pauses the game

- `Q` key abandons the game

# legal

tokamak card game

Copyright (C) 2021 Laura Netz

Copyright (C) 2021 Claude Heiland-Allen

## software license

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## documentation license

This document is a free culture work under the terms of the
Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)
license.

This is a human-readable summary of (and not a substitute for) the license.

You are free to:

- Share — copy and redistribute the material in any medium or format

- Adapt — remix, transform, and build upon the material for any purpose,
  even commercially.

Under the following terms:

- Attribution — You must give appropriate credit, provide a link to the
  license, and indicate if changes were made. You may do so in any
  reasonable manner, but not in any way that suggests the licensor
  endorses you or your use.

- ShareAlike — If you remix, transform, or build upon the material, you
  must distribute your contributions under the same license as the original.

- No additional restrictions — You may not apply legal terms or technological
  measures that legally restrict others from doing anything the license permits.

The licensor cannot revoke these freedoms as long as you follow the
license terms.

You should have received a copy of the license code along with this document.
If not, see <https://creativecommons.org/licenses/by-sa/4.0/>.
