#ifndef LIBTOKAMAK_H
#define LIBTOKAMAK_H 1

#include <stdbool.h>
#include <stdint.h>

#ifdef HAVE_CLOCK
#include <time.h>
typedef const struct timespec *deadline_t;
#else
typedef volatile bool *deadline_t;
#endif

#define TOKAMAK_BITS 64
typedef struct tokamak_s tokamak;
typedef struct tokamak_table_s tokamak_table;
typedef int8_t tokamak_card;
typedef uint64_t tokamak_cardset;
typedef enum
{
  tokamak_commit_ok = 0,
  tokamak_commit_invalid,
  tokamak_commit_stalemate,
  tokamak_commit_winner
} tokamak_commit;

typedef struct
{
  int ranks;
  int suits;
  int jokers;
  int pile;
  int deal;
  int players;
  int starter;
} tokamak_options;

void tokamak_options_default(tokamak_options *O);
bool tokamak_options_valid(const tokamak_options *O);

struct tokamak_table_s
{
  tokamak_cardset rank_regular;
  tokamak_cardset rank_joker;
  tokamak_cardset suit_regular;
  tokamak_cardset suit_joker;
  tokamak_cardset hand_regular;
  tokamak_card hand_njokers;
};

struct tokamak_s
{
  tokamak_card ranks;
  tokamak_card suits;
  tokamak_card jokers;
  tokamak_card pile;
  tokamak_card deal;
  tokamak_card players;
  tokamak_card deck[TOKAMAK_BITS];
  tokamak_card ndeck;
  tokamak_cardset hand[TOKAMAK_BITS];
  tokamak_card player;
  tokamak_table table[2];
  // cached
  bool move_is_valid_known;
  bool move_is_valid;
  bool move_is_unchanged_known;
  bool move_is_unchanged;
};

tokamak *tokamak_new(const tokamak_options *O);
bool tokamak_init(tokamak *T, const tokamak_options *O);
void tokamak_delete(tokamak *T);

int tokamak_get_ranks(const tokamak *T);
int tokamak_get_suits(const tokamak *T);
int tokamak_get_jokers(const tokamak *T);
int tokamak_get_pile(const tokamak *T);
int tokamak_get_deal(const tokamak *T);
int tokamak_get_players(const tokamak *T);
int tokamak_get_regular(const tokamak *T);
int tokamak_get_cards(const tokamak *T);

void tokamak_restore(tokamak *T, tokamak_cardset deck, tokamak_cardset rank_regular, tokamak_cardset rank_joker, tokamak_cardset suit_regular, tokamak_cardset suit_joker, int player);
void tokamak_restore_hand(tokamak *T, int player, tokamak_cardset hand_regular, int hand_njokers);
void tokamak_restore_hand_size(tokamak *T, int player, int ncards);

tokamak_card tokamak_card_invalid(const tokamak *T);
tokamak_card tokamak_card_regular(const tokamak *T, int suit, int rank);
tokamak_card tokamak_card_joker(const tokamak *T, int joker);
bool tokamak_card_is_valid(const tokamak *T, tokamak_card card);
bool tokamak_card_is_regular(const tokamak *T, tokamak_card card);
bool tokamak_card_is_joker(const tokamak *T, tokamak_card card);
int tokamak_card_get_rank(const tokamak *T, tokamak_card card);
int tokamak_card_get_suit(const tokamak *T, tokamak_card card);

tokamak_cardset tokamak_cardset_empty(const tokamak *T);
tokamak_cardset tokamak_cardset_regular(const tokamak *T, int suit, int rank);
int tokamak_cardset_size(const tokamak *T, tokamak_cardset set);
bool tokamak_cardset_is_empty(const tokamak *T, tokamak_cardset set);
bool tokamak_cardset_contains(const tokamak *T, tokamak_cardset set, tokamak_card card);
tokamak_cardset tokamak_cardset_insert(const tokamak *T, tokamak_cardset set, tokamak_card card);
tokamak_cardset tokamak_cardset_remove(const tokamak *T, tokamak_cardset set, tokamak_card card);
tokamak_cardset tokamak_cardset_singleton(const tokamak *T, tokamak_card card);

void tokamak_shuffle(tokamak *T);
void tokamak_deal(tokamak *T);
bool tokamak_move_is_valid(tokamak *T);
bool tokamak_move_is_unchanged(tokamak *T);
void tokamak_move_rollback(tokamak *T);
tokamak_commit tokamak_move_commit(tokamak *T);

tokamak_cardset tokamak_get_hand(const tokamak *T, int player);
tokamak_cardset tokamak_get_rank_regular(const tokamak *T);
tokamak_cardset tokamak_get_rank_joker(const tokamak *T);
tokamak_cardset tokamak_get_suit_regular(const tokamak *T);
tokamak_cardset tokamak_get_suit_joker(const tokamak *T);

tokamak_cardset tokamak_move_get_hand_regular(const tokamak *T);
tokamak_card tokamak_move_get_hand_njokers(const tokamak *T);
tokamak_cardset tokamak_move_get_rank_regular(const tokamak *T);
tokamak_cardset tokamak_move_get_rank_joker(const tokamak *T);
tokamak_cardset tokamak_move_get_suit_regular(const tokamak *T);
tokamak_cardset tokamak_move_get_suit_joker(const tokamak *T);

void tokamak_move_set_hand_regular(tokamak *T, tokamak_cardset c);
void tokamak_move_set_hand_njokers(tokamak *T, tokamak_card c);
void tokamak_move_set_rank_regular(tokamak *T, tokamak_cardset c);
void tokamak_move_set_rank_joker(tokamak *T, tokamak_cardset c);
void tokamak_move_set_suit_regular(tokamak *T, tokamak_cardset c);
void tokamak_move_set_suit_joker(tokamak *T, tokamak_cardset c);

bool tokamak_move_rank_regular_to_hand(tokamak *T, tokamak_card card);
bool tokamak_move_rank_joker_to_hand(tokamak *T, tokamak_card card);
bool tokamak_move_suit_regular_to_hand(tokamak *T, tokamak_card card);
bool tokamak_move_suit_joker_to_hand(tokamak *T, tokamak_card card);
bool tokamak_move_hand_to_rank_regular(tokamak *T, tokamak_card card);
bool tokamak_move_hand_to_rank_joker(tokamak *T, tokamak_card card);
bool tokamak_move_hand_to_suit_regular(tokamak *T, tokamak_card card);
bool tokamak_move_hand_to_suit_joker(tokamak *T, tokamak_card card);

bool tokamak_move_swap_suit_rank(tokamak *T, int suit, int rank);
bool tokamak_move_play_from_hand(tokamak *T, int suit, int rank);
bool tokamak_move_play_to_hand(tokamak *T, int suit, int rank);
int tokamak_get_deck(const tokamak *T);
int tokamak_get_player(const tokamak *T);
int tokamak_get_hand_size(const tokamak *T, int player);

#ifdef TOKAMAK_AI_RANDOM
int tokamak_ai_random_search(tokamak *T, deadline_t deadline);
int tokamak_ai_random_search_parallel(tokamak *T, deadline_t deadline);
#endif

#ifdef TOKAMAK_AI_BRUTE
int tokamak_ai_brute_search_serial(tokamak *T, deadline_t deadline);
int tokamak_ai_brute_search_parallel(tokamak *T, deadline_t deadline);
#endif

#ifdef TOKAMAK_AI_ANDREI
typedef void (*visitor)(void *arg, const tokamak *T);
int tokamak_ai_andrei_visit(tokamak *T, deadline_t deadline, void *arg, visitor visit);
int tokamak_ai_andrei(tokamak *T, deadline_t deadline);
#endif

#ifdef TOKAMAK_AI_QUANTUM
int tokamak_ai_quantum(tokamak *T, deadline_t deadline);
#endif

#ifdef TOKAMAK_AI_MCTS
double tokamak_ai_mcts(tokamak *T, deadline_t deadline);
#endif

#ifdef TOKAMAK_AI_QUANTUM
int tokamak_ai_quantum(tokamak *T, deadline_t deadline);
#endif

#endif
