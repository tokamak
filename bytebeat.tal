( sierp2ring.tal 2022a14-2214 - hopefully working! )
( restrictions on BUFFERSIZE:

    - must be multiple of 0x0200, the SDL audio buffer length
    - with this implementation, must be a power of 2 to make calculations in on-frame work
    - minimum 0x0800 due to around 0x02df samples being heard each frame: using 0x0400 means that only 0x0200 samples would be generated each frame
    - higher numbers like 0x1000 might be more forgiving when Varvara is also busy doing other stuff
)

%BUFFERSIZE { #0800 } ( #02df is 1/60th of a second @44100Hz )
%BUFFER { $0800 }
%BUFFERSIZE-1 { BUFFERSIZE #0001 SUB2 }
%HALF-BUFFERSIZE { BUFFERSIZE #01 SFT2 }
%HALF-BUFFERSIZE-1 { BUFFERSIZE #01 SFT2 #0001 SUB2 }

@bytebeat
&setup ( function* buffer/l* buffer/r* -- )
	,&r STR2
	,&l STR2
	,&function STR2
	( set playback parameters )
	,&l LDR2 .AudioBB/addr DEO2
	,&r LDR2 .AudioBB/addr #10 ADD DEO2
	BUFFERSIZE .AudioBB/length DEO2k #10 ADD DEO2
	#0000 .AudioBB/adsr DEO2k #10 ADD DEO2
	#f0 .AudioBB/volume DEO
	#0f .AudioBB/volume #10 ADD DEO
	( fill buffers )
	;&render-half-buffer JSR2
	;&render-half-buffer JSR2
	( start playback )
	#3c .AudioBB/pitch DEOk #10 ADD DEO
	JMP2r

&on-frame
	( which half is the audio playing in? )
	.AudioBB/position DEI2 HALF-BUFFERSIZE AND2
	( which half would we render next? )
	;&t LDA2 HALF-BUFFERSIZE AND2
	( if they're the same half, we shouldn't render )
	EQU2 ,&skip JCN
	;&render-half-buffer JSR2
	&skip
	JMP2r

&buffer [ &l $2 &r $2 ]
&function $2

&render-half-buffer ( -- )
	LIT2 &t 0000
	&loop ( [t* )
		DUP2 BUFFERSIZE-1 AND2 ( [t* sampleBufferIndex* )
		STH2 ( [t* / [sampleBufferIndex* )
		( actual bytebeat is computed here )
		,&function LDR2 JSR2
		( t* l r / [sampleBufferIndex* )
		( write outputs to buffers )
		,&r LDR2 STH2rk ADD2 STA
		,&l LDR2 STH2r  ADD2 STA
		( check if enough and loop )
		INC2 ( [t++* )
		DUP2 HALF-BUFFERSIZE-1 AND2 ORA ,&loop JCN
	,&t STR2
	JMP2r
