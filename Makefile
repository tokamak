DEFAULT_LANGUAGE = en

all: tokamak.rom tokamak-ncurses tokamak-client-ncurses tokamak-server tokamak-server-websocket tokamak.pdf index.html

clean:
	-rm tokamak-ncurses tokamak.pdf index.html

run-ncurses: tokamak-ncurses
	OMP_STACK_SIZE="16M" ./tokamak-ncurses

run: tokamak.rom
	uxnemu -s 3 tokamak.rom

.PHONY: all clean run-ncurses run

tokamak-ncurses: tokamak-ncurses.c libtokamak.c libtokamak.h Makefile
	gcc -o tokamak-ncurses tokamak-ncurses.c libtokamak.c -std=c11 -D_POSIX_C_SOURCE=200809L -Wall -Wextra -pedantic -O3 -fopenmp -DHAVE_OPENMP -DHAVE_CLOCK -lncursesw -lm -I/usr/include/ncursesw -DDEFAULT_LANGUAGE="\"$(DEFAULT_LANGUAGE)\"" -ggdb -DTOKAMAK_AI_RANDOM -DTOKAMAK_AI_BRUTE -DTOKAMAK_AI_ANDREI -DTOKAMAK_AI_MCTS

tokamak-client-ncurses: tokamak-client-ncurses.c libtokamak.c libtokamak.h libtokamak-client.c libtokamak-client.h Makefile
	gcc -o tokamak-client-ncurses tokamak-client-ncurses.c libtokamak.c libtokamak-client.c -std=c11 -D_POSIX_C_SOURCE=200809L -Wall -Wextra -pedantic -O3 -fopenmp -DHAVE_OPENMP -DHAVE_CLOCK -lncursesw -lm -I/usr/include/ncursesw -lcurl -DDEFAULT_LANGUAGE="\"$(DEFAULT_LANGUAGE)\"" -ggdb -DTOKAMAK_AI_RANDOM -DTOKAMAK_AI_BRUTE -DTOKAMAK_AI_ANDREI -DTOKAMAK_AI_MCTS

tokamak-server: tokamak-server.c libtokamak.c libtokamak.h Makefile
	gcc -o tokamak-server tokamak-server.c libtokamak.c -std=c11 -D_POSIX_C_SOURCE=200809L -Wall -Wextra -pedantic -O3 -fopenmp -DHAVE_OPENMP -DHAVE_CLOCK -lm -lsqlite3 -lcivetweb -DDEFAULT_LANGUAGE="\"$(DEFAULT_LANGUAGE)\"" -ggdb

tokamak-server-websocket: tokamak-server-websocket.c libtokamak.c libtokamak.h Makefile
	gcc -o tokamak-server-websocket tokamak-server-websocket.c libtokamak.c -std=c11 -D_POSIX_C_SOURCE=200809L -Wall -Wextra -pedantic -O3 -fopenmp -DHAVE_OPENMP -DHAVE_CLOCK -lm -lcivetweb -DDEFAULT_LANGUAGE="\"$(DEFAULT_LANGUAGE)\"" -ggdb -Wno-unused-parameter

tokamak.rom: tokamak.tal bytebeat.tal
	uxnasm $< $@ > $@.map

tokamak.pdf: README.md Makefile
	pandoc README.md --toc --metadata title="tokamak" --metadata date="2021-01-14" --metadata author="Claude Heiland-Allen" --metadata author="Laura Netz" --pdf-engine xelatex -V geometry:"a4paper" -V mainfont="DejaVuSans" -V monofont="DejaVuSansMono" -V fontfamilyoptions="sfdefault" -o tokamak.pdf

index.html: README.md tokamak.css Makefile
	pandoc README.md --toc --metadata title="tokamak" --metadata date="2021-01-14" --metadata author="Claude Heiland-Allen" --metadata author="Laura Netz" --standalone --css tokamak.css -o index.html
