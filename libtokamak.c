#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#ifdef HAVE_OPENMP
#include <omp.h>
#endif

#include "libtokamak.h"

#define popcount __builtin_popcountll

static tokamak_cardset _tokamak_regular_mask(const tokamak *T)
{
  return (((tokamak_cardset) 1) << tokamak_get_regular(T)) - 1;
}

static tokamak_cardset _tokamak_joker_mask(const tokamak *T)
{
  return ((((tokamak_cardset)1) << tokamak_get_jokers(T)) - 1) <<
    tokamak_get_regular(T);
}

void tokamak_options_default(tokamak_options *O)
{
  const tokamak_options def = { 13, 4, 2, 3, 5, 2, 0 };
  *O = def;
}

bool tokamak_options_valid(const tokamak_options *O)
{
  int cards = O->ranks * O->suits + O->jokers;
  return 0 < O->ranks && O->ranks <= TOKAMAK_BITS &&
         0 < O->suits && O->suits <= TOKAMAK_BITS &&
         0 <= O->jokers && O->jokers <= TOKAMAK_BITS &&
         0 < cards && cards <= TOKAMAK_BITS &&
         0 < O->pile && O->pile <= O->ranks && O->pile <= O->suits &&
         0 < O->players && O->players <= TOKAMAK_BITS &&
         0 <= O->starter && O->starter < O->players &&
         O->players * O->deal <= cards;
}

tokamak *tokamak_new(const tokamak_options *O)
{
  if (! tokamak_options_valid(O))
  {
    return 0;
  }
  tokamak *T = calloc(1, sizeof(*T));
  if (! T)
  {
    return 0;
  }
  tokamak_init(T, O);
  return T;
}

bool tokamak_init(tokamak *T, const tokamak_options *O)
{
  memset(T, 0, sizeof(*T));
  T->ranks = O->ranks;
  T->suits = O->suits;
  T->jokers = O->jokers;
  T->pile = O->pile;
  T->deal = O->deal;
  T->players = O->players;
  T->player = O->starter;
  T->ndeck = tokamak_get_cards(T);
  for (int c = 0; c < T->ndeck; ++c)
  {
    T->deck[c] = c;
  }
  return T;
}

void tokamak_delete(tokamak *T)
{
  if (T)
  {
    free(T);
  }
}

int tokamak_get_ranks(const tokamak *T)
{
  return T->ranks;
}

int tokamak_get_suits(const tokamak *T)
{
  return T->suits;
}

int tokamak_get_jokers(const tokamak *T)
{
  return T->jokers;
}

int tokamak_get_pile(const tokamak *T)
{
  return T->pile;
}
int tokamak_get_deal(const tokamak *T)
{
  return T->deal;
}

int tokamak_get_players(const tokamak *T)
{
  return T->players;
}

int tokamak_get_regular(const tokamak *T)
{
  return tokamak_get_suits(T) * tokamak_get_ranks(T);
}

int tokamak_get_cards(const tokamak *T)
{
  return tokamak_get_regular(T) + tokamak_get_jokers(T);
}

void tokamak_restore(tokamak *T, tokamak_cardset deck, tokamak_cardset rank_regular, tokamak_cardset rank_joker, tokamak_cardset suit_regular, tokamak_cardset suit_joker, int player)
{
  T->ndeck = 0;
  for (tokamak_card card = 0; card < tokamak_get_cards(T); ++card)
  {
    if (deck & tokamak_cardset_singleton(T, card))
    {
      T->deck[T->ndeck++] = card;
    }
  }
  tokamak_shuffle(T);
  T->player = player;
  T->table[0].rank_regular = rank_regular;
  T->table[0].rank_joker = rank_joker;
  T->table[0].suit_regular = suit_regular;
  T->table[0].suit_joker = suit_joker;
}

void tokamak_restore_hand(tokamak *T, int player, tokamak_cardset hand_regular, int hand_njokers)
{
  tokamak_cardset hand_joker = (((1ull << hand_njokers) - 1) << tokamak_get_regular(T));
  T->hand[player] = hand_regular | hand_joker;
  if (player == T->player)
  {
    T->table[0].hand_regular = hand_regular;
    T->table[0].hand_njokers = hand_njokers;
  }
}

void tokamak_restore_hand_size(tokamak *T, int player, int ncards) // FIXME implement
{
  (void) T;
  (void) player;
  (void) ncards;
}

typedef struct
{
  int key;
  tokamak_card value;
} kv;

static int kv_cmp(const void *a, const void *b)
{
  const kv *x = a;
  const kv *y = b;
  int p = x->key;
  int q = y->key;
  return (p > q) - (q > p);
}

void tokamak_shuffle(tokamak *T)
{
  kv tmp[TOKAMAK_BITS];
  for (tokamak_card i = 0; i < T->ndeck; ++i)
  {
    tmp[i].key = rand();
    tmp[i].value = T->deck[i];
  }
  qsort(&tmp[0], T->ndeck, sizeof(kv), kv_cmp);
  for (tokamak_card i = 0; i < T->ndeck; ++i)
    T->deck[i] = tmp[i].value;
}

tokamak_card tokamak_card_invalid(const tokamak *T)
{
  (void) T;
  return ~((tokamak_card) 0);
}

tokamak_card tokamak_card_regular(const tokamak *T, int suit, int rank)
{
  if (! (0 <= suit && suit < tokamak_get_suits(T) &&
         0 <= rank && rank < tokamak_get_ranks(T)))
  {
    return tokamak_card_invalid(T);
  }
  else
  {
    return rank + tokamak_get_ranks(T) * suit;
  }
}

tokamak_card tokamak_card_joker(const tokamak *T, int joker)
{
  if (! (0 <= joker && joker < tokamak_get_jokers(T)))
  {
    return tokamak_card_invalid(T);
  }
  else
  {
    return joker + tokamak_get_regular(T);
  }
}

bool tokamak_card_is_valid(const tokamak *T, tokamak_card card)
{
  return 0 <= card && card < tokamak_get_cards(T);
}

bool tokamak_card_is_regular(const tokamak *T, tokamak_card card)
{
  return 0 <= card && card < tokamak_get_regular(T);
}

bool tokamak_card_is_joker(const tokamak *T, tokamak_card card)
{
  return tokamak_get_regular(T) <= card && card < tokamak_get_cards(T);
}

int tokamak_card_get_rank(const tokamak *T, tokamak_card card)
{
  if (tokamak_card_is_regular(T, card))
  {
    return card % tokamak_get_ranks(T);
  }
  else
  {
    return -1;
  }
}

int tokamak_card_get_suit(const tokamak *T, tokamak_card card)
{
  if (tokamak_card_is_regular(T, card))
  {
    return card / tokamak_get_ranks(T);
  }
  else
  {
    return -1;
  }
}

tokamak_cardset tokamak_cardset_empty(const tokamak *T)
{
  (void) T;
  return 0;
}

tokamak_cardset tokamak_cardset_singleton(const tokamak *T, tokamak_card card)
{
  if (! tokamak_card_is_valid(T, card))
  {
    return tokamak_cardset_empty(T);
  }
  else
  {
    return ((tokamak_cardset) 1) << card;
  }
}

bool tokamak_cardset_contains(const tokamak *T, tokamak_cardset set, tokamak_card card)
{
  return !!(set & tokamak_cardset_singleton(T, card));
}

tokamak_cardset tokamak_cardset_regular(const tokamak *T, int suit, int rank)
{
  return tokamak_cardset_singleton(T, tokamak_card_regular(T, suit, rank));
}

int tokamak_cardset_size(const tokamak *T, tokamak_cardset cards)
{
  (void) T;
  return popcount(cards);
}

static tokamak_card _tokamak_draw(tokamak *T)
{
  if (T->ndeck)
  {
    return T->deck[--T->ndeck];
  }
  else
  {
    return tokamak_card_invalid(T);
  }
}

void tokamak_move_rollback(tokamak *T)
{
  T->table[1] = T->table[0];
  T->move_is_valid_known = true;
  T->move_is_valid = true;
  T->move_is_unchanged_known = true;
  T->move_is_unchanged = true;
}

void tokamak_deal(tokamak *T)
{
  for (tokamak_card n = 0; n < tokamak_get_deal(T); ++n)
  {
    for (tokamak_card h = 0; h < tokamak_get_players(T); ++h)
    {
      T->hand[h] |= tokamak_cardset_singleton(T, _tokamak_draw(T));
    }
  }
  T->table[0].hand_regular = T->hand[T->player] & _tokamak_regular_mask(T);
  T->table[0].hand_njokers = popcount(T->hand[T->player] & _tokamak_joker_mask(T));
  tokamak_move_rollback(T);
}

static bool _tokamak_valid_table(const tokamak *T, tokamak_card which)
{
  if (T->table[which].rank_regular & T->table[which].suit_regular)
  {
    return false;
  }
  if (T->table[which].rank_regular & T->table[which].rank_joker)
  {
    return false;
  }
  if (T->table[which].suit_regular & T->table[which].suit_joker)
  {
    return false;
  }
  if (tokamak_cardset_size(T, T->table[which].rank_joker) +
      tokamak_cardset_size(T, T->table[which].suit_joker) > tokamak_get_jokers(T))
  {
    return false;
  }

  tokamak_cardset s = T->table[which].rank_regular | T->table[which].rank_joker;
  for (tokamak_card rank = 0; rank < tokamak_get_ranks(T); ++rank)
  {
    tokamak_card after_ace_run = 1;
    if (s & tokamak_cardset_regular(T, 0, rank))
    {
      tokamak_card run = 1;
      for ( tokamak_card suit = 1
          ; (s & tokamak_cardset_regular(T, suit, rank)) &&
            suit < tokamak_get_suits(T)
          ; ++suit
          )
      {
        run++;
        after_ace_run++;
      }
      for ( tokamak_card suit = tokamak_get_suits(T)
          ; (s & tokamak_cardset_regular(T, suit - 1, rank)) &&
            suit > 0
          ; --suit
          )
      {
        run++;
      }
      if (0 < run && run < tokamak_get_pile(T))
      {
        return false;
      }
    }
    tokamak_card run = 0;
    for ( tokamak_card suit = after_ace_run
        ; suit < tokamak_get_suits(T)
        ; ++suit
        )
    {
      if (s & tokamak_cardset_regular(T, suit, rank))
      {
        run++;
      }
      else
      {
        if (0 < run && run < tokamak_get_pile(T))
        {
          return false;
        }
        run = 0;
      }
    }
    if (! (s & tokamak_cardset_regular(T, 0, rank)))
    {
      if (0 < run && run < tokamak_get_pile(T))
      {
        return false;
      }
    }
  }

  s = T->table[which].suit_regular | T->table[which].suit_joker;
  for (tokamak_card suit = 0; suit < tokamak_get_suits(T); ++suit)
  {
    tokamak_card after_ace_run = 1;
    if (s & tokamak_cardset_regular(T, suit, 0))
    {
      tokamak_card run = 1;
      for ( tokamak_card rank = 1
          ; (s & tokamak_cardset_regular(T, suit, rank)) &&
            rank < tokamak_get_ranks(T)
          ; ++rank
          )
      {
        run++;
        after_ace_run++;
      }
      for ( tokamak_card rank = tokamak_get_ranks(T)
          ; (s & tokamak_cardset_regular(T, suit, rank - 1)) &&
            rank > 0
          ; --rank
          )
      {
        run++;
      }
      if (0 < run && run < tokamak_get_pile(T))
      {
        return false;
      }
    }
    tokamak_card run = 0;
    for ( tokamak_card rank = after_ace_run
        ; rank < tokamak_get_ranks(T)
        ; ++rank
        )
    {
      if (s & tokamak_cardset_regular(T, suit, rank))
      {
        run++;
      }
      else
      {
        if (0 < run && run < tokamak_get_pile(T))
        {
          return false;
        }
        run = 0;
      }
    }
    if (! (s & tokamak_cardset_regular(T, suit, 0)))
    {
      if (0 < run && run < tokamak_get_pile(T))
      {
        return false;
      }
    }
  }
  return true;
}

bool tokamak_move_is_valid(tokamak *T)
{
  if (T->move_is_valid_known)
  {
    return T->move_is_valid;
  }
  T->move_is_valid_known = true;
  tokamak_cardset before =
    T->table[0].hand_regular | T->table[0].rank_regular | T->table[0].suit_regular;
  tokamak_cardset after =
    T->table[1].hand_regular | T->table[1].rank_regular | T->table[1].suit_regular;
  if (before != after)
  {
    return T->move_is_valid = false;
  }
  if (T->table[1].hand_regular & (T->table[1].rank_regular | T->table[1].suit_regular))
  {
    return T->move_is_valid = false;
  }
  if (T->table[1].rank_regular & T->table[1].suit_regular)
  {
    return T->move_is_valid = false;
  }
  if ((T->table[1].hand_regular & ~before))
  {
    // took non-joker
    return T->move_is_valid = false;
  }
  int jbefore =
    T->table[0].hand_njokers +
    tokamak_cardset_size(T, T->table[0].rank_joker) +
    tokamak_cardset_size(T, T->table[0].suit_joker);
  int jafter =
    T->table[1].hand_njokers +
    tokamak_cardset_size(T, T->table[1].rank_joker) +
    tokamak_cardset_size(T, T->table[1].suit_joker);
  if (jbefore != jafter)
  {
    return T->move_is_valid = false;
  }
  return T->move_is_valid = _tokamak_valid_table(T, 1);
}

bool tokamak_move_is_unchanged(tokamak *T)
{
  if (T->move_is_unchanged_known)
  {
    return T->move_is_unchanged;
  }
  T->move_is_unchanged_known = true;
  return T->move_is_unchanged =
    T->table[0].hand_regular == T->table[1].hand_regular &&
    T->table[0].hand_njokers == T->table[1].hand_njokers &&
    T->table[0].rank_regular == T->table[1].rank_regular &&
    T->table[0].rank_joker == T->table[1].rank_joker &&
    T->table[0].suit_regular == T->table[1].suit_regular &&
    T->table[0].suit_joker == T->table[1].suit_joker;
}

tokamak_commit tokamak_move_commit(tokamak *T)
{
  if (! tokamak_move_is_valid(T))
  {
    return tokamak_commit_invalid;
  }
  if (tokamak_move_is_unchanged(T))
  {
    if (T->ndeck)
    {
      T->hand[T->player] |= tokamak_cardset_singleton(T, _tokamak_draw(T));
      T->player = (T->player + 1) % T->players;
      T->table[0].hand_regular = T->hand[T->player] & _tokamak_regular_mask(T);
      T->table[0].hand_njokers = popcount(T->hand[T->player] & _tokamak_joker_mask(T));
      tokamak_move_rollback(T);
      return tokamak_commit_ok;
    }
    else
    {
      return tokamak_commit_stalemate;
    }
  }
  else
  {
    T->table[0] = T->table[1];
    T->hand[T->player] = T->table[0].hand_regular | ((((tokamak_cardset) 1 << T->table[0].hand_njokers) - 1) << tokamak_get_regular(T));
    if (T->hand[T->player] == 0)
    {
      return tokamak_commit_winner;
    }
    T->player = (T->player + 1) % T->players;
    T->table[0].hand_regular = T->hand[T->player] & _tokamak_regular_mask(T);
    T->table[0].hand_njokers = popcount(T->hand[T->player] & _tokamak_joker_mask(T));
    tokamak_move_rollback(T);
    return tokamak_commit_ok;
  }
}

bool tokamak_move_swap_suit_rank(tokamak *T, int suit, int rank)
{
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  tokamak_cardset tmp = T->table[1].rank_regular;
  T->table[1].rank_regular = (T->table[1].rank_regular & ~c) | (T->table[1].suit_regular & c);
  T->table[1].suit_regular = (T->table[1].suit_regular & ~c) | (tmp & c);
  tmp = T->table[1].rank_joker;
  T->table[1].rank_joker = (T->table[1].rank_joker & ~c) | (T->table[1].suit_joker & c);
  T->table[1].suit_joker = (T->table[1].suit_joker & ~c) | (tmp & c);
  bool changed =
    ( T->table[1].rank_joker
    | T->table[1].rank_regular
    | T->table[1].suit_joker
    | T->table[1].suit_regular
    ) & c;
  if (changed)
  {
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
  }
  return changed;
}

bool tokamak_move_play_regular_from_hand_to_rank(tokamak *T, int suit, int rank)
{
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  if ((T->table[1].hand_regular & c) &&
      (0 == (T->table[1].rank_regular & c)) &&
      (0 == (T->table[1].rank_joker & c)))
  {
    T->table[1].hand_regular &= ~c;
    T->table[1].rank_regular |= c;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  else
  {
    return false;
  }
}

bool tokamak_move_play_regular_from_hand_to_suit(tokamak *T, int suit, int rank)
{
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  if ((T->table[1].hand_regular & c) &&
      (0 == (T->table[1].suit_regular & c)) &&
      (0 == (T->table[1].suit_joker & c)))
  {
    T->table[1].hand_regular &= ~c;
    T->table[1].suit_regular |= c;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  else
  {
    return false;
  }
}

bool tokamak_move_play_joker_from_hand_to_rank(tokamak *T, int suit, int rank)
{
  int jokers = T->table[1].hand_njokers;
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  if (jokers &&
      (0 == (T->table[1].rank_regular & c)) &&
      (0 == (T->table[1].rank_joker & c)))
  {
    jokers -= 1;
    T->table[1].hand_njokers = jokers;
    T->table[1].rank_joker |= c;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  else
  {
    return false;
  }
}

bool tokamak_move_play_joker_from_hand_to_suit(tokamak *T, int suit, int rank)
{
  int jokers = T->table[1].hand_njokers;
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  if (jokers &&
      (0 == (T->table[1].suit_regular & c)) &&
      (0 == (T->table[1].suit_joker & c)))
  {
    jokers -= 1;
    T->table[1].hand_njokers = jokers;
    T->table[1].suit_joker |= c;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  else
  {
    return false;
  }
}

bool tokamak_move_take_joker_to_hand_from_rank(tokamak *T, int suit, int rank)
{
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  if (T->table[1].rank_joker & c)
  {
    T->table[1].rank_joker &= ~c;
    int jokers = T->table[1].hand_njokers + 1;
    T->table[1].hand_njokers = jokers;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  else
  {
    return false;
  }
}

bool tokamak_move_take_joker_to_hand_from_suit(tokamak *T, int suit, int rank)
{
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  if (T->table[1].suit_joker & c)
  {
    T->table[1].suit_joker &= ~c;
    int jokers = T->table[1].hand_njokers + 1;
    T->table[1].hand_njokers = jokers;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  else
  {
    return false;
  }
}

bool tokamak_move_play_from_hand(tokamak *T, int suit, int rank)
{
  // FIXME no way to prefer playing joker
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  bool moved = false;
  if (T->table[1].hand_regular & c)
  {
    if (0 == (T->table[1].rank_regular & c))
    {
      T->table[1].hand_regular &= ~c;
      T->table[1].rank_regular |= c;
      if (T->table[1].rank_joker & c)
      {
        int jokers = T->table[1].hand_njokers + 1;
        T->table[1].rank_joker &= ~c;
        T->table[1].hand_njokers = jokers;
      }
      moved = true;
    }
    else if (0 == (T->table[1].suit_regular & c))
    {
      T->table[1].hand_regular &= ~c;
      T->table[1].suit_regular |= c;
      if (T->table[1].suit_joker & c)
      {
        int jokers = T->table[1].hand_njokers + 1;
        T->table[1].suit_joker &= ~c;
        T->table[1].hand_njokers = jokers;
      }
      moved = true;
    }
  }
  else if (T->table[1].hand_njokers)
  {
    // FIXME joker identities are lost
    bool played_joker = false;
    if (0 == ((T->table[1].rank_regular | T->table[1].rank_joker) & c))
    {
      T->table[1].rank_joker |= c;
      played_joker = true;
    }
    else if (0 == ((T->table[1].suit_regular | T->table[1].suit_joker) & c))
    {
      T->table[1].suit_joker |= c;
      played_joker = true;
    }
    if (played_joker)
    {
      int jokers = T->table[1].hand_njokers - 1;
      T->table[1].hand_njokers = jokers;
      moved = true;
    }
  }
  if (moved)
  {
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
  }
  return moved;
}

bool tokamak_move_play_to_hand(tokamak *T, int suit, int rank)
{
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  bool moved_joker = false;
  bool moved_regular = false;
  if (T->table[1].rank_joker & c)
  {
    moved_joker = true;
    T->table[1].rank_joker &= ~c;
  }
  else if (T->table[1].rank_regular & c)
  {
    moved_regular = true;
    T->table[1].rank_regular &= ~c;
  }
  else if (T->table[1].suit_joker & c)
  {
    moved_joker = true;
    T->table[1].suit_joker &= ~c;
  }
  else if (T->table[1].suit_regular & c)
  {
    moved_regular = true;
    T->table[1].suit_regular &= ~c;
  }
  if (moved_regular)
  {
    T->table[1].hand_regular |= c;
  }
  else if (moved_joker)
  {
    int jokers = T->table[1].hand_njokers + 1;
    T->table[1].hand_njokers = jokers;
  }
  if (moved_regular || moved_joker)
  {
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
  }
  return moved_regular || moved_joker;
}

int tokamak_get_deck(const tokamak *T)
{
  return T->ndeck;
}

int tokamak_get_player(const tokamak *T)
{
  return T->player;
}

int tokamak_get_hand_size(const tokamak *T, int player)
{
  if (! (0 <= player && player < tokamak_get_players(T)))
  {
    return 0;
  }
  else
  {
    return tokamak_cardset_size(T, T->hand[player]);
  }
}

tokamak_cardset tokamak_get_hand(const tokamak *T, int player)
{
  if (! (0 <= player && player < tokamak_get_players(T)))
  {
    return 0;
  }
  else
  {
    return T->hand[player];
  }
}

tokamak_cardset tokamak_get_rank_regular(const tokamak *T)
{
  return T->table[0].rank_regular;
}

tokamak_cardset tokamak_get_rank_joker(const tokamak *T)
{
  return T->table[0].rank_joker;
}

tokamak_cardset tokamak_get_suit_regular(const tokamak *T)
{
  return T->table[0].suit_regular;
}

tokamak_cardset tokamak_get_suit_joker(const tokamak *T)
{
  return T->table[0].suit_joker;
}

tokamak_cardset tokamak_move_get_hand_regular(const tokamak *T)
{
  return T->table[1].hand_regular;
}

tokamak_card tokamak_move_get_hand_njokers(const tokamak *T)
{
  return T->table[1].hand_njokers;
}

tokamak_cardset tokamak_move_get_rank_regular(const tokamak *T)
{
  return T->table[1].rank_regular;
}

tokamak_cardset tokamak_move_get_rank_joker(const tokamak *T)
{
  return T->table[1].rank_joker;
}

tokamak_cardset tokamak_move_get_suit_regular(const tokamak *T)
{
  return T->table[1].suit_regular;
}

tokamak_cardset tokamak_move_get_suit_joker(const tokamak *T)
{
  return T->table[1].suit_joker;
}

void tokamak_move_set_hand_regular(tokamak *T, tokamak_cardset c)
{
  T->move_is_valid_known = false;
  T->move_is_unchanged_known = false;
  T->table[1].hand_regular = c;
}

void tokamak_move_set_hand_njokers(tokamak *T, tokamak_card c)
{
  T->move_is_valid_known = false;
  T->move_is_unchanged_known = false;
  T->table[1].hand_njokers = c;
}

void tokamak_move_set_rank_regular(tokamak *T, tokamak_cardset c)
{
  T->move_is_valid_known = false;
  T->move_is_unchanged_known = false;
  T->table[1].rank_regular = c;
}

void tokamak_move_set_rank_joker(tokamak *T, tokamak_cardset c)
{
  T->move_is_valid_known = false;
  T->move_is_unchanged_known = false;
  T->table[1].rank_joker = c;
}

void tokamak_move_set_suit_regular(tokamak *T, tokamak_cardset c)
{
  T->move_is_valid_known = false;
  T->move_is_unchanged_known = false;
  T->table[1].suit_regular = c;
}

void tokamak_move_set_suit_joker(tokamak *T, tokamak_cardset c)
{
  T->move_is_valid_known = false;
  T->move_is_unchanged_known = false;
  T->table[1].suit_joker = c;
}

bool tokamak_move_rank_regular_to_hand(tokamak *T, tokamak_card card)
{
  tokamak_cardset s = tokamak_cardset_singleton(T, card);
  if (T->table[1].rank_regular & s)
  {
    T->table[1].rank_regular &= ~s;
    T->table[1].hand_regular |= s;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  return false;
}

bool tokamak_move_rank_joker_to_hand(tokamak *T, tokamak_card card)
{
  tokamak_cardset s = tokamak_cardset_singleton(T, card);
  if (T->table[1].rank_joker & s)
  {
    T->table[1].rank_joker &= ~s;
    T->table[1].hand_njokers += 1;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  return false;
}

bool tokamak_move_suit_regular_to_hand(tokamak *T, tokamak_card card)
{
  tokamak_cardset s = tokamak_cardset_singleton(T, card);
  if (T->table[1].suit_regular & s)
  {
    T->table[1].suit_regular &= ~s;
    T->table[1].hand_regular |= s;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  return false;
}

bool tokamak_move_suit_joker_to_hand(tokamak *T, tokamak_card card)
{
  tokamak_cardset s = tokamak_cardset_singleton(T, card);
  if (T->table[1].suit_joker & s)
  {
    T->table[1].suit_joker &= ~s;
    T->table[1].hand_njokers += 1;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  return false;
}

bool tokamak_move_hand_to_rank_regular(tokamak *T, tokamak_card card)
{
  tokamak_cardset s = tokamak_cardset_singleton(T, card);
  if (T->table[1].hand_regular & s)
  {
    T->table[1].hand_regular &= ~s;
    T->table[1].rank_regular |= s;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  return false;
}

bool tokamak_move_hand_to_rank_joker(tokamak *T, tokamak_card card)
{
  tokamak_cardset s = tokamak_cardset_singleton(T, card);
  if (T->table[1].hand_njokers > 0 && ! (T->table[1].rank_joker & s))
  {
    T->table[1].hand_njokers -= 1;
    T->table[1].rank_joker |= s;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  return false;
}

bool tokamak_move_hand_to_suit_regular(tokamak *T, tokamak_card card)
{
  tokamak_cardset s = tokamak_cardset_singleton(T, card);
  if (T->table[1].hand_regular & s)
  {
    T->table[1].hand_regular &= ~s;
    T->table[1].suit_regular |= s;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  return false;
}

bool tokamak_move_hand_to_suit_joker(tokamak *T, tokamak_card card)
{
  tokamak_cardset s = tokamak_cardset_singleton(T, card);
  if (T->table[1].hand_njokers > 0 && ! (T->table[1].suit_joker & s))
  {
    T->table[1].hand_njokers -= 1;
    T->table[1].suit_joker |= s;
    T->move_is_valid_known = false;
    T->move_is_unchanged_known = false;
    return true;
  }
  return false;
}

#if defined(TOKAMAK_AI_RANDOM) || defined(TOKAMAK_AI_MCTS)
// [[[ pseudo random number generator implementation from Pd's [noise~]
static int global_coin_seed = 307;
static _Thread_local int local_coin_seed = 0;
static bool coin(double P)
{
  if (local_coin_seed == 0)
  {
    local_coin_seed = (global_coin_seed *= 1319);
  }
  local_coin_seed = local_coin_seed * 435898247 + 382842987;
  return (local_coin_seed & 0x7fffFFFF) <= P * 0x7fffFFFF;
}
// ]]]
#endif

#ifdef HAVE_CLOCK
static bool _deadline_elapsed(deadline_t deadline)
{
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  return (now.tv_sec - deadline->tv_sec >=
    deadline->tv_nsec / 1000000000.0 - now.tv_nsec / 1000000000.0);
}
#else
static bool _deadline_elapsed(deadline_t deadline)
{
  return *deadline;
}
#endif

#ifdef TOKAMAK_AI_RANDOM

int tokamak_ai_random_search(tokamak *T, deadline_t deadline)
{
  tokamak_cardset hand_regular = T->table[0].hand_regular;
  tokamak_card hand_njokers = T->table[0].hand_njokers;
  tokamak_cardset rank_regular = T->table[0].rank_regular;
  tokamak_cardset rank_joker = T->table[0].rank_joker;
  tokamak_cardset suit_regular = T->table[0].suit_regular;
  tokamak_cardset suit_joker = T->table[0].suit_joker;
  int best =
    tokamak_cardset_size(T, T->table[0].hand_regular) +
    T->table[0].hand_njokers;
  while (true)
  {
    if (_deadline_elapsed(deadline))
    {
      break;
    }
    T->table = T->table[0];
    int jokers = T->table[1].hand_njokers;
    for (int suit = 0; suit < tokamak_get_suits(T); ++suit)
    {
      for (int rank = 0; rank < tokamak_get_ranks(T); ++rank)
      {
        tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
        // play card, taking jokers
        if (T->table[1].hand_regular & c)
        {
          if (T->table[1].rank_joker & c)
          {
            if (coin(0.5))
            {
              jokers += 1;
              T->table[1].rank_joker &= ~c;
              T->table[1].hand_regular &= ~c;
              T->table[1].rank_regular |= c;
            }
          }
          else if (T->table[1].suit_joker & c)
          {
            if (coin(0.5))
            {
              jokers += 1;
              T->table[1].suit_joker &= ~c;
              T->table[1].hand_regular &= ~c;
              T->table[1].suit_regular |= c;
            }
          }
          else
          {
            if (coin(0.5))
            {
              T->table[1].hand_regular &= ~c;
              if (coin(0.5))
              {
                T->table[1].rank_regular |= c;
              }
              else
              {
                T->table[1].suit_regular |= c;
              }
            }
          }
        }
        // take jokers
        else if (T->table[1].rank_joker & c)
        {
          if (coin(0.5))
          {
            jokers += 1;
            T->table[1].rank_joker &= ~c;
          }
        }
        else if (T->suit_joker & c)
        {
          if (coin(0.5))
          {
            jokers += 1;
            T->table[1].suit_joker &= ~c;
          }
        }
        // play jokers
        else if (jokers &&
          (T->table[1].rank_joker & c) == 0 && (T->table[1].rank_regular & c) == 0)
        {
          if (coin(0.1)) // FIXME optimize probability
          {
            jokers -= 1;
            T->table[1].rank_joker |= c;
          }
        }
        else if (jokers &&
          (T->table[1].suit_joker & c) == 0 && (T->table[1].suit_regular & c) == 0)
        {
          if (coin(0.1)) // FIXME optimize probability
          {
            jokers -= 1;
            T->table[1].suit_joker |= c;
          }
        }
        // swap rank/suit
        else if (coin(0.5))
        {
          tokamak_move_swap_suit_rank(T, suit, rank);
        }
      }
    }
    T->table[1].hand_njokers = jokers;
    if (tokamak_move_is_valid(T))
    {
      int metric =
        tokamak_cardset_size(T, T->table[1].hand_regular) +
        T->table[1].hand_njokers;
      if (metric < best)
      {
        best = metric;
        hand_regular = T->table[1].hand_regular;
        hand_njokers = T->table[1].hand_njokers;
        rank_regular = T->table[1].rank_regular;
        rank_joker = T->table[1].rank_joker;
        suit_regular = T->table[1].suit_regular;
        suit_joker = T->table[1].suit_joker;
        if (best <= 0)
        {
          break;
        }
      }
    }
  }
  T->table[1].hand_regular = hand_regular;
  T->table[1].hand_njokers = hand_njokers;
  T->table[1].rank_regular = rank_regular;
  T->table[1].rank_joker = rank_joker;
  T->table[1].suit_regular = suit_regular;
  T->table[1].suit_joker = suit_joker;
  return best;
}

int tokamak_ai_random_search_parallel
  (tokamak *T, deadline_t deadline)
{
  int best =
    tokamak_cardset_size(T, T->table[1].hand_regular) +
    T->table[1].hand_njokers;
  int nthreads = 1;
#ifdef HAVE_OMP
  nthreads = omp_get_max_threads();
#endif
  tokamak T_local[nthreads];
  for (int thread = 0; thread < nthreads; ++thread)
  {
    T_local[thread] = *T;
  }
  #pragma omp parallel for schedule(static, 1) num_threads(nthreads)
  for (int thread = 0; thread < nthreads; ++thread)
  {
    int best_local = tokamak_ai_random_search(&T_local[thread], deadline);
    #pragma omp critical
    {
      if (best_local <= best)
      {
        best = best_local;
        *T = T_local[thread];
      }
    }
  }
  return best;
}

#endif

#ifdef TOKAMAK_AI_BRUTE

static void _tokamak_ai_brute_search
  ( tokamak *T, deadline_t deadline
  , tokamak *best_T, int *best, int nchanges
  , tokamak_card card, bool toplevel
  )
{
  if (nchanges == 0)
  {
    int metric =
      tokamak_cardset_size(T, T->table[1].hand_regular) +
      T->table[1].hand_njokers;
    if (metric < *best && tokamak_move_is_valid(T))
    {
      *best = metric;
      *best_T = *T;
    }
    return;
  }
  if (! tokamak_card_is_valid(T, card) || *best == 0)
  {
    return;
  }
  //for (; card < tokamak_get_cards(T); ++card)
  {
    if (_deadline_elapsed(deadline))
    {
      return;
    }
    int suit = tokamak_card_get_suit(T, card);
    int rank = tokamak_card_get_rank(T, card);
    tokamak orig = *T;

#define RECURSE \
    { \
      _tokamak_ai_brute_search \
        (T, deadline, best_T, best, nchanges - 1, card + 1, false); \
      *T = orig; \
      if (*best == 0) return; \
    }
    if (tokamak_move_play_regular_from_hand_to_suit(T, suit, rank)) RECURSE
    if (tokamak_move_play_regular_from_hand_to_rank(T, suit, rank)) RECURSE
    if (tokamak_move_play_joker_from_hand_to_suit(T, suit, rank)) RECURSE
    if (tokamak_move_play_joker_from_hand_to_rank(T, suit, rank)) RECURSE
    if (tokamak_move_take_joker_to_hand_from_suit(T, suit, rank)) RECURSE
    if (tokamak_move_take_joker_to_hand_from_rank(T, suit, rank)) RECURSE
    if (tokamak_move_swap_suit_rank(T, suit, rank)) RECURSE
    nchanges += 1; RECURSE
#undef RECURSE
#if 0
    if (toplevel)
    {
      break;
    }
#endif
  }
}

int tokamak_ai_brute_search_serial
  (tokamak *T, deadline_t deadline)
{
  tokamak best_T = *T;
  int best =
    tokamak_cardset_size(T, T->table[1].hand_regular) +
    T->table[1].hand_njokers;
  int table_size = best +
    tokamak_cardset_size(T, T->table[1].rank_regular) +
    tokamak_cardset_size(T, T->table[1].suit_regular) +
    tokamak_cardset_size(T, T->table[1].rank_joker) +
    tokamak_cardset_size(T, T->table[1].suit_joker);
  for (int nchanges = 0; nchanges <= table_size; ++nchanges)
  {
    if (_deadline_elapsed(deadline))
    {
      break;
    }
    _tokamak_ai_brute_search(T, deadline, &best_T, &best, nchanges, 0, true);
  }
  *T = best_T;
  return best;
}

int tokamak_ai_brute_search_parallel
  (tokamak *T, deadline_t deadline)
{
  int cards = tokamak_get_cards(T);
  int best =
    tokamak_cardset_size(T, T->table[1].hand_regular) +
    T->table[1].hand_njokers;
  tokamak best_T = *T;
  for (int nchanges = 0; nchanges < tokamak_get_cards(T); ++nchanges)
  {
    if (_deadline_elapsed(deadline))
    {
      break;
    }
    #pragma omp parallel for schedule(dynamic, 1)
    for (tokamak_card card = 0; card < cards; ++card)
    {
      int best_local = tokamak_get_cards(T);
      tokamak T_local = *T;
      tokamak best_T_local = *T;
      _tokamak_ai_brute_search
        ( &T_local, deadline
        , &best_T_local, &best_local
        , nchanges, card, false
        );
      #pragma omp critical
      {
        if (best_local <= best)
        {
          best = best_local;
          best_T = best_T_local;
        }
      }
    }
    *T = best_T;
  }
  return best;
}

#endif

#ifdef TOKAMAK_AI_ANDREI

bool tokamak_move_get_suit_pile_ends(const tokamak *T, int suit, int rank, int *rank1, int *rank2)
{
  int ranks = tokamak_get_ranks(T);
  tokamak_cardset s =
    tokamak_move_get_suit_regular(T) | tokamak_move_get_suit_joker(T);
  if (! (s & tokamak_cardset_regular(T, suit, rank)))
  {
    return false;
  }
  for (*rank2 = rank; *rank2 < rank + ranks && (s & tokamak_cardset_regular(T, suit, *rank2 % ranks)); ++*rank2)
  {
    // nop
  }
  if (*rank2 == rank + ranks)
  {
    // full
    *rank1 = -1;
    *rank2 = -1;
    return true;
  }
  else
  {
    --*rank2;
    *rank2 %= ranks;
  }
  for (*rank1 = rank + ranks; *rank1 > rank && (s & tokamak_cardset_regular(T, suit, *rank1 % ranks)); --*rank1)
  {
    // nop
  }
  // full already excluded
  ++*rank1;
  *rank1 %= ranks;
  return true;
}

bool tokamak_move_get_rank_pile_ends(const tokamak *T, int suit, int rank, int *suit1, int *suit2)
{
  int suits = tokamak_get_suits(T);
  tokamak_cardset r =
    tokamak_move_get_rank_regular(T) | tokamak_move_get_rank_joker(T);
  if (! (r & tokamak_cardset_regular(T, suit, rank)))
  {
    return false;
  }
  for (*suit2 = suit; *suit2 < suit + suits && (r & tokamak_cardset_regular(T, *suit2 % suits, rank)); ++*suit2)
  {
    // nop
  }
  if (*suit2 == suit + suits)
  {
    // full
    *suit1 = -1;
    *suit2 = -1;
    return true;
  }
  else
  {
    --*suit2;
    *suit2 %= suits;
  }
  for (*suit1 = suit + suits; *suit1 > suit && (r & tokamak_cardset_regular(T, *suit1 % suits, rank)); --*suit1)
  {
    // nop
  }
  // full already excluded
  ++*suit1;
  *suit1 %= suits;
  return true;
}

static int _tokamak_ai_andrei(tokamak *T, deadline_t deadline, bool toplevel, void *arg, visitor visit);

static void _tokamak_ai_andrei_search
  ( tokamak *T, deadline_t deadline
  , tokamak *best_T, int *best
  , int suit, int rank, int axis, int jokers
  , tokamak_cardset visited
  , void *arg, visitor visit
  )
{
  if (_deadline_elapsed(deadline))
  {
    return;
  }
  if (*best == 0)
  {
    return;
  }
  tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
  if (visited & c)
  {
    return;
  }
  visited |= c;
  int suits = tokamak_get_suits(T);
  int ranks = tokamak_get_ranks(T);
  int pile = tokamak_get_pile(T);
  if (axis == 0)
  {
    bool played_joker = false;
    bool swapped = false;
    if (tokamak_move_play_regular_from_hand_to_suit(T, suit, rank) ||
        (played_joker = tokamak_move_play_joker_from_hand_to_suit(T, suit, rank)) ||
        (((tokamak_move_get_rank_regular(T) | tokamak_move_get_rank_joker(T)) & tokamak_cardset_regular(T, suit, rank)) && (swapped = tokamak_move_swap_suit_rank(T, suit, rank))))
    {
      // [rank] is suit
      int rank1 = -1, rank2 = -1;
      if (tokamak_move_get_suit_pile_ends(T, suit, rank, &rank1, &rank2))
      {
        // [rank1 .. rank2] are suit
        int p = (rank2 - rank1 + ranks) % ranks;
        if ((rank1 < 0 && rank2 < 0) || p + 1 >= pile)
        {
          // ok
          if (tokamak_move_is_valid(T))
          {
            if (visit)
            {
              visit(arg, T);
            }
            int metric = tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 + jokers;
            while (metric < *best)
            {
              *best = metric;
              *best_T = *T;
              if (metric == 0)
              {
                return;
              }
              _tokamak_ai_andrei(T, deadline, false, arg, visit);
              metric = tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 + jokers;
            }
          }
        }
        else
        {
          // need to extend
          tokamak orig = *T;
          _tokamak_ai_andrei_search(T, deadline, best_T, best, suit, (rank1 + ranks - 1) % ranks, 0, jokers - (played_joker ? 1 : 0), visited, arg, visit);
          *T = orig;
          _tokamak_ai_andrei_search(T, deadline, best_T, best, suit, (rank2 + 1) % ranks, 0, jokers - (played_joker ? 1 : 0), visited, arg, visit);
          *T = orig;
        }
      }
    }
    if (swapped)
    {
      // may need to fixup what we broke
      int suitn[2] = { (suit + suits - 1) % suits, (suit + 1) % suits };
      for (int n = 0; n < 2; ++n)
      {
        int suit1 = -1, suit2 = -1;
        if (tokamak_move_get_rank_pile_ends(T, suitn[n], rank, &suit1, &suit2))
        {
          // [suit1 .. suit2] are rank
          int p = (suit2 - suit1 + suits) % suits;
          if ((suit1 < 0 && suit2 < 0) || p + 1 >= pile)
          {
            // ok
            if (tokamak_move_is_valid(T))
            {
              if (visit)
              {
                visit(arg, T);
              }
              int metric = tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 + jokers;
              while (metric < *best)
              {
                *best = metric;
                *best_T = *T;
                if (metric == 0)
                {
                  return;
                }
                _tokamak_ai_andrei(T, deadline, false, arg, visit);
                metric = tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 + jokers;
              }
            }
          }
          else
          {
            // need to extend
            tokamak orig = *T;
            _tokamak_ai_andrei_search(T, deadline, best_T, best, (suit1 + suits - 1) % suits, rank, 1, jokers, visited, arg, visit);
            *T = orig;
            _tokamak_ai_andrei_search(T, deadline, best_T, best, (suit2 + 1) % suits, rank, 1, jokers, visited, arg, visit);
            *T = orig;
          }
        }
      }
    }
  }
  else // axis == 1
  {
    bool played_joker = false;
    bool swapped = false;
    if (tokamak_move_play_regular_from_hand_to_rank(T, suit, rank) ||
        (played_joker = tokamak_move_play_joker_from_hand_to_rank(T, suit, rank)) ||
        (((tokamak_move_get_suit_regular(T) | tokamak_move_get_suit_joker(T)) & tokamak_cardset_regular(T, suit, rank)) && (swapped = tokamak_move_swap_suit_rank(T, suit, rank))))
    {
      int suit1 = -1, suit2 = -1;
      if (tokamak_move_get_rank_pile_ends(T, suit, rank, &suit1, &suit2))
      {
        int p = (suit2 - suit1 + suits) % suits;
        if ((suit1 < 0 && suit2 < 0) || p + 1 >= pile)
        {
          // ok
          if (tokamak_move_is_valid(T))
          {
            if (visit)
            {
              visit(arg, T);
            }
            int metric = tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 + jokers;
            while (metric < *best)
            {
              *best = metric;
              *best_T = *T;
              if (metric == 0)
              {
                return;
              }
              _tokamak_ai_andrei(T, deadline, false, arg, visit);
              metric = tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 + jokers;
            }
          }
        }
        else
        {
          // need to extend
          tokamak orig = *T;
          _tokamak_ai_andrei_search(T, deadline, best_T, best, (suit1 + suits - 1) % suits, rank, 1, jokers - (played_joker ? 1 : 0), visited, arg, visit);
          *T = orig;
          _tokamak_ai_andrei_search(T, deadline, best_T, best, (suit2 + 1) % suits, rank, 1, jokers - (played_joker ? 1 : 0), visited, arg, visit);
          *T = orig;
        }
      }
    }
    if (swapped)
    {
      // may need to fixup what we broke
      int rankn[2] = { (rank + ranks - 1) % ranks, (rank + 1) % ranks };
      for (int n = 0; n < 2; ++n)
      {
        int rank1 = -1, rank2 = -1;
        if (tokamak_move_get_suit_pile_ends(T, suit, rankn[n], &rank1, &rank2))
        {
          // [rank1 ... rank2] are suit
          int p = (rank2 - rank1 + ranks) % ranks;
          if ((rank1 < 0 && rank2 < 0) || p + 1 >= pile)
          {
            // ok
            if (tokamak_move_is_valid(T))
            {
              int metric = tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 + jokers;
              while (metric < *best)
              {
                *best = metric;
                *best_T = *T;
                if (metric == 0)
                {
                  return;
                }
                _tokamak_ai_andrei(T, deadline, false, arg, visit);
                metric = tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 + jokers;
              }
            }
          }
          else
          {
            // need to extend
            tokamak orig = *T;
            _tokamak_ai_andrei_search(T, deadline, best_T, best, suit, (rank1 + ranks - 1) % ranks, 0, jokers, visited, arg, visit);
            *T = orig;
            _tokamak_ai_andrei_search(T, deadline, best_T, best, suit, (rank2 + 1) % ranks, 0, jokers, visited, arg, visit);
            *T = orig;
          }
        }
      }
    }
  }
}

static int _tokamak_ai_andrei(tokamak *T, deadline_t deadline, bool toplevel, void *arg, visitor visit)
{
  if (visit)
  {
    visit(arg, T);
  }
  int metric =
    tokamak_cardset_size(T, tokamak_move_get_hand_regular(T)) * 2 +
    tokamak_move_get_hand_njokers(T) * 1;
  if (metric == 0)
  {
    return metric;
  }
  int best = metric;
  tokamak best_T = *T;
  int moved = false;
  if (toplevel)
  {
    // take jokers to hand
    tokamak_cardset rj = tokamak_move_get_rank_joker(T);
    tokamak_cardset sj = tokamak_move_get_suit_joker(T);
    for (int suit = 0; suit < tokamak_get_suits(T); ++suit)
    {
      for (int rank = 0; rank < tokamak_get_ranks(T); ++rank)
      {
        tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
        if (rj & c)
        {
          tokamak orig = *T;
          tokamak_move_take_joker_to_hand_from_rank(T, suit, rank);
          if (! tokamak_move_is_valid(T) && ! tokamak_move_play_regular_from_hand_to_rank(T, suit, rank))
          {
            *T = orig;
          }
          else
          {
            moved = true;
          }
        }
        if (sj & c)
        {
          tokamak orig = *T;
          tokamak_move_take_joker_to_hand_from_suit(T, suit, rank);
          if (! tokamak_move_is_valid(T) && ! tokamak_move_play_regular_from_hand_to_suit(T, suit, rank))
          {
            *T = orig;
          }
          else
          {
            moved = true;
          }
        }
      }
    }
  }
  if (moved)
  {
    if (visit)
    {
      visit(arg, T);
    }
  }
  // for each card in hand
  tokamak_cardset hr = tokamak_move_get_hand_regular(T);
  int jokers = tokamak_move_get_hand_njokers(T);
  int suits = tokamak_get_suits(T);
  int ranks = tokamak_get_ranks(T);
#if HAVE_OPENMP
  #pragma omp parallel for collapse(3) schedule(dynamic, 1)
  for (int suit = 0; suit < suits; ++suit)
  {
    for (int rank = 0; rank < ranks; ++rank)
    {
      for (int axis = 0; axis < 2; ++axis)
      {
        tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
        if ((hr & c) || jokers)
        {
          tokamak T_local = *T;
          tokamak best_T_local = *T;
          int best_local = metric;
          _tokamak_ai_andrei_search
            ( &T_local, deadline, &best_T_local, &best_local
            , suit, rank, axis, jokers, (tokamak_cardset) 0, arg, visit
            );
          #pragma omp critical
          {
            if (best_local < best)
            {
              best = best_local;
              best_T = best_T_local;
            }
          }
        }
      }
    }
  }
#else
  tokamak orig = *T;
  for (int suit = 0; suit < suits; ++suit)
  {
    for (int rank = 0; rank < ranks; ++rank)
    {
      for (int axis = 0; axis < 2; ++axis)
      {
        tokamak_cardset c = tokamak_cardset_regular(T, suit, rank);
        if ((hr & c) || jokers)
        {
          *T = orig;
          tokamak best_T_local = *T;
          int best_local = metric;
          _tokamak_ai_andrei_search
            ( T, deadline, &best_T_local, &best_local
            , suit, rank, axis, jokers, (tokamak_cardset) 0, arg, visit
            );
          if (best_local < best)
          {
            best = best_local;
            best_T = best_T_local;
          }
          if (best == 0)
          {
            *T = best_T;
            return best;
          }
        }
      }
    }
  }
#endif
  *T = best_T;
  return best;
}

int tokamak_ai_andrei(tokamak *T, deadline_t deadline)
{
  return _tokamak_ai_andrei(T, deadline, true, 0, 0);
}

int tokamak_ai_andrei_visit(tokamak *T, deadline_t deadline, void *arg, visitor visit)
{
  return _tokamak_ai_andrei(T, deadline, true, arg, visit);
}

#endif

#ifdef TOKAMAK_AI_MCTS

static void _reservoir(tokamak *R, const tokamak *S, int i)
{
  if (coin(1.0 / (1.0 + i)))
  {
    *R = *S;
  }
}

struct reservoir
{
  tokamak T;
  int i;
};

void _reservoir_visitor(void *arg, const tokamak *T)
{
  struct reservoir *R = arg;
  #pragma omp critical
  _reservoir(&R->T, T, R->i++);
}

static void _tokamak_shuffle_unknowns(tokamak *T)
{
  int hands[TOKAMAK_BITS];
  for (int player = 0; player < tokamak_get_players(T); ++player)
  {
    if (player != tokamak_get_player(T))
    {
      hands[player] = tokamak_get_hand_size(T, player);
      for (tokamak_card card = 0; card < tokamak_get_cards(T); ++card)
      {
        if (T->hand[player] & tokamak_cardset_singleton(T, card))
        {
          T->deck[T->ndeck++] = card;
        }
      }
    }
  }
  tokamak_shuffle(T);
  for (int player = 0; player < tokamak_get_players(T); ++player)
  {
    if (player != tokamak_get_player(T))
    {
      T->hand[player] = 0;
      for (int i = 0; i < hands[player]; ++i)
      {
        T->hand[player] |= tokamak_cardset_singleton(T, T->deck[--T->ndeck]);
      }
    }
  }
}

static double _rollout(const tokamak *T, deadline_t deadline)
{
  tokamak S = *T;
  double win = 1.0;
  while (true)
  {
    if (_deadline_elapsed(deadline))
    {
      return 0.5;
    }
    _tokamak_shuffle_unknowns(&S);
    struct reservoir R = {0};
    _tokamak_ai_andrei(&S, deadline, true, &R, _reservoir_visitor);
    switch (tokamak_move_commit(&R.T))
    {
      case tokamak_commit_winner:
        return win;
        break;
      case tokamak_commit_stalemate:
        return 1 - win;
        break;
      case tokamak_commit_ok:
        continue;
        break;
      default:
        abort();
        break;
    }
    S = R.T;
    win = 1.0 - win;
  }
}

struct node
{
  struct node *parent;
  struct node *next_child;
  struct node *children;
  tokamak S;
  tokamak T;
  double t;
  int n;
};

static struct node *node_new(struct node *parent, const tokamak *T)
{
  struct node *n = calloc(1, sizeof(*n));
  n->parent = parent;
  n->S = *T;
  n->T = *T;
  return n;
}

static void node_delete_tree(struct node *root)
{
  struct node *n, *next;
  for (n = root->children; n; n = next)
  {
    next = n->next_child;
    node_delete_tree(n);
  }
  free(root);
}

struct collector
{
  struct node *parent;
};

static void _collect_visitor(void *arg, const tokamak *T)
{
  struct collector *C = arg;
  #pragma omp critical
  {
    struct node *child = node_new(C->parent, T);
    tokamak_move_commit(&child->T);
    child->next_child = C->parent->children;
    C->parent->children = child;
  }
}

double tokamak_ai_mcts(tokamak *T, deadline_t deadline)
{
  const double C = sqrt(2);
  struct node *root = node_new(0, T);
  while (true)
  {
    if (_deadline_elapsed(deadline))
    {
      break;
    }
    struct node *current = root;
    while (current->children)
    {
      struct node *max_node = 0;
      double max_ucb1 = -1.0/0.0;
      for (struct node *child = current->children; child; child = child->next_child)
      {
        if (child->n == 0)
        {
          max_node = child;
          break;
        }
        else
        {
          double ucb1 = child->t / child->n + C * sqrt(log(root->n) / child->n);
          if (max_ucb1 < ucb1)
          {
            max_ucb1 = ucb1;
            max_node = child;
          }
        }
      }
      current = max_node;
    }
    // leaf
    if (current == root || current->n != 0)
    {
      struct collector K = {0};
      K.parent = current;
      _tokamak_ai_andrei(&current->T, deadline, true, &K, _collect_visitor);
      current = current->children;
    }
    // rollout
    double score = _rollout(&current->T, deadline);
    for (struct node *parent = current; parent; parent = parent->parent)
    {
      parent->n += 1;
      parent->t += score;
      score = 1 - score; // FIXME 2 players
    }
    if (! root->children->next_child)
    {
      break;
    }
  }
  // choose
  double best = -1.0 / 0.0;
  struct node *best_node = 0;
  for (struct node *child = root->children; child; child = child->next_child)
  {
    if (! child->n)
    {
      best_node = child;
      break;
    }
    else if (best < child->t / child->n)
    {
      best = child->t / child->n;
      best_node = child;
    }
  }
  *T = best_node->S;
  node_delete_tree(root);
  return best;
}

#endif

#ifdef TOKAMAK_AI_QUANTUM

struct q_slot_s
{
  uint16_t map;
  uint8_t center;
  uint8_t rank;
  uint8_t suit;
  // could be packed in <= 24 bits for pile <= 4
  // 12 bits: map
  // 4 bits: center
  // pile bits: rank starts
  // pile bits: suit starts
};
typedef struct q_slot_s q_slot;

struct q_board_s
{
  q_slot slots[64]; // maximum number of slots
  int jokers; // number of unfixed jokers available
};
typedef struct q_board_s q_board;

#define X 14
#define UNFIXED 15

// hand
// rank regular
// suit regular
// rank joker
// suit joker
uint8_t q_cell[2][2][2][2][2] =
  { {{{{0, 1}, {2, 3}},  {{4, X}, {5, X}}}
  , {{{6, 7}, {X, X}},  {{X, X}, {X, X}}}}
  , {{{{8, 9}, {10, 11}},  {{X, X}, {X, X}}}
  , {{{X, X}, {X, X}},  {{X, X}, {X, X}}}}
  };
bool q_hand_regular[12] = {0,0,0,0,0,0,0,0,1,1,1,1};
bool q_rank_regular[12] = {0,0,0,0,0,0,1,1,0,0,0,0};
bool q_suit_regular[12] = {0,0,0,0,1,1,0,0,0,0,0,0};
bool q_rank_joker[12]   = {0,0,1,1,0,1,0,0,0,0,1,1};
bool q_suit_joker[12]   = {0,1,0,1,0,0,0,1,0,1,0,1};

bool q_rank[12]         = {0,0,1,1,0,1,1,1,0,0,1,1}; // rank_regular | rank_joker
#define q_rank_mask 0xCEC // above as little-endian bit string
bool q_suit[12]         = {0,1,0,1,1,1,0,1,0,1,0,1}; // suit_regular | suit_joker
#define q_suit_mask 0xABA // above as little-endian bit string
int q_joker[12]         = {0,1,1,2,0,1,0,1,0,1,1,2}; // rank_joker + suit_joker

bool q_empty[12]        = {1,1,1,1,0,0,0,0,0,0,0,0};
#define E 0x00F
bool q_hand[12]         = {0,0,0,0,1,1,1,1,1,1,1,1};
#define H 0xFF0
bool q_table[12]        = {0,0,0,0,1,1,1,1,0,0,0,0};
#define T 0x0F0
int q_map_initial[12]   = {E,E,E,E,T,T,T,T,H,H,H,H};
#undef E
#undef H
#undef T

int q_order[12] = { 0, 4, 6, 8, 5, 7, 1, 2, 9, 10, 3, 11 };

void q_init(tokamak *T, deadline_t deadline, q_board *q, tokamak_table *t)
{
  memset(q, 0, sizeof(*q));
  int suits = tokamak_get_suits(T);
  int ranks = tokamak_get_ranks(T);
  int pile = tokamak_get_pile(T);
  int pilemask = (1 << pile) - 1;
  q->jokers
    = t->hand_njokers
    + tokamak_cardset_size(T, t->suit_joker)
    + tokamak_cardset_size(T, t->rank_joker);
  for (int suit = 0; suit < suits; ++suit)
  {
    for (int rank = 0; rank < ranks; ++rank)
    {
      tokamak_card c = tokamak_card_regular(T, suit, rank);
      tokamak_cardset cs = tokamak_cardset_singleton(T, c);
      bool chand  = !!(cs & t->hand_regular);
      bool crankr = !!(cs & t->rank_regular);
      bool csuitr = !!(cs & t->suit_regular);
      bool crankj = !!(cs & t->rank_joker);
      bool csuitj = !!(cs & t->suit_joker);
      int cell = q_cell[chand][crankr][csuitr][crankj][csuitj];
      assert(cell != X);
      q->slots[c].map = q_map_initial[cell];
      q->slots[c].center = UNFIXED;
      q->slots[c].rank = pilemask;
      q->slots[c].rank = pilemask;
    }
  }
}

void q_finish(tokamak *T, deadline_t deadline, q_board *q, tokamak_table *t)
{
  int suits = tokamak_get_suits(T);
  int ranks = tokamak_get_ranks(T);
  int pile = tokamak_get_pile(T);
  t->hand_njokers = q->jokers;
  for (int suit = 0; suit < suits; ++suit)
  {
    for (int rank = 0; rank < ranks; ++rank)
    {
      tokamak_card c = tokamak_card_regular(T, suit, rank);
      tokamak_cardset set = tokamak_cardset_singleton(T, c);
      int cell = q->slots[c].center;
      assert(0 <= cell && cell < 12);
      if (q_hand_regular[cell])
      {
        t->hand_regular |= set;
      }
      else
      {
        t->hand_regular &= ~set;
      }
      if (q_rank_regular[cell])
      {
        t->rank_regular |= set;
      }
      else
      {
        t->rank_regular &= ~set;
      }
      if (q_suit_regular[cell])
      {
        t->suit_regular |= set;
      }
      else
      {
        t->suit_regular &= ~set;
      }
      if (q_rank_joker[cell])
      {
        t->rank_joker |= set;
      }
      else
      {
        t->rank_joker &= ~set;
      }
      if (q_suit_joker[cell])
      {
        t->suit_joker |= set;
      }
      else
      {
        t->suit_joker &= ~set;
      }
    }
  }
}

int q_ranks(tokamak *T, deadline_t deadline, q_board *q)
{
  int suits = tokamak_get_suits(T);
  int ranks = tokamak_get_suits(T);
  int pile = tokamak_get_pile(T);
  int count = 0;
  for (int suit = 0; suit < suits; ++suit)
  {
    for (int rank = 0; rank < ranks; ++rank)
    {
      tokamak_card c = tokamak_card_regular(T, suit, rank);
      if (q->slots[c].map & q_rank_mask)
      {
        for (int offset = 0; offset < pile; ++offset)
        {
          if (q->slots[c].rank & (1 << offset))
          {
            for (int rank2 = rank - offset; rank2 < rank - offset + pile; ++rank2)
            {
              tokamak_card c2 = tokamak_card_regular(T, suit, (rank2 + ranks) % ranks);
              if (! (q->slots[c2].map & q_rank_mask))
              {
                // not possible at this offset
                q->slots[c].rank &= ~(1 << offset);
                ++count;
                break;
              }
            }
          }
        }
        if (! q->slots[c].rank)
        {
          q->slots[c].map &= ~q_rank_mask;
          if (! q->slots[c].map)
          {
            // contradiction, no possibilities
            return -1;
          }
        }
      }
    }
  }
  return count;
}

int q_suits(tokamak *T, deadline_t deadline, q_board *q)
{
  int suits = tokamak_get_suits(T);
  int ranks = tokamak_get_suits(T);
  int pile = tokamak_get_pile(T);
  int count = 0;
  for (int suit = 0; suit < suits; ++suit)
  {
    for (int rank = 0; rank < ranks; ++rank)
    {
      tokamak_card c = tokamak_card_regular(T, suit, rank);
      if (q->slots[c].map & q_suit_mask)
      {
        for (int offset = 0; offset < pile; ++offset)
        {
          if (q->slots[c].suit & (1 << offset))
          {
            for (int suit2 = suit - offset; suit2 < suit - offset + pile; ++suit2)
            {
              tokamak_card c2 = tokamak_card_regular(T, (suit2 + suits) % suits, rank);
              if (! (q->slots[c2].map & q_suit_mask))
              {
                // not possible at this offset
                q->slots[c].suit &= ~(1 << offset);
                ++count;
                break;
              }
            }
          }
        }
        if (! q->slots[c].suit)
        {
          // not possible at any offset
          q->slots[c].map &= ~q_suit_mask;
          if (! q->slots[c].map)
          {
            // contradiction, no possibilities
            return -1;
          }
        }
      }
    }
  }
  return count;
}

int q_piles(tokamak *T, deadline_t deadline, q_board *q)
{
  int r;
  int count = 0;
  while (0 < (r = q_ranks(T, deadline, q)))
  {
    count += r;
    if (_deadline_elapsed(deadline))
    {
      break;
    }
  }
  if (r < 0)
  {
    return -1;
  }
  while (0 < (r = q_suits(T, deadline, q)))
  {
    count += r;
    if (_deadline_elapsed(deadline))
    {
      break;
    }
  }
  if (r < 0)
  {
    return -1;
  }
  return count;
}

int q_fix(const tokamak *T, deadline_t deadline, q_board *q)
{
  int fixed = 0;
  int suits = tokamak_get_suits(T);
  int ranks = tokamak_get_suits(T);
  for (int suit = 0; suit < suits; ++suit)
  {
    for (int rank = 0; rank < ranks; ++rank)
    {
      tokamak_card c = tokamak_card_regular(T, suit, rank);
      if (q->slots[c].center == UNFIXED && __builtin_popcount(q->slots[c].map == 1))
      {
        for (int cell = 0; cell < 12; ++cell)
        {
          if (q->slots[c].map & (1 << cell))
          {
            if (q_rank[cell] && ! q->slots[c].rank)
            {
              // contradiction: not pileable
              return -1;
            }
            if (q_suit[cell] && ! q->slots[c].suit)
            {
              // contradiction: not pileable
              return -1;
            }
            if (q_joker[cell] > q->jokers)
            {
              // contradiction: not enough jokers
              return -1;
            }
            q->slots[c].center = cell;
            q->jokers -= q_joker[cell];
            ++fixed;
            break;
          }
        }
      }
    }
  }
  return fixed;
}

tokamak_card q_min(tokamak *T, deadline_t deadline, q_board *q)
{
  int suits = tokamak_get_suits(T);
  int ranks = tokamak_get_suits(T);
  // find position with minimum count > 1 to collapse
  int min_suit = -1, min_rank = -1, min_size = INT_MAX;
  for (int suit = 0; suit < suits; ++suit)
  {
    for (int rank = 0; rank < ranks; ++rank)
    {
      tokamak_cardset card = tokamak_card_regular(T, suit, rank);
      int size = __builtin_popcount(q->slots[card].map);
      if (1 < size && size < min_size)
      {
        min_size = size;
        min_suit = suit;
        min_rank = rank;
      }
    }
  }
  if (1 < min_size && min_size < INT_MAX)
  {
    return tokamak_card_regular(T, min_suit, min_rank);
  }
  else
  {
    return tokamak_card_invalid(T);
  }
}

int _tokamak_ai_quantum(tokamak *T, deadline_t deadline, q_board *q);

int q_collapse(tokamak *T, deadline_t deadline, q_board *q, tokamak_card card)
{
  q_board q_orig = *q;
  for (int ix = 0; ix < 12; ++ix)
  {
    int cell = q_order[ix];
    if (q->slots[card].map & (1 << cell))
    {
      q->slots[card].center = cell;
      q->slots[card].map = 1 << cell;
      q->jokers -= q_joker[cell];
      int r = _tokamak_ai_quantum(T, deadline, q);
      if (_deadline_elapsed(deadline))
      {
        return -1;
      }
      if (r == 0)
      {
        return 0; // success
      }
      // contradiction, rollback
      *q = q_orig;
    }
  }
  return -1;
}

int _tokamak_ai_quantum(tokamak *T, deadline_t deadline, q_board *q)
{
  if (0 > q_piles(T, deadline, q))
  {
    return -1; // contradiction
  }
  if (0 > q_fix(T, deadline, q))
  {
    return -1;
  }
  tokamak_card card = q_min(T, deadline, q);
  if (tokamak_card_is_valid(T, card))
  {
    int r = q_collapse(T, deadline, q, card);
    if (_deadline_elapsed(deadline))
    {
      return -1;
    }
    if (r < 0)
    {
      return -1; // failure
    }
    return 0; // success
  }
  else
  {
    if (_deadline_elapsed(deadline))
    {
      return -1;
    }
    return 0;
  }
}

int tokamak_ai_quantum(tokamak *T, deadline_t deadline)
{
  q_board q;
  q_init(T, deadline, &q, &T->table[1]);
  tokamak_table best_move = T->table[1];
  int best_score = T->table[1].hand_njokers + 2 * tokamak_cardset_size(T, T->table[1].hand_regular);
  q_board orig = q;
  while (! _deadline_elapsed(deadline))
  {
    int r = _tokamak_ai_quantum(T, deadline, &q);
    if (r == 0)
    {
      q_finish(T, deadline, &q, &T->table[1]);
      if (tokamak_move_is_valid(T))
      {
        int score = T->table[1].hand_njokers + 2 * tokamak_cardset_size(T, T->table[1].hand_regular);
        if (score < best_score)
        {
          best_score = score;
          best_move = T->table[1];
        }
      }
      if (best_score == 0)
      {
        break;
      }
    }
    else
    {
      q = orig;
    }
  }
  T->table[1] = best_move;
  return best_score;
}

#endif
