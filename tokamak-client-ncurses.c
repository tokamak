#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <locale.h>
#include <langinfo.h>
#include <ncurses.h>

#include "libtokamak.h"
#include "libtokamak-client.h"

#ifndef DEFAULT_LANGUAGE
#define DEFAULT_LANGUAGE "en"
#endif

// all strings UTF-8 unless otherwise specified
struct language_s
{
  const char *lang; // ASCII only
  const char *language;
  const char *title_screen[4];
  const char *main_menu[6];
  const char *arena_name[4];
  const char *nplayer_name[4];
  const char *player_name[4];
  const char *player_type[5];
  const char *network_menu[6];
  const char *in;
  const char *paused;
  const char *deck;
  const char *tokamak;
  const char *game_over;
  const char *winner[2];
  const char *stalemate;
  const char *abandoned;
  const char *round;
  const char *scores;
  const char *skill_level;
  const char *utf8_error; // ASCII only
  const char *initscr_error;
};
typedef struct language_s language;

// in alphabetical order of language name
#define LANGUAGES 2
static const language S[LANGUAGES] =
{ { "ca", "Català"
  , { "Tokamak (c) 2021"
    , "idea de la Laura Netz"
    , "codi de Claude Heiland-Allen"
    , 0
    }
  , { "Partit Amistós"
    , "Torneig"
    , "Jugadors"
    , "Arena"
    , "NETWORK"
    , "Abandonar"
    }
  , { "Clàssica"
    , "Salvatge"
    , "Minúscula"
    , "Bèstia"
    }
  , { "Solitari"
    , "Contra"
    , "Trio"
    , "Quartet"
    }
  , { "Jugador 1"
    , "Jugador 2"
    , "Jugador 3"
    , "Jugador 4"
    }
  , { "Humà"
    , "Aleatori"
    , "Bruta"
    , "Andrei"
    , "MCTS"
    }
  , { "COOKIE JAR"
    , "SERVER URL"
    , "REGISTER"
    , "NEW GAME"
    , "LIST GAMES"
    , "JOIN GAME"
    }
  , "al"
  , "En pausa"
  , "Baralla"
  , "Tokamak"
  , "Fi del joc!"
  , { "", " guanya!" }
  , "Sorteig d’impàs."
  , "Abandonat."
  , "Ronda"
  , "Puntuacions"
  , "Nivell d'habilitat"
  , "ERROR: el conjunt de caracters regionals no es UTF-8\n" // must be ASCII
  , "ERROR: no s'ha pogut inicialitzar ncurses\n"
  }
, { "en", "English"
  , { "Tokamak (c) 2021"
    , "idea by Laura Netz"
    , "code by Claude Heiland-Allen"
    , 0
    }
  , { "Friendly"
    , "Tournament"
    , "Players"
    , "Arena"
    , "NETWORK"
    , "Quit"
    }
  , { "Classic"
    , "Wild"
    , "Tiny"
    , "Beast"
    }
  , { "Solo"
    , "Versus"
    , "Trio"
    , "Quartet"
    }
  , { "Player 1"
    , "Player 2"
    , "Player 3"
    , "Player 4"
    }
  , { "Human"
    , "Random"
    , "Brute"
    , "Andrei"
    , "MCTS"
    }
  , { "COOKIE JAR"
    , "SERVER URL"
    , "REGISTER"
    , "NEW GAME"
    , "LIST GAMES"
    , "JOIN GAME"
    }
  , "in"
  , "Paused"
  , "Deck"
  , "Tokamak"
  , "Game over!"
  , { "", " wins!" }
  , "Stalemate."
  , "Abandoned."
  , "Round"
  , "Scores"
  , "Skill level"
  , "ERROR: the locale character set is not UTF-8\n" // must be ASCII
  , "ERROR: failed to initialize ncurses library\n"
  }
};
int L = 0;

static const tokamak_options arenas[4] =
  { { 13,  4,  2,  3,  5,  0, 0 }
  , { 13,  4, 12,  3,  5,  0, 0 }
  , {  4,  4,  0,  2,  2,  0, 0 }
  , {  6,  6,  6,  4,  4,  0, 0 }
  };
static const int narenas = 4;

static const char *card_back_name = "🂠 ";
static const char *card_joker_name = "🃟 ";
#define CARD_REGULAR_NAME_SUITS 6
#define CARD_REGULAR_NAME_RANKS 13
static const char *card_regular_name[CARD_REGULAR_NAME_SUITS][CARD_REGULAR_NAME_RANKS] =
{ { "🂡 ", "🂢 ", "🂣 ", "🂤 ", "🂥 ", "🂦 ", "🂧 ", "🂨 ", "🂩 ", "🂪 ", "🂫 ", /*"🂬 ",*/ "🂭 ", "🂮 " }
, { "🂱 ", "🂲 ", "🂳 ", "🂴 ", "🂵 ", "🂶 ", "🂷 ", "🂸 ", "🂹 ", "🂺 ", "🂻 ", /*"🂼 ",*/ "🂽 ", "🂾 " }
, { "🃁 ", "🃂 ", "🃃 ", "🃄 ", "🃅 ", "🃆 ", "🃇 ", "🃈 ", "🃉 ", "🃊 ", "🃋 ", /*"🃌 ",*/ "🃍 ", "🃎 " }
, { "🃑 ", "🃒 ", "🃓 ", "🃔 ", "🃕 ", "🃖 ", "🃗 ", "🃘 ", "🃙 ", "🃚 ", "🃛 ", /*"🃜 ",*/ "🃝 ", "🃞 " }
, { "A@", "2@", "3@", "4@", "5@", "6@", "7@", "8@", "9@", "X@", "J@", /*"C@",*/ "Q@", "K@" }
, { "A#", "2#", "3#", "4#", "5#", "6#", "7#", "8#", "9#", "X#", "J#", /*"C#",*/ "Q#", "K#" }
};

static const int delayms = 250;
static const int ai_thinking_time_ms = 5000;

static int max(int a, int b)
{
  return a > b ? a : b;
}

static int decimal_digits(int n)
{
  int digits = 1;
  for (int d = 2, s = 10; n >= s; ++d, s *= 10) // FIXME handle overflow?
  {
    digits = d;
  }
  return digits;
}

static void mvadd_decimal(int y, int x, int digits, int s)
{
  int sdigits = decimal_digits(s);
  for (int d = 0; d < sdigits; ++d, s /= 10)
  {
    const char *digit[10] =
      { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
    mvaddstr(y, x + digits - 1 - d, digit[s % 10]);
  }
  for (int d = sdigits; d < digits; ++d)
  {
    mvaddstr(y, x + digits - 1 - d, " ");
  }
}

static void title_screen(WINDOW *window, bool colour);
static void main_menu(WINDOW *window, bool colour);
static bool players_menu(WINDOW *window, bool colour, int *players, int player_type[4]);
static bool player_type_menu(WINDOW *window, bool colour, int *players, int player_type[4]);
static bool arena_menu(WINDOW *window, bool colour, int *arena);
static bool network_menu(WINDOW *window, bool colour);
static bool game_banner(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena);
static bool round_banner(WINDOW *window, bool colour, int round);
static bool player_banner(WINDOW *window, bool colour, int player, const int player_type[4]);
static bool result_banner(WINDOW *window, bool colour, int winner, const int player_type[4]);
static bool display_scores(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena, const int wins[4], int draws, int round, double cputime[4]);
static int play_move(WINDOW *window, bool colour, tokamak *T, const int player_type[4], int *cursor_suit, int *cursor_rank, double cputime[4]);
static int single_game(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena, int starter, double cputime[4]);
static void play_friendly(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena);
static void play_tournament(WINDOW *window, bool oclour, int nplayers, const int player_type[4], int arena);
static bool pause(WINDOW *window, bool colour, const tokamak *T, const int player_type[4]);

static void title_screen(WINDOW *window, bool colour)
{
  if (colour) color_set(1, NULL);
  erase();
  int w = 0, h = 0;
  getmaxyx(window, h, w);
  int h0 = 4 + (S[L].title_screen[3] ? 2 : 0);
  int w0 = max(max(max(
    strlen(S[L].title_screen[0]),
    strlen(S[L].title_screen[1])),
    strlen(S[L].title_screen[2])),
    S[L].title_screen[3] ? strlen(S[L].title_screen[3]) : 0);
  int y = (h - h0) / 2;
  int x = (w - w0) / 2;
  if (colour) color_set(10, NULL);
  attron(A_BOLD);
  mvaddstr(y, x, S[L].title_screen[0]);
  mvaddstr(y + 2, x, S[L].title_screen[1]);
  mvaddstr(y + 3, x, S[L].title_screen[2]);
  if (S[L].title_screen[3])
  {
    mvaddstr(y + 5, x, S[L].title_screen[3]);
  }
  attroff(A_BOLD);
  refresh();
  timeout(2000);
  switch (getch())
  {
    case 'q':
      return;
    default:
      main_menu(window, colour);
      return;
  }
}

static void main_menu(WINDOW *window, bool colour)
{
  int mainmenuitem = 0;
  int nplayers = 2;
  int player_type[4] = { 0, 0, 0, 0 };
  int arena = 0;
  while (true)
  {
    if (colour) color_set(1, NULL);
    erase();
    int w = 0, h = 0;
    getmaxyx(window, h, w);
    const int nitems = 6;
    int h0 = 3 * (nitems - 1) + 1;
    int y = (h - h0) / 2;
    if (colour) color_set(1, NULL);
    erase();
    if (colour) color_set(10, NULL);
    for (int i = 0; i < nitems; ++i)
    {
      int w0 = strlen(S[L].main_menu[i]);
      int x = (w - w0) / 2;
      if (colour) color_set(i == mainmenuitem ? 10 : 1, NULL);
      if (i == mainmenuitem)
      {
        attron(A_BOLD);
        mvaddstr(y + 3 * i, x - 2, "*");
      }
      mvaddstr(y + 3 * i, x , S[L].main_menu[i]);
      if (i == mainmenuitem)
      {
        mvaddstr(y + 3 * i, x + w0 + 1, "*");
        attroff(A_BOLD);
      }
    }
    refresh();
    timeout(-1);
    switch (getch())
    {
      case 'q':
        return;
      case '\n': case '\r': case ' ': case KEY_RIGHT:
        switch (mainmenuitem)
        {
          case 0:
            play_friendly(window, colour, nplayers, player_type, arena);
            break;
          case 1:
            play_tournament(window, colour, nplayers, player_type, arena);
            break;
          case 2:
            if (players_menu(window, colour, &nplayers, player_type))
            {
              return;
            }
            break;
          case 3:
            if (arena_menu(window, colour, &arena))
            {
              return;
            }
            break;
          case 4:
            if (network_menu(window, colour))
            {
              return;
            }
          case 5:
            return;
            break;
        }
        break;
      case KEY_DOWN:
        mainmenuitem = (mainmenuitem + 1) % nitems;
        break;
      case KEY_UP:
        mainmenuitem = (mainmenuitem + nitems - 1) % nitems;
        break;
      default:
        beep();
        break;
    }
  }
}

static bool players_menu(WINDOW *window, bool colour, int *players, int player_type[4])
{
  const int nplayers = 4;
  int nplayer1 = *players - 1;
  while (true)
  {
    if (colour) color_set(1, NULL);
    erase();
    int w = 0, h = 0;
    getmaxyx(window, h, w);
    int h0 = 3 * (nplayers - 1) + 1;
    int y = (h - h0) / 2;
    if (colour) color_set(1, NULL);
    erase();
    for (int i = 0; i < nplayers; ++i)
    {
      int w0 = strlen(S[L].nplayer_name[i]);
      int x = (w - w0) / 2;
      if (colour) color_set(i == nplayer1 ? 10 : 1, NULL);
      if (i == nplayer1)
      {
        attron(A_BOLD);
        mvaddstr(y + 3 * i, x - 2, "*");
      }
      mvaddstr(y + 3 * i, x , S[L].nplayer_name[i]);
      if (i == nplayer1)
      {
        mvaddstr(y + 3 * i, x + w0 + 1, "*");
        attroff(A_BOLD);
      }
    }
    refresh();
    timeout(-1);
    switch (getch())
    {
      case KEY_DOWN:
        nplayer1 = (nplayer1 + 1) % nplayers;
        break;
      case KEY_UP:
        nplayer1 = (nplayer1 + nplayers - 1) % nplayers;
        break;
      case 'q':
        return true;
        break;
      default:
        *players = nplayer1 + 1;
        return player_type_menu(window, colour, players, player_type);
    }
  }
}

static bool player_type_menu(WINDOW *window, bool colour, int *players, int player_type[4])
{
  int playertypeitem = 0;
  while (true)
  {
    if (colour) color_set(1, NULL);
    erase();
    int w = 0, h = 0;
    getmaxyx(window, h, w);
    int h0 = 3 * (*players - 1) + 1;
    int y = (h - h0) / 2;
    if (colour) color_set(1, NULL);
    erase();
    for (int i = 0; i < *players; ++i)
    {
      int w0 = strlen(S[L].player_type[player_type[i]]);
      int x = (w - w0) / 2;
      if (colour) color_set(i == playertypeitem ? 10 : 1, NULL);
      if (i == playertypeitem)
      {
        attron(A_BOLD);
        mvaddstr(y + 3 * i, x - 2, "*");
      }
      mvaddstr(y + 3 * i, x , S[L].player_type[player_type[i]]);
      if (i == playertypeitem)
      {
        mvaddstr(y + 3 * i, x + w0 + 1, "*");
        attroff(A_BOLD);
      }
    }
    refresh();
    timeout(-1);
    switch (getch())
    {
      case KEY_DOWN:
        playertypeitem = (playertypeitem + 1) % *players;
        break;
      case KEY_UP:
        playertypeitem = (playertypeitem + *players - 1) % *players;
        break;
      case 'q':
        return true;
        break;
      case '\n': case '\r':
        return false;
        break;
      default:
        player_type[playertypeitem] = (player_type[playertypeitem] + 1) % 5;
        break;
    }
  }
}

static bool arena_menu(WINDOW *window, bool colour, int *arena)
{
  while (true)
  {
    if (colour) color_set(1, NULL);
    erase();
    int w = 0, h = 0;
    getmaxyx(window, h, w);
    int h0 = 3 * (narenas - 1) + 1;
    int y = (h - h0) / 2;
    if (colour) color_set(1, NULL);
    erase();
    if (colour) color_set(10, NULL);
    for (int i = 0; i < narenas; ++i)
    {
      int w0 = strlen(S[L].arena_name[i]);
      int x = (w - w0) / 2;
      if (colour) color_set(i == *arena ? 10 : 1, NULL);
      if (i == *arena)
      {
        attron(A_BOLD);
        mvaddstr(y + 3 * i, x - 2, "*");
      }
      mvaddstr(y + 3 * i, x , S[L].arena_name[i]);
      if (i == *arena)
      {
        mvaddstr(y + 3 * i, x + w0 + 1, "*");
        attroff(A_BOLD);
      }
    }
    refresh();
    timeout(-1);
    switch (getch())
    {
      case 'q':
        return true;
        break;
      case KEY_DOWN:
        *arena = (*arena + 1) % narenas;
        break;
      case KEY_UP:
        *arena = (*arena + narenas - 1) % narenas;
        break;
      default:
        return false;
        break;
    }
  }
}

typedef struct
{
  char **game;
  size_t count;
} player_games_list;

void player_games_callback(void *cbdata, const char *game)
{
  player_games_list *list = cbdata;
  list->game = realloc(list->game, (++list->count) * sizeof(*list->game));
  list->game[list->count - 1] = strdup(game);
}

static bool network_menu(WINDOW *window, bool colour)
{
  (void) window;
  while (true)
  {
    if (colour) color_set(1, NULL);
    erase();
    refresh();
    timeout(-1);
    switch (getch())
    {
      case 'q':
        return true;
        break;
      default:
        return false;
        break;
    }
  }
}

static bool game_banner(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena)
{
  (void) player_type;
  if (colour) color_set(1, NULL);
  erase();
  int w = 0, h = 0;
  getmaxyx(window, h, w);
  int h0 = 5;
  int y = (h - h0) / 2;
  if (colour) color_set(10, NULL);
  attron(A_BOLD);
  int ln = strlen(S[L].nplayer_name[nplayers - 1]);
  mvaddstr(y, (w - ln) / 2, S[L].nplayer_name[nplayers - 1]);
  mvaddstr(y + 2, (w - strlen(S[L].in)) / 2, S[L].in);
  int la = strlen(S[L].arena_name[arena]);
  mvaddstr(y + 4, (w - la) / 2, S[L].arena_name[arena]);
  attroff(A_BOLD);
  refresh();
  timeout(2000);
  return getch() == 'q';
}

static bool round_banner(WINDOW *window, bool colour, int round)
{
  if (colour) color_set(1, NULL);
  erase();
  int w = 0, h = 0;
  getmaxyx(window, h, w);
  int digits = decimal_digits(round);
  int w0 = strlen(S[L].round) + 1 + digits;
  int h0 = 1;
  int x = (w - w0) / 2;
  int y = (h - h0) / 2;
  if (colour) color_set(10, NULL);
  attron(A_BOLD);
  mvaddstr(y, x, S[L].round);
  mvadd_decimal(y, x + w0 - digits, digits, round);
  attroff(A_BOLD);
  refresh();
  timeout(2000);
  return getch() == 'q';
}

static bool player_banner(WINDOW *window, bool colour, int player, const int player_type[4])
{
  if (colour) color_set(1, NULL);
  erase();
  int w = 0, h = 0;
  getmaxyx(window, h, w);
  int h0 = 3;
  int y = (h - h0) / 2;
  if (colour) color_set(10, NULL);
  attron(A_BOLD);
  const char *n = S[L].player_name[player];
  mvaddstr(y, (w - strlen(n)) / 2, n);
  const char *t = S[L].player_type[player_type[player]];
  mvaddstr(y + 2, (w - strlen(t)) / 2, t);
  attroff(A_BOLD);
  refresh();
  if (player_type[player])
  {
    timeout(delayms);
  }
  else
  {
    timeout(-1);
  }
  return getch() == 'q';
}

static int single_game(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena, int starter, double cputime[4])
{
  tokamak_options O = arenas[arena];
  O.players = nplayers;
  O.starter = starter;
  tokamak *T = tokamak_new(&O);
  if (! T)
  {
    return -2; // FIXME handle failure more permanently
  }
  tokamak_shuffle(T);
  tokamak_deal(T);
  int cursor_suit[4] = { 0, 0, 0, 0 };
  int cursor_rank[4] = { 0, 0, 0, 0 };
  int result = -3;
  while (result == -3)
  {
    if (player_banner(window, colour, tokamak_get_player(T), player_type))
    {
      result = -2;
    }
    else
    {
      result = play_move
        ( window, colour, T, player_type
        , &cursor_suit[tokamak_get_player(T)]
        , &cursor_rank[tokamak_get_player(T)]
        , cputime
        );
    }
  }
  result_banner(window, colour, result, player_type);
  tokamak_delete(T);
  return result;
}

static void play_friendly(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena)
{
  game_banner(window, colour, nplayers, player_type, arena);
  int starter = rand() % nplayers;
  double cputime[4] = { 0, 0, 0, 0 };
  single_game(window, colour, nplayers, player_type, arena, starter, cputime);
}

static bool result_banner(WINDOW *window, bool colour, int winner, const int player_type[4])
{
  if (colour) color_set(1, NULL);
  erase();
  int w = 0, h = 0;
  getmaxyx(window, h, w);
  if (colour) color_set(10, NULL);
  attron(A_BOLD);
  int h0 = 3;
  int y = (h - h0) / 2;
  mvaddstr(y, (w - strlen(S[L].game_over)) / 2, S[L].game_over);
  switch (winner)
  {
    case 0: case 1: case 2: case 3:
    {
      int lw = strlen(S[L].winner[0]);
      int lp = strlen(S[L].player_name[winner]);
      int lt = strlen(S[L].player_type[player_type[winner]]);
      int w0 = lw + lp + 1 + lt + strlen(S[L].winner[1]);
      int x = (w - w0) / 2;
      mvaddstr(y + 2, x, S[L].winner[0]);
      mvaddstr(y + 2, x + lw, S[L].player_name[winner]);
      mvaddstr(y + 2, x + lw + lp + 1, S[L].player_type[player_type[winner]]);
      mvaddstr(y + 2, x + lw + lp + 1 + lt, S[L].winner[1]);
      break;
    }
    case -1: case -2:
    {
      const char *s = winner == -1 ? S[L].stalemate : S[L].abandoned;
      int w0 = strlen(s);
      int x = (w - w0) / 2;
      mvaddstr(y + 2, x, s);
      break;
    }
  }
  attroff(A_BOLD);
  refresh();
  timeout(2000);
  return getch() == 'q';
}

static bool display_scores(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena, const int wins[4], int draws, int round, double cputime[4])
{
  (void) arena;
  (void) draws;
  (void) round;
  int hiscore = 0;
  for (int i = 0; i < nplayers; ++i)
  {
    hiscore = max(hiscore, wins[i]);
  }
  int digits = decimal_digits(hiscore);
  int w0 = strlen(S[L].scores);
  for (int i = 0; i < nplayers; ++i)
  {
    w0 = max(w0, digits + 2 + strlen(S[L].player_name[i]));
    w0 = max(w0, digits + 2 + strlen(S[L].player_type[player_type[i]]) + (player_type[i] ? 10 : 0));
  }
  if (colour) color_set(1, NULL);
  erase();
  int w = 0, h = 0;
  getmaxyx(window, h, w);
  int h0 = 2 + 3 * nplayers - 1;
  int y = (h - h0) / 2;
  int x = (w - w0) / 2;
  mvaddstr(y, (w - strlen(S[L].scores)) / 2, S[L].scores);
  for (int i = 0; i < nplayers; ++i)
  {
    if (wins[i] == hiscore)
    {
      if (colour) color_set(10, NULL);
      attron(A_BOLD);
    }
    else
    {
      if (colour) color_set(1, NULL);
    }
    mvadd_decimal(y + 2 + 3 * i, x, digits, wins[i]);
    mvaddstr(y + 2 + 3 * i, x + digits + 2, S[L].player_name[i]);
    if (player_type[i])
    {
      char str[100];
      snprintf(str, 100, "%s (%.2fs)", S[L].player_type[player_type[i]], cputime[i]);
      str[100-1] = 0;
      mvaddstr(y + 2 + 3 * i + 1, x + digits + 2, str);
    }
    else
    {
      mvaddstr(y + 2 + 3 * i + 1, x + digits + 2, S[L].player_type[player_type[i]]);
    }
    if (wins[i] == hiscore)
    {
      attroff(A_BOLD);
    }
  }
  refresh();
  timeout(2000);
  return getch() == 'q';
}

static void play_tournament(WINDOW *window, bool colour, int nplayers, const int player_type[4], int arena)
{
  double cputime[4] = { 0, 0, 0, 0 };
  int wins[4] = { 0, 0, 0 ,0 };
  int draws = 0;
  int round = 0;
  int starter = rand() % nplayers;
  bool running = true;
  while (running)
  {
    running &= ! round_banner(window, colour, ++round);
    if (running)
    {
      int winner = single_game(window, colour, nplayers, player_type, arena, starter, cputime);
      switch (winner)
      {
        case -1: // stalemate
          draws++;
          break;
        case -2: // abandoned
          running = false;
          break;
        default:
          wins[winner]++;
          break;
      }
      starter = (starter + 1) % nplayers;
    }
    running &= ! display_scores(window, colour, nplayers, player_type, arena, wins, draws, round, cputime);
  }
}

static int play_move(WINDOW *window, bool colour, tokamak *T, const int player_type[4], int *cursor_suit, int *cursor_rank, double cputime[4])
{
  int aiphase = 0;
  while (true)
  {
    if (colour) color_set(1, NULL);
    erase();
    int w = 0, h = 0;
    getmaxyx(window, h, w);
    int h0 = tokamak_get_suits(T) * 3 + tokamak_get_players(T) * 3 + 3;
    int w0 = tokamak_get_ranks(T) * 6 + 1;
    int x = (w - w0) / 2;
    int y = (h - h0) / 2;

    // draw deck
    mvaddstr(y, x, S[L].deck);
    for (int i = 0; i < tokamak_get_deck(T); ++i)
      mvaddstr(y + 1, x + 2 + i, card_back_name);

    // display hands
    for (int p = 0; p < tokamak_get_players(T); ++p)
    {
      if (colour) color_set(p == tokamak_get_player(T) ? 10 : 1, NULL);
      if (p == tokamak_get_player(T))
      {
        attron(A_BOLD);
      }
      const char *pn = S[L].player_name[p];
      mvaddstr(y + (p + 1) * 3, x, pn);
      const char *pt = S[L].player_type[player_type[p]];
      mvaddstr(y + (p + 1) * 3, x + strlen(pn) + 1, pt);
      for (int i = 0; i < tokamak_get_hand_size(T, p); ++i)
      {
        mvaddstr(y + (p + 1) * 3 + 1, x + 2 + i, card_back_name);
      }
      if (p == tokamak_get_player(T))
      {
        attroff(A_BOLD);
      }
    }

    // display tokamak
    if (colour) color_set(1, NULL);
    mvaddstr(y + 3 * (tokamak_get_players(T) + 1), x, S[L].tokamak);
    attron(A_BOLD);
    for (int suit = 0; suit < tokamak_get_suits(T); ++suit)
    {
      for (int rank = 0; rank < tokamak_get_ranks(T); ++rank)
      {
        int y0 = y + 3 * (tokamak_get_players(T) + 1) + 2 + 3 * suit;
        int x0 = x + 2 + 6 * rank;
        bool cursor = *cursor_rank == rank && *cursor_suit == suit &&
          player_type[tokamak_get_player(T)] == 0;
        tokamak_card i = tokamak_card_regular(T, suit, rank);
        tokamak_cardset card = tokamak_cardset_singleton(T, i);

        // cursor
        if (cursor)
        {
          if (colour) color_set(8, NULL);
          mvaddstr(y0 - 1, x0 - 2, "********");
          mvaddstr(y0, x0 - 2, "**");
        }
        // top left: rankwise
        if (tokamak_move_get_rank_regular(T) & card)
        {
          if (colour) color_set(4, NULL);
          mvaddstr(y0, x0, card_regular_name[suit][rank]);
        }
        else if (tokamak_move_get_rank_joker(T) & card)
        {
          if (colour) color_set(5, NULL);
          mvaddstr(y0, x0, card_joker_name);
        }
        else
        {
          if (colour) color_set(9, NULL);
          attroff(A_BOLD);
          mvaddstr(y0, x0, "::");
          attron(A_BOLD);
        }
        // top right: blank
        if (colour) color_set(9, NULL);
        attroff(A_BOLD);
        mvaddstr(y0, x0 + 2, "::");
        attron(A_BOLD);
        // cursor
        if (cursor)
        {
          if (colour) color_set(8, NULL);
          mvaddstr(y0, x0 + 4, "**");
          mvaddstr(y0 + 1, x0 - 2, "**");
        }
        // bottom left: hand
        if (tokamak_move_get_hand_regular(T) & card)
        {
          if (colour) color_set(2, NULL);
          mvaddstr(y0 + 1, x0, card_regular_name[suit][rank]);
        }
        else if (tokamak_move_get_hand_njokers(T) && cursor)
        {
          if (colour) color_set(3, NULL);
          mvaddstr(y0 + 1, x0, card_joker_name);
        }
        else
        {
          if (colour) color_set(9, NULL);
          attroff(A_BOLD);
          mvaddstr(y0 + 1, x0, "::");
          attron(A_BOLD);
        }
        // bottom right: suitwise
        if (tokamak_move_get_suit_regular(T) & card)
        {
          if (colour) color_set(6, NULL);
          mvaddstr(y0 + 1, x0 + 2, card_regular_name[suit][rank]);
        }
        else if (tokamak_move_get_suit_joker(T) & card)
        {
          if (colour) color_set(7, NULL);
          mvaddstr(y0 + 1, x0 + 2, card_joker_name);
        }
        else
        {
          if (colour) color_set(9, NULL);
          attroff(A_BOLD);
          mvaddstr(y0 + 1, x0 + 2, "::");
          attron(A_BOLD);
        }
        // cursor
        if (cursor)
        {
          if (colour) color_set(8, NULL);
          mvaddstr(y0 + 1, x0 + 4, "**");
          mvaddstr(y0 + 2, x0 - 2, "********");
        }
      }
    }
    attroff(A_BOLD);

    // update display
    refresh();

    // handle ai
    if (player_type[tokamak_get_player(T)] && aiphase == 0)
    {
      struct timespec refreshtime;
      clock_gettime(CLOCK_MONOTONIC, &refreshtime);
      refreshtime.tv_nsec += 1000000L * delayms;
      while (refreshtime.tv_nsec >= 1000000000L)
      {
        refreshtime.tv_sec += 1;
        refreshtime.tv_nsec -= 1000000000L;
      }
      struct timespec deadline;
      clock_gettime(CLOCK_MONOTONIC, &deadline);
      deadline.tv_nsec += 1000000L * ai_thinking_time_ms;
      while (deadline.tv_nsec >= 1000000000L)
      {
        deadline.tv_sec += 1;
        deadline.tv_nsec -= 1000000000L;
      }
      struct timespec thought_time_begin;
      clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &thought_time_begin);
      switch (player_type[tokamak_get_player(T)])
      {
        case 1:
          tokamak_ai_random_search_parallel(T, &deadline);
          break;
        case 2:
          tokamak_ai_brute_search_parallel(T, &deadline);
          break;
        case 3:
          tokamak_ai_andrei(T, &deadline);
          break;
        case 4:
          tokamak_ai_mcts(T, &deadline);
          break;
      }
      struct timespec thought_time_end;
      clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &thought_time_end);
      cputime[tokamak_get_player(T)] += (thought_time_end.tv_sec - thought_time_begin.tv_sec) + (thought_time_end.tv_nsec - thought_time_begin.tv_nsec) / (double) 1000000000L;
      clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &refreshtime, 0);
      aiphase = 1;
      continue;
    }
    else if (player_type[tokamak_get_player(T)] && aiphase == 1)
    {
      timeout(delayms);
      switch (getch())
      {
        case ERR: // timeout
          aiphase = 0;
aicommit:
          switch (tokamak_move_commit(T))
          {
            case tokamak_commit_ok:
              return -3;
            case tokamak_commit_invalid:
              beep();
              tokamak_move_rollback(T);
              goto aicommit;
              break;
            case tokamak_commit_stalemate:
              return -1;
              break;
            case tokamak_commit_winner:
              return tokamak_get_player(T);
              break;
          }
          break;
        case 'p': // pause
          if (pause(window, colour, T, player_type))
          {
            return -2;
          }
          break;
        case 'q': // quit
          aiphase = 0;
          return -2;
          break;
        default:
          beep();
          break;
      }
    }

    // handle input
    else
    {
      timeout(-1);
      switch (getch())
      {

        case '\n': case '\r': // commit
        {
          switch (tokamak_move_commit(T))
          {
            case tokamak_commit_ok:
              return -3;
              break;
            case tokamak_commit_invalid:
              beep();
              break;
            case tokamak_commit_stalemate:
              return -1;
              break;
            case tokamak_commit_winner:
              return tokamak_get_player(T);
              break;
          }
          break;
        }

        case KEY_BACKSPACE: // rollback
          tokamak_move_rollback(T);
          break;

        case KEY_UP: // cursor
          {
            int s = tokamak_get_suits(T);
            *cursor_suit = (*cursor_suit + s - 1) % s;
          }
          break;

        case KEY_DOWN: // cursor
          *cursor_suit = (*cursor_suit + 1) % tokamak_get_suits(T);
          break;

        case KEY_LEFT: // cursor
          {
            int r = tokamak_get_ranks(T);
            *cursor_rank = (*cursor_rank + r - 1) % r;
          }
          break;

        case KEY_RIGHT: // cursor
          *cursor_rank = (*cursor_rank + 1) % tokamak_get_ranks(T);
          break;

        case ' ': // swap (rank, suit) at cursor
          tokamak_move_swap_suit_rank(T, *cursor_suit, *cursor_rank);
          break;

        case KEY_NPAGE: // play from hand to tokamak
          if (! tokamak_move_play_from_hand(T, *cursor_suit, *cursor_rank))
          {
            beep();
          }
          break;

        case KEY_PPAGE: // play from tokamak to hand
          if (! tokamak_move_play_to_hand(T, *cursor_suit, *cursor_rank))
          {
            beep();
          }
          break;

        case 'p': // pause
          if (pause(window, colour, T, player_type))
          {
            return -2;
          }
          break;

        case 'q': // quit
          return -2;
          break;

        default: // invalid key
          beep();
          break;
      }
    }
  }
}

static bool pause(WINDOW *window, bool colour, const tokamak *T, const int player_type[4])
{
  (void) player_type;
  if (colour) color_set(1, NULL);
  erase();
  int w = 0, h = 0;
  getmaxyx(window, h, w);
  int h0 = 3;
  int w0 = strlen(S[L].player_name[tokamak_get_player(T)]);
  int y = (h - h0) / 2;
  if (colour) color_set(10, NULL);
  attron(A_BOLD);
  mvaddstr(y, (w - w0) / 2, S[L].player_name[tokamak_get_player(T)]);
  mvaddstr(y + 2, (w - strlen(S[L].paused)) / 2, S[L].paused);
  attroff(A_BOLD);
  refresh();
  timeout(-1);
  return getch() == 'q';
}

extern int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;

  // randomize
  srand(time(0));

  // configure language
  setlocale(LC_ALL, "");
  char *lang_ab = nl_langinfo(_NL_ADDRESS_LANG_AB);
  char *lang = 0;
  if (lang_ab && *lang_ab)
  {
    lang = strndup(lang_ab, 2);
  }
  else
  {
    lang = strdup(DEFAULT_LANGUAGE);
  }
  for (int l = 0; l < LANGUAGES; ++l)
  {
    if (0 == strcmp(lang, S[l].lang))
    {
      L = l;
      break;
    }
  }

  // abort if UTF-8 is required but not available
  if (0 != strcmp("UTF-8", nl_langinfo(CODESET)) && S[L].utf8_error)
  {
    fprintf(stderr, "%s", S[L].utf8_error);
    return 1;
  }

  // initialize ncurses
  WINDOW *window = initscr();
  if (! window)
  {
    fprintf(stderr, "%s", S[L].initscr_error);
    return 1;
  }
  start_color();
  bool colour = has_colors() && COLOR_PAIRS > 10;
  if (colour)
  {
    init_pair(1, 7, 0);  // labels
    init_pair(2, 15, 2); // hand
    init_pair(3, 15, 2); // hand joker
    init_pair(4, 15, 1); // rank
    init_pair(5, 15, 1); // rank joker
    init_pair(6, 15, 4); // suit
    init_pair(7, 15, 4); // suit joker
    init_pair(8, 0, 15); // cursor
    init_pair(9, 8, 8);  // blank
    init_pair(10, 2, 0); // active player
  }
  noecho();
  curs_set(0);
  keypad(window, TRUE);

  const char *cookiejar = "tokamak-client-ncurses.cookies";
  if (argc > 1)
  {
    cookiejar = argv[1];
  }
  const char *server = "https://localhost:8080";
  if (argc > 2)
  {
    server = argv[2];
  }
  char *playername = strdup("tokamak-client-ncurses-#######");
  char *hash = strchr(playername, '#');
  while (*hash == '#')
  {
    *hash++ = '0' + (rand() % 10);
  }
  if (argc > 3)
  {
    server = argv[2];
  }


  tokamak_client_new(server, cookiejar);

  // enter game
  title_screen(window, colour);

  // exit ncurses
  delwin(window);
  endwin();
  refresh();
  return 0;
}
