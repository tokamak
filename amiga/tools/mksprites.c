#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
  if (argc != 3)
  {
    fprintf(stderr, "usage: %s fps bpm\n", argv[0]);
    return 1;
  }
  double fps = atof(argv[1]);
  double bpm = atof(argv[2]);
  int frames = round(fps / (bpm / 60));
  bpm = fps / frames * 60;
  fprintf(stderr, "%d\t%.18f\n", frames, bpm);
  int width, height;
  fscanf(stdin, "P5\n%d %d 255", &width, &height);
  if ('\n' != fgetc(stdin))
  {
    return 1;
  }
  unsigned char *input = malloc(width * height);
  fread(input, width * height, 1, stdin);
  unsigned char *stage1 = malloc(width * height);
  unsigned char *stage2 = malloc(width * height);
  unsigned char *stage3 = malloc(width * height);
  unsigned char *output = malloc(width * height);
  for (int frame = 0; frame < 2 * frames; ++frame)
  {
    double degrees = 90.0 * (frame - frames) / frames;
    double radians = degrees * M_PI / 180;
    double shearx = sin(radians);
    double sheary = -tan(radians / 2);
    for (int y = 0; y < height; ++y)
    {
      for (int x = 0; x < width; ++x)
      {
        int iy = round(y + (x - width / 2) * sheary);
        iy %= height;
        iy += height;
        iy %= height;
        stage1[y * width + x] = input[iy * width + x];
      }
    }
    for (int y = 0; y < height; ++y)
    {
      for (int x = 0; x < width; ++x)
      {
        int ix = round(x + (y - height / 2) * shearx);
        ix %= width;
        ix += width;
        ix %= width;
        stage2[y * width + x] = stage1[y * width + ix];
      }
    }
    for (int y = 0; y < height; ++y)
    {
      for (int x = 0; x < width; ++x)
      {
        int iy = round(y + (x - width / 2) * sheary);
        iy %= height;
        iy += height;
        iy %= height;
        output[y * width + x] = stage2[iy * width + x];
      }
    }
    if(0)
    for (int y = 0; y < height; ++y)
    {
      for (int x = 0; x < width; ++x)
      {
        int c = stage3[y * width + x];
        if (c)
        {
          int n = 0;
          for (int dy = -1; dy <= 1; ++dy)
          {
            for (int dx = -1; dx <= 1; ++dx)
            {
              if (! dx == ! dy) continue;
              int iy = y + dy;
              iy %= height;
              iy += height;
              iy %= height;
              int ix = x + dx;
              ix %= width;
              ix += width;
              ix %= width;
              n += ! stage3[iy * width + ix];
            }
          }
          c *= (n > 0);
        }
        output[y * width + x] = c;
      }
    }
    int y = height / 2;
    int x;
    for (x = 0; x < width; ++x)
    {
      if (output[y * width + x]) break;
    }
    //assert(x < width);
    if (0)
    do
    {
      output[y * width + x] = 0;
      int found = 0;
      int dx = 0;
      int dy = 0;
      for (dy = -1; dy <= 1; ++dy)
      {
        for (dx = -1; dx <= 1; ++dx)
        {
          if (dx == 0 && dy == 0) continue;
          if (output[(y + dy) * width + (x + dx)] == 255)
          {
            found = 1;
            break;
          }
        }
        if (found) break;
      }
      if (! found) break;
      y += dy;
      x += dx;
      output[y * width + x] = 0;
      found = 0;
      dx = 0;
      dy = 0;
      for (dy = -1; dy <= 1; ++dy)
      {
        for (dx = -1; dx <= 1; ++dx)
        {
          if (dx == 0 && dy == 0) continue;
          if (output[(y + dy) * width + (x + dx)] == 255)
          {
            found = 1;
            break;
          }
        }
        if (found) break;
      }
      output[y * width + x] = 128;
      if (! found) break;
      y += dy;
      x += dx;
    }
    while (1);
    for (int y = 0; y < height; ++y)
    for (int x = 0; x < width; ++x)
    if (output[y * width + x])
    {
      output[y * width + x] = 255;
    }
    printf("P5\n%d %d\n255\n", width, height);
    fwrite(output, width * height, 1, stdout);
  }
  return 0;
}
