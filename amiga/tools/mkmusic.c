#include <math.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  char sep[2] = ", ";
  // pitch of A in Hz
  double A = 415.0;
  // Bach WTC / Lehman
  // <https://i0.wp.com/www.rolf-musicblog.net/wp-content/uploads/2020/08/tuning_table_final.png>
  double cents[12] = { 0, 4, 0, 6, 4, 2, 4, -2, 8, 2, 4, 4 };
  // Amiga NTSC + PAL
  double clocks[2] = { 3579545.0, 3546895.0 };
  // compute scales
  double hz0 = A / pow(2, ((5 * 12 + 0) * 100 + cents[0]) / 1200);
  printf("const ULONG period[2][10][12] = {\n");
  int first = 1;
  for (int pal = 0; pal < 2; ++pal)
  {
    for (int octave = 0; octave < 10; ++octave)
    {
      for (int note = 0; note < 12; ++note)
      {
        double hz = hz0 * pow(2, ((octave * 12 + note) * 100 + cents[note]) / 1200);
        int period = clocks[pal] / hz;
        printf("%c %d\n", sep[first], period);
        first = 0;
      }
    }
  }
  printf("};\n");
  printf("const BYTE waveform_index[32] = {\n");
  int k = 0;
  first = 1;
  for (int a = 0; a < 2; ++a)
  for (int b = 0; b < 2; ++b)
  for (int c = 0; c < 2; ++c)
  for (int d = 0; d < 2; ++d)
  for (int e = 0; e < 2; ++e)
  {
    if ((0 == a + b + c + d + e) || (e && (d || c)) || (d && c) || (d && b) || (c && a))
    {
      printf("%c %d\n", sep[first], -1);
    }
    else
    {
      printf("%c %d\n", sep[first], k++);
    }
    first = 0;
  }
  printf("};\n");
  int block = 4;
  printf("#define waveform_block %d\n", block);
  int suits[] = { 4, 4, 6 };
  int ranks[] = { 13, 4, 6 };
  for (int q = 0; q < 3; ++q)
  {
    int length = block * suits[q] * ranks[q];
    printf("#define waveform_%dx%d_length %d\n", ranks[q], suits[q], length);
    printf("const BYTE waveform_%dx%d_data[4][%d][%d] = {\n", ranks[q], suits[q], k, length);
    first = 1;
    for (int j = 0; j < 4; ++j)
    for (int a = 0; a < 2; ++a)
    for (int b = 0; b < 2; ++b)
    for (int c = 0; c < 2; ++c)
    for (int d = 0; d < 2; ++d)
    for (int e = 0; e < 2; ++e)
    {
      if ((0 == a + b + c + d + e) || (e && (d || c)) || (d && c) || (d && b) || (c && a))
      {
        continue;
      }
      for (int i = 0; i < length; ++i)
      {
        double w = 0;
        w += e * sin(2 * M_PI *  3 * (i + 0.5) / length);
        w += d * sin(2 * M_PI *  4 * (i + 0.5) / length);
        w += c * sin(2 * M_PI *  5 * (i + 0.5) / length);
        w += b * sin(2 * M_PI *  8 * (i + 0.5) / length);
        w += a * sin(2 * M_PI * 10 * (i + 0.5) / length);
        int peak = 0x7f * sin((1 + j) * M_PI * w);
        printf("%c %d\n", sep[first], peak);
        first = 0;
      }
    }
    printf("};\n");
  }
  return 0;
}
