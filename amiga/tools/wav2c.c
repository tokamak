#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sndfile.h>

int main(int argc, char **argv)
{
  SF_INFO info = {0};
  SNDFILE *in = sf_open(argv[1], SFM_READ, &info);
  int16_t *buffer = malloc(sizeof(*buffer) * info.channels * info.frames);
  sf_readf_short(in, buffer, info.frames);
  sf_close(in);
  for (int i = 0; i < info.channels * info.frames; ++i)
  {
    int16_t x = buffer[i];
    x >>= 2;
    printf("%s 0x%04x \n", i == 0 ? " " : ",", x & 0xFFFF);
  }
  return 0;
}
