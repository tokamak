#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
  const char *name = argc > 1 ? argv[1] : "image";
  int width, height;
  fscanf(stdin, "P5\n%d %d 255", &width, &height);
  if ('\n' != fgetc(stdin))
  {
    return 1;
  }
  unsigned char *input = malloc(width * height);
  fread(input, width * height, 1, stdin);
  int palette[256];
  memset(palette, 0, sizeof(palette));
  for (int k = 0; k < width * height; ++k)
  {
    palette[input[k]]++;
  }
  int colours = 0;
  for (int k = 0; k < 256; ++k)
  {
    if (palette[k])
    {
      palette[k] = colours++;
    }
    else
    {
      palette[k] = -1;
    }
  }
  int depth = 1;
  while (colours > 1 << depth)
  {
    ++depth;
  }
  if (argc > 2 && 0 == strcmp("--no-palette", argv[2]))
  {
    for (int k = 0; k < 256; ++k)
    {
      palette[k] = k;
    }
  }
  int width_in_words = (width + 15) / 16;
  printf("#define %s_depth %d\n", name, depth);
  printf("#define %s_height %d\n", name, height);
  printf("#define %s_width_in_words %d\n", name, width_in_words);
  printf("#define %s_bytes_per_row %d\n", name, width_in_words * 2);
  printf("#define %s_width %d\n", name, width);
  printf("__chip const UWORD %s_data[%s_depth][%s_height][%s_width_in_words] =\n", name, name, name, name);
  char sep = '{';
  for (int plane = 0; plane < depth; ++plane)
  {
    for (int y = 0; y < height; ++y)
    {
      int x = 0;
      for (int word = 0; word < width_in_words; ++word)
      {
        int data = 0;
        for (int bit = 15; bit >= 0 && x < width; --bit, ++x)
        {
          if (palette[input[y * width + x]] & (1 << plane))
          {
            data |= 1 << bit;
          }
        }
        printf("%c 0x%04x", sep, data);
        sep = ',';
      }
      printf("\n");
    }
    printf("\n");
  }
  printf("};\n");
  free(input);
  printf("struct Image %s_image = { 0, 0, %s_width, %s_height, %s_depth, (UWORD *) &%s_data[0][0][0], 1, 0, 0 };\n", name, name, name, name, name);
  printf("struct BitMap %s_bitmap = { %s_bytes_per_row, %s_height, 0, %s_depth, 0, { ", name, name, name, name);
  for (int i = 0; i < depth; ++i)
  {
    printf("(PLANEPTR) &%s_data[%d][0][0], ", name, i);
  }
  for (int i = depth; i < 8; ++i)
  {
    printf("0%s", i < 7 ? ", " : " } };\n");
  }
  return 0;
}
