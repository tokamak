# Tokamak (classic Amiga version)

## Build

Requires `amiga-gcc`, plus various other tools
(to be documented later...).

Compile with `make`.

Output Amiga disk directory in `Tokamak/`,
in particular the Amiga executable `Tokamak/Tokamak.000`.

## Run

Boot the Amiga disk directory in `FS-UAE` emulator,
or on real Amiga hardware.
