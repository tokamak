#pragma once

struct BitMap;

void destroyBitMap(struct BitMap *bitmap, int width, int height, int depth);
struct BitMap *createBitMap(int width, int height, int depth);
