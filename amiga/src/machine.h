#pragma once

#include <dos/dos.h>
#include <exec/types.h>
#include <graphics/gfx.h>
#include <intuition/intuition.h>

#include "andrei.h"
#include "audio.h"
#include "graphics.h"
#include "../../libtokamak.h"

#define TIMER_PRIORITY 60
#define AUDIO_PRIORITY 50
#define GRAPHICS_PRIORITY 40
#define NORMAL_PRIORITY 0
#define SONIFY_PRIORITY -10
#define ANDREI_PRIORITY -10
#define IDLE_PRIORITY -50

struct State
{
  volatile bool deadline;
  BPTR out;
  ULONG ctrlc;
  int counter;

  struct Screen *screen;
  struct BitMap *bitmap;
  struct Window *window;

  tokamak_options options;
  tokamak T;
  
  struct AudioState audio;
  struct GraphicsState graphics;
};

void ENGINE_main(struct State *s);

struct State *initialize(BPTR out, ULONG ctrlc);
void cleanup(struct State *State);
bool poll_ui(struct State *s);
