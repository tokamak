#include <proto/alib.h>
#include <proto/exec.h>
#include <proto/graphics.h>

#include "graphics.h"
#include "machine.h"

#include "../graphics/vertical.h"
#include "../graphics/vertical_open.h"
#include "../graphics/vertical_joker.h"
#include "../graphics/horizontal.h"
#include "../graphics/horizontal_open.h"
#include "../graphics/horizontal_joker.h"

extern BYTE AUDIO_volume;

enum graphics_action
{
  graphics_idle = 0,
  graphics_draw_corners,
  graphics_andrei,
  graphics_gameover
};

struct Task *GRAPHICS_task;
volatile bool GRAPHICS_running, GRAPHICS_stopped, GRAPHICS_interrupted, GRAPHICS_started, GRAPHICS_ready;
volatile enum graphics_action GRAPHICS_action;
volatile ULONG GRAPHICS_counter;
struct State *GRAPHICS_state;

volatile bool GRAPHICS_draw_corners_on;
volatile int GRAPHICS_draw_corners_ranks;
volatile int GRAPHICS_draw_corners_suits;

void GRAPHICS_draw_board_diff(struct State *s)
{
  struct GraphicsState *g = &s->graphics;
  struct RastPort *rp = &s->screen->RastPort;
  // read state to draw
  tokamak_cardset T_hand_regular = g->T_new.hand_regular;
  tokamak_cardset T_rank_regular = g->T_new.rank_regular;
  tokamak_cardset T_suit_regular = g->T_new.suit_regular;
  tokamak_cardset T_rank_joker = g->T_new.rank_joker;
  tokamak_cardset T_suit_joker = g->T_new.suit_joker;
  int T_hand_njokers = g->T_new.hand_njokers;
  // compute difference
  tokamak_cardset hand_regular = g->T_old.hand_regular ^ T_hand_regular;
  tokamak_cardset rank_regular = g->T_old.rank_regular ^ T_rank_regular;
  tokamak_cardset suit_regular = g->T_old.suit_regular ^ T_suit_regular;
  tokamak_cardset rank_joker = g->T_old.rank_joker ^ T_rank_joker;
  tokamak_cardset suit_joker = g->T_old.suit_joker ^ T_suit_joker;
  int jokers0 = g->T_old.hand_njokers;
  int jokers1 = T_hand_njokers;
  // check if we need to draw anything
  bool unchanged = ! hand_regular && ! rank_regular && ! suit_regular && ! rank_joker && ! suit_joker && jokers0 == jokers1;
  if (unchanged)
  {
    return;
  }
  // store state
  g->T_old = g->T_new;
  int x0 = (s->screen->Width - 24 * s->T.ranks) >> 1;
  int y0 = (s->screen->Height - 24 * s->T.suits) >> 1;
  // draw jokers in hand
  if (jokers0 > jokers1)
  {
    int tmp = jokers0; jokers0 = jokers1; jokers1 = tmp;
  }
  int jy = y0 - 24 - 12;
  for (int j = jokers0, jx = x0 + jokers0 * 24; j < jokers1; ++j, jx += 24)
  {
    BltBitMapRastPort(&vertical_open_bitmap, 0, 0, rp, jx, jy, vertical_open_width, vertical_open_height, 0x60);
  }
  // draw the table
  for (int suit = 0, y = y0; suit < s->T.suits; ++suit, y += 24)
  {
    for (int rank = 0, x = x0; rank < s->T.ranks; ++rank, x += 24)
    {
      tokamak_cardset c = tokamak_cardset_singleton(&s->T, tokamak_card_regular(&s->T, suit, rank));
      if (hand_regular & c)
      {
        BltBitMapRastPort(&vertical_open_bitmap, 0, 0, rp, x, y, vertical_open_width, vertical_open_height, 0x60);
      }
      if (rank_regular & c)
      {
        BltBitMapRastPort(&vertical_bitmap, 0, 0, rp, x, y, vertical_width, vertical_height, 0x60);
      }
      if (rank_joker & c)
      {
        BltBitMapRastPort(&vertical_joker_bitmap, 0, 0, rp, x, y, vertical_joker_width, vertical_joker_height, 0x60);
      }
      if (suit_regular & c)
      {
        BltBitMapRastPort(&horizontal_bitmap, 0, 0, rp, x, y, horizontal_width, horizontal_height, 0x60);
      }
      if (suit_joker & c)
      {
        BltBitMapRastPort(&horizontal_joker_bitmap, 0, 0, rp, x, y, horizontal_joker_width, horizontal_joker_height, 0x60);
      }
    }
  }
  WaitBlit();
}

void GRAPHICS_main(void)
{
  GRAPHICS_running = true;
  struct Library *GfxBase;
  if ((GfxBase = OpenLibrary((CONST_STRPTR) "graphics.library", 0L)))
  {
    struct State *s = GRAPHICS_state;
    struct GraphicsState *g = &s->graphics;
    struct RastPort *rp = &s->screen->RastPort;

    while (GRAPHICS_running)
    {
      switch (GRAPHICS_action)
      {

        default:
        case graphics_idle:
        {
          WaitTOF();
          ++GRAPHICS_counter;
          break;
        }

        case graphics_draw_corners:
        {
          GRAPHICS_ready = false;
          GRAPHICS_started = true;
          int on = GRAPHICS_draw_corners_on;
          int ranks = GRAPHICS_draw_corners_ranks;
          int suits = GRAPHICS_draw_corners_suits;
          int x0 = ((s->screen->Width - 24 * ranks) >> 1) - 1;
          int y0 = ((s->screen->Height - 24 * suits) >> 1) - 1;
          if (on)
          {
            WaitTOF();
            ++GRAPHICS_counter;
            SetRast(rp, 0);
          }
          for (int suit = 0, y = y0; suit <= suits && GRAPHICS_running && ! GRAPHICS_interrupted; ++suit, y += 24)
          {
            for (int rank = 0, x = x0; rank <= ranks && GRAPHICS_running && ! GRAPHICS_interrupted; ++rank, x += 24)
            {
              WaitTOF();
              ++GRAPHICS_counter;
              WritePixel(rp, x + 1, y + 0);
              WritePixel(rp, x + 0, y + 1);
              WritePixel(rp, x + 1, y + 1);
              WritePixel(rp, x + 2, y + 1);
              WritePixel(rp, x + 1, y + 2);
            }
          }
          if (! on)
          {
            WaitTOF();
            ++GRAPHICS_counter;
            SetRast(rp, 0);
          }
          g->T_old = (tokamak_table){0};
          GRAPHICS_ready = true;
          GRAPHICS_action = graphics_idle;
          break;
        }

        case graphics_andrei:
        {
          GRAPHICS_ready = false;
          GRAPHICS_started = true;
          // calculate dims
          int x0 = (s->screen->Width - 24 * s->T.ranks) >> 1;
          int y0 = (s->screen->Height - 24 * s->T.suits) >> 1;
          int y = y0 + s->T.suits * 24 + 36;
          Move(rp, x0, y);
          WaitTOF();
          ++GRAPHICS_counter;
          Text(rp, (CONST_STRPTR) "AndreI is thinking...", 21);
          Forbid();
          g->T_new = s->T.table[1];
          Permit();
          GRAPHICS_draw_board_diff(s);
          while (GRAPHICS_running && ! GRAPHICS_interrupted)
          {
            WaitTOF();
            ++GRAPHICS_counter;
            if (g->verbose)
            {
              Forbid();
              g->T_new = s->T.table[1];
              Permit();
              GRAPHICS_draw_board_diff(s);
            }
          }
          if (GRAPHICS_running)
          {
            Move(rp, x0, y);
            Text(rp, (CONST_STRPTR) "AndreI is thinking...", 21);
            Forbid();
            g->T_new = s->T.table[1];
            Permit();
            GRAPHICS_draw_board_diff(s);
          }
          GRAPHICS_ready = true;
          GRAPHICS_action = graphics_idle;
          break;
        }

        case graphics_gameover:
        {
          GRAPHICS_ready = false;
          GRAPHICS_started = true;
          while (GRAPHICS_running && ! GRAPHICS_interrupted && AUDIO_volume > 0)
          {
            WaitTOF();
            ++GRAPHICS_counter;
          }
          GRAPHICS_ready = true;
          GRAPHICS_action = graphics_idle;
          break;
        }

      }
    }
    CloseLibrary(GfxBase);
  }
  GRAPHICS_stopped = true;
}

bool GRAPHICS_start(struct State *s)
{
  GRAPHICS_state = s;
  GRAPHICS_running = true;
  GRAPHICS_interrupted = false;
  GRAPHICS_stopped = false;
  GRAPHICS_ready = true;
  GRAPHICS_started = false;
  GRAPHICS_action = graphics_idle;
  GRAPHICS_counter = 0;
  //s->graphics.verbose = true;
  if ((GRAPHICS_task = CreateTask((CONST_STRPTR) "Tokamak/graphics", GRAPHICS_PRIORITY, (void *) (ULONG) GRAPHICS_main, 4000L)))
  {
    return true;
  }
  return false;
}

ULONG GRAPHICS_stop(void)
{
  GRAPHICS_running = false;
  while (! GRAPHICS_stopped)
  {
    WaitTOF();
  }
  return GRAPHICS_counter;
}

void GRAPHICS_interrupt(void)
{
  GRAPHICS_interrupted = true;
}

bool GRAPHICS_is_running(void)
{
  return ! GRAPHICS_ready;
}

void GRAPHICS_draw_corners(bool on, int ranks, int suits)
{
  GRAPHICS_draw_corners_on = on;
  GRAPHICS_draw_corners_ranks = ranks;
  GRAPHICS_draw_corners_suits = suits;
  GRAPHICS_interrupted = false;
  GRAPHICS_started = false;
  GRAPHICS_action = graphics_draw_corners;
  while (! GRAPHICS_started)
  {
    WaitTOF();
  }
}

void GRAPHICS_andrei(bool on)
{
  if (on)
  {
    GRAPHICS_interrupted = false;
    GRAPHICS_started = false;
    GRAPHICS_action = graphics_andrei;
    while (! GRAPHICS_started)
    {
      WaitTOF();
    }
  }
  else
  {
    GRAPHICS_interrupt();
  }
}

void GRAPHICS_gameover(int player, tokamak_commit commit)
{
  (void) player;
  (void) commit;
}
