#include <devices/audio.h>
#include <proto/alib.h>
#include <proto/exec.h>
#include <proto/graphics.h>

#include "audio.h"
#include "machine.h"
#include "sonify.h"
#include "../sounds/music.h"

ULONG SONIFY_rank_length, SONIFY_suit_length;
BYTE *SONIFY_rank_data, *SONIFY_suit_data;

#define corner_length 10
__chip const BYTE corner_data[corner_length] = {
  0x00
, 0x7F
, 0x80
, 0x7F
, 0x80
, 0x7F
, 0x80
, 0x7F
, 0x80
, 0x00
};

struct AUDIO
{
  struct IOAudio *io[2];
  struct MsgPort *port[2];
  struct Message *msg;
  ULONG device;
  ULONG clock;
  ULONG speed;
  UBYTE *data;
  ULONG size;
  UBYTE chan[4];
  UBYTE *chans[4];
  struct Task *task;
  BYTE oldpri;
};

void AUDIO_delete(struct AUDIO *a)
{
  if (! a->device) CloseDevice((struct IORequest *) a->io[0]);
  if (a->port[0]) DeletePort(a->port[0]);
  if (a->port[1]) DeletePort(a->port[1]);
  if (a->io[0]) FreeMem(a->io[0], sizeof(struct IOAudio));
  if (a->io[1]) FreeMem(a->io[1], sizeof(struct IOAudio));
  if (a->task) SetTaskPri(a->task, a->oldpri);
  if (a->data) FreeMem(a->data, a->size);
  FreeMem(a, sizeof(struct AUDIO));
}

struct AUDIO *AUDIO_new(ULONG isPAL, ULONG samplerate, ULONG blocksize, struct AUDIO *copyFrom, BYTE channel)
{
  BYTE c = 0;
  struct AUDIO *a = (struct AUDIO *) AllocMem(sizeof(struct AUDIO), MEMF_PUBLIC | MEMF_CLEAR);
  if (! a)
  {
    return a;
  }
  a->device = 1;
  a->clock = isPAL ? 3546895L : 3579545L;
  a->speed = a->clock / samplerate;
  a->size = blocksize * 2;
  a->data = (UBYTE *) AllocMem(a->size, MEMF_CHIP | MEMF_CLEAR);
  a->io[0] = (struct IOAudio *) AllocMem(sizeof(struct IOAudio), MEMF_PUBLIC | MEMF_CLEAR);
  a->io[1] = (struct IOAudio *) AllocMem(sizeof(struct IOAudio), MEMF_PUBLIC | MEMF_CLEAR);
  a->port[0] = CreatePort(0, 0);
  a->port[1] = CreatePort(0, 0);
  a->chan[0] = 15;
  a->chan[1] = 15;
  a->chan[2] = 15;
  a->chan[3] = 15;
  a->chans[0] = &a->chan[0];
  a->chans[1] = &a->chan[1];
  a->chans[2] = &a->chan[2];
  a->chans[3] = &a->chan[3];
  if (a->data && a->io[0] && a->port[0] && a->io[1] && a->port[1])
  {
    if (copyFrom)
    {
      *(a->io[0]) = *(copyFrom->io[0]);
      a->io[0]->ioa_Request.io_Message.mn_ReplyPort = a->port[0];
      a->io[0]->ioa_Request.io_Unit = (void *) (1 << channel);
    }
    else
    {
      while (a->device && c < 4)
      {
        a->io[0]->ioa_Request.io_Message.mn_ReplyPort = a->port[0];
        a->io[0]->ioa_Request.io_Message.mn_Node.ln_Pri = 127;
        a->io[0]->ioa_AllocKey = 0;
        a->io[0]->ioa_Data = a->chans[c++];
        a->io[0]->ioa_Length = 1;
        a->device = OpenDevice((CONST_STRPTR) AUDIONAME, 0L, (struct IORequest *) a->io[0], 0L);
      }
    }
    a->io[0]->ioa_Request.io_Command = CMD_WRITE;
    a->io[0]->ioa_Request.io_Flags = ADIOF_PERVOL;
    a->io[0]->ioa_Volume = 64;
    a->io[0]->ioa_Period = a->speed;
    a->io[0]->ioa_Cycles = 1;
    a->io[0]->ioa_Length = blocksize;
    *(a->io[1]) = *(a->io[0]);
    a->io[1]->ioa_Request.io_Message.mn_ReplyPort = a->port[1];
    a->io[0]->ioa_Data = a->data;
    a->io[1]->ioa_Data = a->data + blocksize;
  }
  if (a->device && ! copyFrom)
  {
    AUDIO_delete(a);
    a = 0;
  }
  return a;
}

void AUDIO_oneshot(struct IOAudio *io, const BYTE *data, LONG length, LONG volume, LONG period)
{
  io->ioa_Request.io_Command = CMD_WRITE;
  io->ioa_Request.io_Flags = ADIOF_PERVOL;
  io->ioa_Volume = volume;
  io->ioa_Period = period;
  io->ioa_Cycles = 1;
  io->ioa_Length = length;
  io->ioa_Data = (UBYTE *) data;
  BeginIO((struct IORequest *) io);
}

void AUDIO_loop(struct IOAudio *io, const BYTE *data, LONG length, LONG volume, LONG period)
{
  io->ioa_Request.io_Command = CMD_WRITE;
  io->ioa_Request.io_Flags = ADIOF_PERVOL;
  io->ioa_Volume = volume;
  io->ioa_Period = period;
  io->ioa_Cycles = 0;
  io->ioa_Length = length;
  io->ioa_Data = (UBYTE *) data;
  BeginIO((struct IORequest *) io);
}

void AUDIO_pervol(struct IOAudio *io, LONG volume, LONG period)
{
  io->ioa_Request.io_Command = ADCMD_PERVOL;
  io->ioa_Request.io_Flags = IOF_QUICK; // no reply is sent
  io->ioa_Volume = volume;
  io->ioa_Period = period;
  DoIO((struct IORequest *) io);
}

void AUDIO_silence(struct IOAudio *io)
{
  AbortIO((struct IORequest *) io);
}

void AUDIO_waitio(struct MsgPort *port)
{
  Wait(1 << port->mp_SigBit);
  while (! GetMsg(port)) { }
}

enum audio_action
{
  audio_idle = 0,
  audio_draw_corners,
  audio_andrei,
  audio_gameover
};

struct Task *AUDIO_task;
volatile bool AUDIO_running, AUDIO_stopped, AUDIO_interrupted, AUDIO_ready, AUDIO_started;
volatile enum audio_action AUDIO_action;
struct State *AUDIO_state;
volatile ULONG AUDIO_counter;

volatile bool AUDIO_draw_corners_on;
volatile int AUDIO_draw_corners_ranks;
volatile int AUDIO_draw_corners_suits;

const LONG player_hz[4] = { 45, 48, 40, 50 };

BYTE AUDIO_gameover_player;
tokamak_commit AUDIO_gameover_commit;

BYTE AUDIO_volume;
void AUDIO_music(struct AUDIO **audio)
{
  if (AUDIO_action == audio_gameover)
  {
    --AUDIO_volume;
    if (AUDIO_volume < 0)
    {
      AUDIO_volume = 0;
    }
  }
  else
  {
    AUDIO_volume = 64;
  }
  struct State *s = AUDIO_state;
  BYTE player = tokamak_get_player(&s->T);
  LONG sonify_length = waveform_block * tokamak_get_suits(&s->T) * tokamak_get_ranks(&s->T);
  LONG sonify_speed = audio[0]->clock / (player_hz[player & 3] * sonify_length);
  BYTE valid = s->T.move_is_valid_known && s->T.move_is_valid;
  BYTE changed = ! (s->T.move_is_unchanged_known && s->T.move_is_unchanged);
  tokamak_table T = s->T.table[1];
  BYTE hand_njokers = T.hand_njokers;
  BYTE hand_regular = tokamak_cardset_size(&s->T, T.hand_regular);
  LONG detune = hand_njokers + hand_regular + ! valid;
  BYTE octave = 0;
  if (valid)
  {
    if (changed)
    {
      octave = 0;
    }
    else
    {
      octave = 1;
    }
  }
  else
  {
    if (changed)
    {
      octave = 0;
    }
    else
    {
      octave = 3; // impossible?
    }
  }
  LONG speed = sonify_speed >> octave;
  WaitTOF();
  ++AUDIO_counter;
  AUDIO_pervol(audio[0]->io[1], AUDIO_volume, speed + ((detune + 0) >> 1));
  AUDIO_pervol(audio[1]->io[1], AUDIO_volume, speed - ((detune + 1) >> 1));
  AUDIO_pervol(audio[2]->io[1], AUDIO_volume, sonify_speed);
  AUDIO_pervol(audio[3]->io[1], AUDIO_volume, sonify_speed);
}

void AUDIO_main(void)
{
  struct State *s = AUDIO_state;
  bool isPAL = true; // FIXME get from gfxbase
  int blocksize = 2; // FIXME change this placeholder later
  int samplerate = 22050; // FIXME change this placeholder later
  struct AUDIO *audio[5] = {0, 0, 0, 0, 0};
  BYTE *data[2][4];
  if ((audio[4] = AUDIO_new(isPAL, samplerate, blocksize, 0, 0)) &&
      (audio[0] = AUDIO_new(isPAL, samplerate, blocksize, audio[4], 0)) &&
      (audio[1] = AUDIO_new(isPAL, samplerate, blocksize, audio[4], 1)) &&
      (audio[2] = AUDIO_new(isPAL, samplerate, blocksize, audio[4], 2)) &&
      (audio[3] = AUDIO_new(isPAL, samplerate, blocksize, audio[4], 3)) )
  {

    // store data pointers
    for (int w = 0; w < 2; ++w)
    {
      for (int a = 0; a < 4; ++a)
      {
        data[w][a] = (BYTE *) audio[a]->io[w]->ioa_Data;
      }
    }
    LONG sonify_length = waveform_block * tokamak_get_ranks(&s->T) * tokamak_get_suits(&s->T);
    BYTE player = tokamak_get_player(&s->T);
    LONG sonify_speed = audio[0]->clock / (player_hz[player & 3] * sonify_length);
    while (AUDIO_running)
    {
      switch (AUDIO_action)
      {

        case audio_idle:
        {
          AUDIO_music(audio);
          break;
        }

        case audio_draw_corners:
        {
          AUDIO_ready = false;
          AUDIO_started = true;
          bool on = AUDIO_draw_corners_on;
          int ranks = AUDIO_draw_corners_ranks;
          int suits = AUDIO_draw_corners_suits;
          if (on)
          {
            WaitTOF();
            ++AUDIO_counter;
            // clear
          }
          else
          {
            AUDIO_silence(audio[0]->io[0]);
            AUDIO_silence(audio[1]->io[0]);
            AUDIO_silence(audio[2]->io[0]);
            AUDIO_silence(audio[3]->io[0]);
          }
          int k = on ? 0 : (suits + 1) * (ranks + 1);
          for (int suit = 0; suit <= suits && AUDIO_running && ! AUDIO_interrupted; ++suit)
          {
            for (int rank = 0; rank <= ranks && AUDIO_running && ! AUDIO_interrupted; ++rank)
            {
              WaitTOF();
              ++AUDIO_counter;
              LONG corner_speed = audio[0]->clock / (1000 + k * 1000 / ((suits + 1) * (ranks + 1)));
              AUDIO_oneshot(audio[0]->io[0], corner_data, corner_length, 64, corner_speed);
              AUDIO_waitio(audio[0]->port[0]);
              if (on) k++; else k--;
            }
          }
          if (on)
          {
            AUDIO_loop(audio[0]->io[0], SONIFY_rank_data, SONIFY_rank_length, 64, sonify_speed);
            AUDIO_loop(audio[1]->io[0], SONIFY_suit_data, SONIFY_suit_length, 64, sonify_speed);
            AUDIO_loop(audio[2]->io[0], SONIFY_rank_data, SONIFY_rank_length, 64, sonify_speed - 1);
            AUDIO_loop(audio[3]->io[0], SONIFY_suit_data, SONIFY_suit_length, 64, sonify_speed + 1);
          }
          else
          {
            WaitTOF();
            ++AUDIO_counter;
            // clear
          }
          AUDIO_ready = true;
          AUDIO_action = audio_idle;
          break;
        }

        case audio_andrei:
        {
          AUDIO_ready = false;
          AUDIO_started = true;
          while (AUDIO_running && ! AUDIO_interrupted)
          {
            AUDIO_music(audio);
          }
          AUDIO_ready = true;
          AUDIO_action = audio_idle;
          break;
        }

        case audio_gameover:
        {
          AUDIO_ready = false;
          AUDIO_started = true;
          while (AUDIO_running && ! AUDIO_interrupted && AUDIO_volume > 0)
          {
            AUDIO_music(audio);
          }
          AUDIO_ready = true;
          AUDIO_action = audio_idle;
          break;
        }

      }
    }
    // restore original data pointers before deleting!
    for (int w = 0; w < 2; ++w)
    {
      for (int a = 0; a < 4; ++a)
      {
        audio[a]->io[w]->ioa_Data = (UBYTE *) data[w][a];
      }
    }
  }
  if (audio[0]) AUDIO_delete(audio[0]);
  if (audio[1]) AUDIO_delete(audio[1]);
  if (audio[2]) AUDIO_delete(audio[2]);
  if (audio[3]) AUDIO_delete(audio[3]);
  if (audio[4]) AUDIO_delete(audio[4]);
  AUDIO_stopped = true;
}

bool AUDIO_start(struct State *state)
{
  AUDIO_state = state;
  AUDIO_running = true;
  AUDIO_interrupted = false;
  AUDIO_stopped = false;
  AUDIO_ready = true;
  AUDIO_started = false;
  AUDIO_action = audio_idle;
  AUDIO_counter = 0;
  SONIFY_rank_data = AllocMem(4096, MEMF_CHIP | MEMF_CLEAR);
  if (! SONIFY_rank_data)
  {
    return false;
  }
  SONIFY_suit_data = SONIFY_rank_data + 2048;
  if ((AUDIO_task = CreateTask((CONST_STRPTR) "Tokamak/audio", AUDIO_PRIORITY, (void *) (ULONG) AUDIO_main, 4000L)))
  {
    return true;
  }
  return false;
}

void AUDIO_stop(void)
{
  AUDIO_running = false;
  while (! AUDIO_stopped)
  {
    WaitTOF();
  }
  FreeMem(SONIFY_rank_data, 4096);
}

bool AUDIO_is_running(void)
{
  return ! AUDIO_ready;
}

void AUDIO_interrupt(void)
{
  AUDIO_interrupted = true;
}

void AUDIO_draw_corners(bool on, int ranks, int suits)
{
  AUDIO_draw_corners_on = on;
  AUDIO_draw_corners_ranks = ranks;
  AUDIO_draw_corners_suits = suits;
  AUDIO_interrupted = false;
  AUDIO_started = false;
  AUDIO_action = audio_draw_corners;
  while (! AUDIO_started)
  {
    WaitTOF();
  }
}

void AUDIO_andrei(bool on)
{
  if (on)
  {
    AUDIO_interrupted = false;
    AUDIO_started = false;
    AUDIO_action = audio_andrei;
    while (! AUDIO_started)
    {
      WaitTOF();
    }
  }
  else
  {
    AUDIO_interrupt();
  }
}

void AUDIO_gameover(int player, tokamak_commit commit)
{
  AUDIO_gameover_player = player;
  AUDIO_gameover_commit = commit;
  AUDIO_interrupted = false;
  AUDIO_started = false;
  AUDIO_action = audio_gameover;
  while (! AUDIO_started)
  {
    WaitTOF();
  }
}
