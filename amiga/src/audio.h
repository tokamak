#pragma once

#include <stdbool.h>

#include <exec/types.h>

#include "../../libtokamak.h"

struct AudioState
{
  BYTE tick;
};

struct State;
bool AUDIO_start(struct State *state);
void AUDIO_stop(void);
bool AUDIO_is_running(void);
void AUDIO_interrupt(void);
void AUDIO_draw_corners(bool on, int ranks, int suits);
void AUDIO_andrei(bool on);
void AUDIO_gameover(int player, tokamak_commit commit);

bool SONIFY_start(struct State *state);
void SONIFY_stop(void);
