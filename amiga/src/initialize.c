#include <stdio.h>
#include <string.h>

#include <exec/types.h>
#include <graphics/gels.h>
#include <graphics/gfxmacros.h>
#include <intuition/intuition.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include "bitmap.h"
#include "machine.h"
#include "version.h"

void cleanup(struct State *s)
{
  const int width = 320;
  const int height = 256;
  const int depth = 1;
  if (s)
  {
    if (s->window) CloseWindow(s->window);
    if (s->screen) CloseScreen(s->screen);
    if (s->bitmap) destroyBitMap(s->bitmap, width, height, depth);
    if (IntuitionBase) CloseLibrary((struct Library *) IntuitionBase);
    if (GfxBase) CloseLibrary((struct Library *) GfxBase);
    FreeMem(s, sizeof(*s));
  }
}

bool poll_ui(struct State *s)
{
  ++s->counter;
  if (SetSignal(0L, 0L) & (1L << s->window->UserPort->mp_SigBit))
  {
    struct IntuiMessage *msg;
    while ((msg = (struct IntuiMessage *) GetMsg(s->window->UserPort)))
    {
      switch (msg->Class)
      {
        case CLOSEWINDOW:
        {
          ReplyMsg((struct Message *) msg);
          return true;
        }
        default:
        {
          // TODO really handle message
          ReplyMsg((struct Message *) msg);
        }
      }
    }
  }
  return false;
}

__chip const UWORD pointer_crosshair[] =
{
    0x0000, 0x0000,     /* reserved, must be NULL */

    0x0000, 0x0380,
    0x0100, 0x0380,
    0x0100, 0x0380,
    0x0100, 0x0380,
    0x0100, 0x0380,

    0x0000, 0x0000,

    0x0000, 0xF83E,
    0x783C, 0xF83E,
    0x0000, 0xF83E,

    0x0000, 0x0000,

    0x0100, 0x0380,
    0x0100, 0x0380,
    0x0100, 0x0380,
    0x0100, 0x0380,
    0x0000, 0x0380,

    0x0000, 0x0000,

    0x0000, 0x0000,     /* reserved, must be NULL */
};

struct State *initialize(BPTR out, ULONG ctrlc)
{
  const int width = 320;
  const int height = 256;
  const int depth = 1;

  struct NewScreen screen_spec =
    { 0, 0, width, height, depth, 0, 1, 0
    , CUSTOMSCREEN
    | CUSTOMBITMAP
    | SCREENQUIET
    , 0, 0, 0, 0
    };

  struct NewWindow window_spec =
    { 0, 0, 25, 12
    , 0, 0 // pens
    , CLOSEWINDOW // IDCMP
    , WINDOWCLOSE // WFLG
    | BORDERLESS
    | RMBTRAP
    , 0, 0, 0, 0, 0
    , 0, 0, width, height - 1 // min/max sizes
    , CUSTOMSCREEN
    };

  struct State *s;
  if ((s = AllocMem(sizeof(*s), MEMF_CLEAR)))
  if ((GfxBase = (struct GfxBase *) OpenLibrary((CONST_STRPTR) "graphics.library", 0L)))
  if ((IntuitionBase = (struct IntuitionBase *) OpenLibrary((CONST_STRPTR) "intuition.library", 0L)))
  if ((screen_spec.CustomBitMap = s->bitmap = createBitMap(width, height, depth)))
  if ((window_spec.Screen = s->screen = OpenScreen(&screen_spec)))
  {
    struct ViewPort *vp = &s->screen->ViewPort;
    struct RastPort *rp = &s->screen->RastPort;
    SetRGB4(vp, 0, 0, 0, 0);
    SetRGB4(vp, 1, 15, 15, 15);
    SetDrMd(rp, COMPLEMENT);
    SetRast(rp, 0);

    if ((s->window = OpenWindow(&window_spec)))
    {
      tokamak_options_default(&s->options);
#if 0
      s->options.ranks = 6;
      s->options.suits = 6;
      s->options.jokers = 6;
      s->options.deal = 5;
      s->options.pile = 4;
#endif
      tokamak_init(&s->T, &s->options);
for (int i = 0; i < 5; ++i) tokamak_shuffle(&s->T);
      SetPointer(s->window, (UWORD *) pointer_crosshair, 16, 16, -8, -7);
      ActivateWindow(s->window);
      SetRast(rp, 0);

      s->out = out;
      s->ctrlc = ctrlc;
      s->counter = 0;
      return s;
    }
  }
  cleanup(s);
  return NULL;
}
