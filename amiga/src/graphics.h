#pragma once

#include <stdbool.h>

#include <exec/types.h>

#include "../../libtokamak.h"

struct GraphicsState
{
  tokamak_table T_new, T_old;
  bool verbose;
};

struct State;
bool GRAPHICS_start(struct State *state);
ULONG GRAPHICS_stop();

bool GRAPHICS_is_running();
void GRAPHICS_draw_corners(bool on, int ranks, int suits);
void GRAPHICS_andrei(bool on);
void GRAPHICS_gameover(int player, tokamak_commit commit);
