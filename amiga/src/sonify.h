#pragma once

#include <stdbool.h>

#include <exec/types.h>

struct State;

extern ULONG SONIFY_rank_length, SONIFY_suit_length;
extern BYTE *SONIFY_rank_data, *SONIFY_suit_data;

bool SONIFY_start(struct State *s);
void SONIFY_stop(void);
