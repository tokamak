#include <proto/alib.h>
#include <proto/exec.h>
#include <proto/graphics.h>

#include "sonify.h"
#include "machine.h"

#define waveform_block 4
extern const BYTE waveform_13x4_data[4][11][waveform_block * 13 * 4];
extern const BYTE waveform_6x6_data[4][11][waveform_block * 6 * 6];
extern const BYTE waveform_4x4_data[4][11][waveform_block * 4 * 4];
extern const BYTE waveform_index[32];

struct Task *SONIFY_task;
struct State *SONIFY_state;
volatile bool SONIFY_running, SONIFY_stopped;
ULONG SONIFY_counter;

void SONIFY_main(void)
{
  struct State *s = SONIFY_state;
  UWORD src = 0, dst = 0;
  while (SONIFY_running)
  {
    WaitTOF();
    ++SONIFY_counter;
    tokamak_table T = s->T.table[1];
    int suits = s->T.suits;
    int ranks = s->T.ranks;
    SONIFY_rank_length = waveform_block * suits * ranks;
    dst = 0;
    BYTE j = T.hand_njokers;
    if (j > 3) j = 3;
    for (int suit = 0; suit < suits; ++suit)
    {
      for (int rank = 0; rank < ranks; ++rank)
      {
        tokamak_card c = tokamak_card_regular(&s->T, suit, rank);
        BYTE chand = tokamak_cardset_contains(&s->T, T.hand_regular, c);
        BYTE csuitr = tokamak_cardset_contains(&s->T, T.suit_regular, c);
        BYTE crankr = tokamak_cardset_contains(&s->T, T.rank_regular, c);
        BYTE csuitj = tokamak_cardset_contains(&s->T, T.suit_joker, c);
        BYTE crankj = tokamak_cardset_contains(&s->T, T.rank_joker, c);
        BYTE card = (chand << 0) | (csuitr << 1) | (crankr << 2) | (csuitj << 3) | (crankj << 4);
        for (int sample = 0; sample < waveform_block; ++sample)
        {
          BYTE w = waveform_index[card];
          if (w < 0)
          {
            SONIFY_rank_data[dst] = 0;
          }
          else
          {
            if (ranks == 13 && suits == 4)
            {
              SONIFY_rank_data[dst] = waveform_13x4_data[j][w][src];
            }
            else if (ranks == 4 && suits == 4)
            {
              SONIFY_rank_data[dst] = waveform_4x4_data[j][w][src];
            }
            else if (ranks == 6 && suits == 6)
            {
              SONIFY_rank_data[dst] = waveform_6x6_data[j][w][src];
            }
            else
            {
              SONIFY_rank_data[dst] = 0;
            }
          }
          ++src;
          ++dst;
          if (src == SONIFY_rank_length)
          {
            src = 0;
          }
        }
      }
    }
    src += 11;
    if (src >= SONIFY_rank_length)
    {
      src -= SONIFY_rank_length;
    }
    WaitTOF();
    ++SONIFY_counter;
    T = s->T.table[1];
    suits = s->T.suits;
    ranks = s->T.ranks;
    SONIFY_suit_length = waveform_block * suits * ranks;
    dst = 0;
    j = T.hand_njokers;
    if (j > 3) j = 3;
    for (int rank = 0; rank < ranks; ++rank)
    {
      for (int suit = 0; suit < suits; ++suit)
      {
        tokamak_card c = tokamak_card_regular(&s->T, suit, rank);
        BYTE chand = tokamak_cardset_contains(&s->T, T.hand_regular, c);
        BYTE csuitr = tokamak_cardset_contains(&s->T, T.suit_regular, c);
        BYTE crankr = tokamak_cardset_contains(&s->T, T.rank_regular, c);
        BYTE csuitj = tokamak_cardset_contains(&s->T, T.suit_joker, c);
        BYTE crankj = tokamak_cardset_contains(&s->T, T.rank_joker, c);
        BYTE card = (chand << 0) | (csuitr << 1) | (crankr << 2) | (csuitj << 3) | (crankj << 4);
        for (int sample = 0; sample < waveform_block; ++sample)
        {
          BYTE w = waveform_index[card];
          if (w < 0)
          {
            SONIFY_suit_data[dst] = 0;
          }
          else
          {
            if (ranks == 13 && suits == 4)
            {
              SONIFY_rank_data[dst] = waveform_13x4_data[j][w][src];
            }
            else if (ranks == 4 && suits == 4)
            {
              SONIFY_rank_data[dst] = waveform_4x4_data[j][w][src];
            }
            else if (ranks == 6 && suits == 6)
            {
              SONIFY_rank_data[dst] = waveform_6x6_data[j][w][src];
            }
            else
            {
              SONIFY_rank_data[dst] = 0;
            }
          }
          ++src;
          ++dst;
          if (src == SONIFY_suit_length)
          {
            src = 0;
          }
        }
      }
    }
    src += SONIFY_suit_length - 11;
    if (src >= SONIFY_suit_length)
    {
      src -= SONIFY_suit_length;
    }
  }
  SONIFY_stopped = true;
}

bool SONIFY_start(struct State *s)
{
  SONIFY_state = s;
  SONIFY_stopped = false;
  SONIFY_running = true;
  SONIFY_counter = 0;
  if ((SONIFY_task = CreateTask((CONST_STRPTR) "Tokamak/sonify", SONIFY_PRIORITY, (void *) (ULONG) SONIFY_main, 4000L)))
  {
    return true;
  }
  return false;
}

void SONIFY_stop(void)
{
  SONIFY_running = false;
  while (! SONIFY_stopped)
  {
    WaitTOF();
  }
}
