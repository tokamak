#include <proto/exec.h>
#include <proto/graphics.h>

#include "bitmap.h"

void destroyBitMap(struct BitMap *bitmap, int width, int height, int depth)
{
  if (bitmap)
  {
    for (WORD planeNum = 0; planeNum < depth; planeNum++)
    {
      if (bitmap->Planes[planeNum])
      {
        FreeRaster(bitmap->Planes[planeNum], width, height);
      }
    }
    FreeMem(bitmap, sizeof(*bitmap));
  }
}

struct BitMap *createBitMap(int width, int height, int depth)
{
  struct BitMap *bitmap;
  if ((bitmap = (struct BitMap *) AllocMem(sizeof(*bitmap), MEMF_PUBLIC | MEMF_CLEAR)))
  {
    InitBitMap(bitmap, depth, width, height);
    int allocatedBitMaps = 1;
    for (WORD planeNum = 0; planeNum < depth && allocatedBitMaps; planeNum++)
    {
      if (! (bitmap->Planes[planeNum] = AllocRaster(width, height)))
      {
        allocatedBitMaps = 0;
      }
    }
    if (allocatedBitMaps)
    {
      return bitmap;
    }
    destroyBitMap(bitmap, width, height, depth);
  }
  return NULL;
}
