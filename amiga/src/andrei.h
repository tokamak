#pragma once

#include <stdbool.h>

#include "../../libtokamak.h"

struct State;

bool ANDREI_start(struct State *state);
bool ANDREI_is_running(void);
void ANDREI_run(void);
void ANDREI_interrupt(void);
void ANDREI_stop(void);
void ANDREI_visit(void *arg, const tokamak *T);
