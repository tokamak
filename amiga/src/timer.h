#pragma once

#include <stdbool.h>

bool TIMER_start(void);
ULONG TIMER_stop(void);
