#include <proto/alib.h>
#include <proto/exec.h>
#include <proto/graphics.h>

#include "machine.h"

struct Task *ANDREI_task;
volatile bool ANDREI_running, ANDREI_stopped;
volatile bool ANDREI_started, ANDREI_has_started, ANDREI_ready;
volatile bool ANDREI_deadline;
struct State *ANDREI_state;

void ANDREI_main(void)
{
  struct State *s = ANDREI_state;
  struct Library *GfxBase;
  if ((GfxBase = OpenLibrary((CONST_STRPTR) "graphics.library", 0L)))
  {
    while (ANDREI_running)
    {
      while (ANDREI_running && ! ANDREI_started)
      {
        WaitTOF();
      }
      if (ANDREI_started)
      {
        ANDREI_ready = false;
        ANDREI_started = false;
        ANDREI_deadline = false;
        ANDREI_has_started = true;
        if (ANDREI_running)
        {
          tokamak_ai_andrei(&s->T, &ANDREI_deadline);
//          tokamak_ai_quantum(&s->T, &ANDREI_deadline);
//          tokamak_ai_brute_search_serial(&s->T, &ANDREI_deadline);
        }
        ANDREI_ready = true;
      }
    }
    CloseLibrary(GfxBase);
  }
  ANDREI_stopped = true;
}

bool ANDREI_start(struct State *state)
{
  ANDREI_running = true;
  ANDREI_stopped = false;
  ANDREI_ready = true;
  ANDREI_state = state;
  if ((ANDREI_task = CreateTask((CONST_STRPTR) "Tokamak/andrei", ANDREI_PRIORITY, (void *) (ULONG) ANDREI_main, 102400L)))
  {
    return true;
  }
  return false;
}

bool ANDREI_is_running(void)
{
  return ! ANDREI_ready;
}

void ANDREI_interrupt(void)
{
  ANDREI_deadline = true;
}

void ANDREI_run(void)
{
  ANDREI_interrupt();
  while (ANDREI_is_running())
  {
    WaitTOF();
  }
  ANDREI_has_started = false;
  ANDREI_started = true;
  while (! ANDREI_has_started)
  {
    WaitTOF();
  }
}

void ANDREI_stop(void)
{
  ANDREI_interrupt();
  ANDREI_running = false;
  while (! ANDREI_stopped) { WaitTOF(); }
}
