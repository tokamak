#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>

#include "andrei.h"
#include "graphics.h"
#include "machine.h"

bool ENGINE_wait(struct State *s)
{
  while (GRAPHICS_is_running())
  {
    WaitTOF();
    if (poll_ui(s))
    {
      return true;
    }
  }
  while (AUDIO_is_running())
  {
    WaitTOF();
    if (poll_ui(s))
    {
      return true;
    }
  }
  return false;
}

bool ENGINE_draw_corners(struct State *s, bool on)
{
  GRAPHICS_draw_corners(on, s->T.ranks, s->T.suits);
  AUDIO_draw_corners(on, s->T.ranks, s->T.suits);
  return ENGINE_wait(s);
}

tokamak_commit ENGINE_andrei(struct State *s)
{
  GRAPHICS_andrei(true);
  AUDIO_andrei(true);
  ANDREI_run();
  int counter = 0;
  while (ANDREI_is_running())
  {
    WaitTOF();
    ++counter;
    if (counter >= 500)
    {
      ANDREI_interrupt();
    }
    if (poll_ui(s))
    {
      return tokamak_commit_invalid;
    }
  }
  for (int i = 0; i < 2; ++i)
  {
    tokamak_commit commit;
    switch ((commit = tokamak_move_commit(&s->T)))
    {
      case tokamak_commit_ok:
      case tokamak_commit_stalemate:
      case tokamak_commit_winner:
      {
        GRAPHICS_andrei(false);
        AUDIO_andrei(false);
        if (ENGINE_wait(s))
        {
          return tokamak_commit_invalid;
        }
        return commit;
      }
      default:
      case tokamak_commit_invalid:
      {
        tokamak_move_rollback(&s->T);
        break;
      }
    }
  }
  return tokamak_commit_invalid;
}

bool ENGINE_gameover(struct State *s, int player, tokamak_commit commit)
{
  GRAPHICS_gameover(player, commit);
  AUDIO_gameover(player, commit);
  return ENGINE_wait(s);
}

bool ENGINE_gameplay(struct State *s)
{
  tokamak_init(&s->T, &s->options);
  if (ENGINE_draw_corners(s, true))
  {
    return true;
  }
  tokamak_shuffle(&s->T);
  tokamak_deal(&s->T);
  tokamak_commit commit = tokamak_commit_ok;
  while (commit == tokamak_commit_ok)
  {
    for (int i = 0; i < 25; ++i)
    {
      WaitTOF();
    }
    int player = tokamak_get_player(&s->T);
    // player type andrei
    switch ((commit = ENGINE_andrei(s)))
    {
      case tokamak_commit_invalid:
        return true;
      case tokamak_commit_ok:
        continue;
      case tokamak_commit_stalemate:
      case tokamak_commit_winner:
        if (ENGINE_gameover(s, player, commit))
        {
          return true;
        }
    }
  }
  return ENGINE_draw_corners(s, false);
}

void ENGINE_main(struct State *s)
{
  while (! ENGINE_gameplay(s))
  {
    for (int i = 0; i < 25; ++i)
    {
      WaitTOF();
    }
  }
}
