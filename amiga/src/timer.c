#include <proto/alib.h>
#include <proto/exec.h>
#include <proto/graphics.h>

#include "machine.h"
#include "timer.h"

struct Task *TIMER_task;
volatile bool TIMER_running, TIMER_stopped;
volatile ULONG TIMER_counter;

void TIMER_main(void)
{
  struct Library *GfxBase;
  if ((GfxBase = OpenLibrary((CONST_STRPTR) "graphics.library", 0L)))
  {
    while (TIMER_running)
    {
      WaitTOF();
      ++TIMER_counter;
    }
    CloseLibrary(GfxBase);
  }
  TIMER_stopped = true;
}

bool TIMER_start(void)
{
  TIMER_running = true;
  TIMER_stopped = false;
  TIMER_counter = 0;
  if ((TIMER_task = CreateTask((CONST_STRPTR) "Tokamak/timer", TIMER_PRIORITY, (void *) (ULONG) TIMER_main, 4000L)))
  {
    return true;
  }
  return false;
}

ULONG TIMER_stop(void)
{
  TIMER_running = false;
  while (! TIMER_stopped)
  {
    WaitTOF();
  }
  return TIMER_counter;
}
