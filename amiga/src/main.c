#include <dos/dos.h>
#include <exec/types.h>
#include <proto/dos.h>
#include <workbench/startup.h>

#include "andrei.h"
#include "audio.h"
#include "graphics.h"
#include "machine.h"
#include "timer.h"
#include "version.h"

int main1(BPTR out, ULONG ctrlc)
{
  int retval = 20;
  struct State *s = NULL;
  ULONG graphics = 0, timer = 0;
  if ((s = initialize(out, ctrlc)))
  {
    if (TIMER_start())
    {
      if (AUDIO_start(s))
      {
        if (SONIFY_start(s))
        {
          if ((GRAPHICS_start(s)))
          {
            if (ANDREI_start(s))
            {
              ENGINE_main(s);
              ANDREI_stop();
            }
            graphics = GRAPHICS_stop();
          }
          SONIFY_stop();
        }
        AUDIO_stop();
      }
      timer = TIMER_stop();
    }
    // print number of dropped frames
    ULONG counter = timer - graphics;
    char number[16];
    number[15] = 0;
    number[14] = '/';
    int m = 0;
    for (int n = 13; n >= 0; --n)
    {
      if (counter)
      {
        number[n] = '0' + (counter % 10);
        counter /= 10;
        m = n;
      }
      else
      {
        break;
      }
    }
    Write(s->out, "Dropped frames: ", 16);
    Write(s->out, number + m, 15 - m);
    counter = timer;
    number[14] = '\n';
    m = 0;
    for (int n = 13; n >= 0; --n)
    {
      if (counter)
      {
        number[n] = '0' + (counter % 10);
        counter /= 10;
        m = n;
      }
      else
      {
        break;
      }
    }
    Write(s->out, number + m, 15 - m);
    cleanup(s);
    retval = 0;
  }
  return retval;
}

extern int main(int argc, char **argv)
{
  BPTR out = NULL;
  int retval = 20;
  if (argc == 0)
  {
    struct WBStartup *wb;
    if ((wb = (struct WBStartup *) argv))
    {
      if ((out = Open((CONST_STRPTR) "CON:", MODE_NEWFILE)))
      {
        retval = main1(out, SIGBREAKF_CTRL_C);
        Close(out);
      }
      else
      {
        /* what do? */
      }
    }
    else
    {
      BPTR out;
      if ((out = Open((CONST_STRPTR) "CON:", MODE_NEWFILE)))
      {
        Write(out, "workbench error\n", 16);
        Close(out);
      }
    }
  }
  else
  {
    retval = main1(Output(), SIGBREAKF_CTRL_C);
  }
  return retval;
}
