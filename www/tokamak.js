function tokamak_cardset_from_strings(shi, slo)
{
  return { hi: parseInt(shi), lo: parseInt(slo) };
}

function tokamak_cardset_empty()
{
  return { hi: 0, lo: 0 };
}

function tokamak_get_ranks()
{
  return document.getElementById('ranks').value;
}

function tokamak_get_suits()
{
  return document.getElementById('suits').value;
}

function tokamak_get_jokers()
{
  return document.getElementById('jokers').value
}

function tokamak_get_regular()
{
  return tokamak_get_suits() * tokamak_get_ranks();
}

function tokamak_get_cards()
{
  return tokamak_get_regular() + tokamak_get_jokers();
}

function tokamak_card_regular(suit, rank)
{
  if (! (0 <= suit && suit < tokamak_get_suits() &&
         0 <= rank && rank < tokamak_get_ranks()))
  {
    return ~0;
  }
  else
  {
    return rank + tokamak_get_ranks() * suit;
  }
}

function tokamak_card_is_valid(card)
{
  return 0 <= card && card < tokamak_get_cards();
}

function tokamak_card_is_regular(card)
{
  return 0 <= card && card < tokamak_get_regular();
}

function tokamak_card_is_joker(card0)
{
  var card = card0 - tokamak_get_regular();
  return 0 <= card && card < tokamak_get_jokers();
}

function tokamak_card_get_rank(card)
{
  if (tokamak_card_is_regular(card))
  {
    return card % tokamak_get_ranks();
  }
  else
  {
    return -1;
  }
}

function tokamak_card_get_suit(card)
{
  if (tokamak_card_is_regular(card))
  {
    return Math.floor(card / tokamak_get_ranks());
  }
  else
  {
    return -1;
  }
}

function tokamak_cardset_singleton(card)
{
  if (! tokamak_card_is_valid(card))
  {
    return tokamak_cardset_empty();
  }
  else
  {
    return card >= 32 ? { hi: 1 << (card - 32), lo: 0 } : { hi: 0, lo: 1 << card };
  }
}

function tokamak_cardset_contains_card(cardset, card)
{
  if (card >= 32)
  {
    return !!(cardset.hi & (1 << (card - 32)))
  }
  else
  {
    return !!(cardset.lo & (1 << card));
  }
}

function tokamak_card_name(suit, rank)
{
  var suit_name = 'BCDEFGHILMNOPRSTUVWXYZbcdefghilmnoprstuvwxyz'[suit];
  var rank_name = '' + (1 + rank);
  return rank_name + suit_name;
}

var tokamak_joker_name = '*';

var tokamak_id = 0;
var tokamak_host = false;
var tokamak_playing = false;
var tokamak_state = 'unknown';
var tokamak_hand_regular = tokamak_cardset_empty();
var tokamak_hand_jokers = 0;

function tokamak_connect()
{
  var reconnect = true;
  var wsproto = (location.protocol === 'https:') ? 'wss:' : 'ws:';
  var connection = new WebSocket(wsproto + '//' + location.host + '/ws');
  var send = function(str) {
    console.log('tokamak: send: ' + str);
    connection.send(str);
  };
  connection.onmessage = function (msg) {
    var arg = msg.data.split('\r\n')[0].split(' ');
    console.log(arg);
    if (arg[0] == 'Ready:' && arg.length == 2 && arg[1] == '1')
    {
      document.getElementById('status').setAttribute('class', 'connecting');
      send('Connect: ' + tokamak_room + ' ' + tokamak_name + ' ' + tokamak_auth + '\r\n');
    }
    else if (arg[0] == 'Connected:' && arg.length == 2 && arg[1] == '0')
    {
      reconnect = false;
      connection.close();
    }
    else if (arg[0] == 'Connected:' && arg.length == 2 && arg[1] == '1')
    {
      document.getElementById('status').setAttribute('class', 'connected');
    }
    else if (arg[0] == 'Join:' && arg.length == 4)
    {
      var name = decodeURIComponent(arg[1]);
      var me = tokamak_name == arg[1];
      var host = arg[2] == '1';
      if (me)
      {
        tokamak_host = host;
      }
      var playing = arg[3] == '1';
      if (me)
      {
        tokamak_playing = playing;
      }
      var players = document.getElementById('players').children;
      var found = false;
      for (var p = 0; p < players.length; ++p)
      {
        var player_name = players[p].children[1].innerText;
        if (player_name == name)
        {
          players[p].classList.remove('disconnected');
          found = true;
        }
        if (tokamak_host)
        {
          var input = players[p].children[0];
          input.disabled = tokamak_state == '2';
          var f = function (pname) {
            input.oninput = function (e) {
              send('Play: ' + (e.target.checked ? 1 : 0) + ' ' + encodeURIComponent(pname) + '\r\n');
            };
          };
          f(player_name);
        }
      }
      if (! found)
      {
        var li = document.createElement('li');
        if (host)
        {
          li.classList.add('host');
        }
        if (me)
        {
          li.classList.add('me');
        }
        var label = document.createElement('label');
        var id = 'player' + tokamak_id;
        tokamak_id++;
        label.setAttribute('for', id);
        label.innerText = name;
        var input = document.createElement('input');
        input.setAttribute('type', 'checkbox');
        input.setAttribute('id', id);
        input.setAttribute('name', id);
        if (playing)
        {
          input.checked = true;
          if (me)
          {
            tokamak_playing = true;
          }
        }
        if (me)
        {
          input.oninput = function (e) {
            tokamak_playing = e.target.checked;
            send('Play: ' + (tokamak_playing ? 1 : 0) + '\r\n');
          };
          input.disabled = tokamak_state == '2';
        }
        else
        {
          input.disabled = ! tokamak_host || tokamak_state == '2';
          if (tokamak_host)
          {
            input.oninput = function (e) {
              send('Play: ' + (e.target.checked ? 1 : 0) + ' ' + encodeURIComponent(name) + '\r\n');
            };
          }
        }
        li.appendChild(input);
        li.appendChild(label);
        document.getElementById('players').appendChild(li);
      }
    }
    else if (arg[0] == 'Part:' && arg.length == 2)
    {
      var name = decodeURIComponent(arg[1]);
      var players = document.getElementById('players').children;
      for (var p = 0; p < players.length; ++p)
      {
        if (players[p].children[1].innerText == name)
        {
          players[p].classList.add('disconnected');
          break;
        }
      }
    }
    else if (arg[0] == 'Play:' && arg.length == 3)
    {
      var name = decodeURIComponent(arg[1]);
      var checked = arg[2] == '1';
      var players = document.getElementById('players').children;
      for (var p = 0; p < players.length; ++p)
      {
        if (players[p].children[1].innerText == name)
        {
          players[p].children[0].checked = checked;
          break;
        }
      }
      if (arg[1] == tokamak_name)
      {
        tokamak_playing = checked;
      }
    }
    else if (arg[0] == 'Uptime:' && arg.length == 2)
    {
      document.getElementById('uptime').innerText = arg[1];
    }
    else if (arg[0] == 'Ranks:' && arg.length == 2)
    {
      document.getElementById('ranks').value = arg[1];
    }
    else if (arg[0] == 'Suits:' && arg.length == 2)
    {
      document.getElementById('suits').value = arg[1];
    }
    else if (arg[0] == 'Jokers:' && arg.length == 2)
    {
      document.getElementById('jokers').value = arg[1];
    }
    else if (arg[0] == 'Pile:' && arg.length == 2)
    {
      document.getElementById('pile').value = arg[1];
    }
    else if (arg[0] == 'Deal:' && arg.length == 2)
    {
      document.getElementById('deal').value = arg[1];
    }
    else if (arg[0] == 'State:' && arg.length == 2)
    {
      tokamak_state = arg[1];
      document.getElementById('start').disabled = arg[1] != '1';
      document.getElementById('stop').disabled = ! (arg[1] == '2' && (tokamak_host || tokamak_playing));
      document.getElementById('ranks').disabled = arg[1] == '2';
      document.getElementById('suits').disabled = arg[1] == '2';
      document.getElementById('jokers').disabled = arg[1] == '2';
      document.getElementById('pile').disabled = arg[1] == '2';
      document.getElementById('deal').disabled = arg[1] == '2';
      var name = decodeURIComponent(tokamak_name);
      var players = document.getElementById('players').children;
      for (var p = 0; p < players.length; ++p)
      {
        if (players[p].children[1].innerText == name)
        {
          players[p].children[0].disabled = arg[1] == '2';
          break;
        }
      }
    }
    else if (arg[0] == 'Deck:' && arg.length == 2)
    {
      document.getElementById('deck').innerText = 'Deck: ' + arg[1] + ' cards';
      var hands = document.getElementById('hands');
      while (hands.children.length > 0)
      {
        hands.removeChild(hands.children[0]);
      }
    }
    else if (arg[0] == 'HandSize:' && arg.length == 3)
    {
      var hands = document.getElementById('hands');
      var hand = document.createElement('li');
      hand.innerText = arg[2] + ' cards';
      hands.appendChild(hand);
    }
    else if (arg[0] == 'Hand:' && arg.length == 5)
    {
      tokamak_hand_regular = tokamak_cardset_from_strings(arg[2], arg[3]);
      tokamak_hand_jokers = parseInt(arg[4]);
      var hands = document.getElementById('hands');
      var hand = document.createElement('li');
      var text = '';
      var ranks = tokamak_get_ranks();
      var suits = tokamak_get_suits();
      for (var suit = 0; suit < suits; ++suit)
      {
        for (var rank = 0; rank < ranks; ++rank)
        {
          if (tokamak_cardset_contains_card(tokamak_hand_regular, tokamak_card_regular(suit, rank)))
          {
            if (text != '')
            {
              text += ' ';
            }
            text += tokamak_card_name(suit, rank);
          }
        }
      }
      for (var joker = 0; joker < tokamak_hand_jokers; ++joker)
      {
        if (text != '');
        {
          text += ' ';
        }
        text += tokamak_joker_name;
      }
      hand.innerText = text;
      hands.appendChild(hand);

    }
    else if (arg[0] == 'Board:' && arg.length == 10)
    {
      var ranks = tokamak_get_ranks();
      var suits = tokamak_get_suits();
      var mode = parseInt(arg[1]);
      var interactive = arg[1] == 1 || arg[1] == 2 || arg[1] == 3;
      var rolledback = arg[1] == 1;
      var canrollback = arg[1] == 2 || arg[1] == 3;
      var cancommit = arg[1] == 3;
      var gameover = arg[1] == 4;
      var hand_regular = tokamak_hand_regular;
      var hand_jokers = tokamak_hand_jokers;
      var rank_regular = tokamak_cardset_from_strings(arg[2], arg[3]);
      var rank_joker = tokamak_cardset_from_strings(arg[4], arg[5]);
      var suit_regular = tokamak_cardset_from_strings(arg[6], arg[7]);
      var suit_joker = tokamak_cardset_from_strings(arg[8], arg[9]);
      var table = document.createElement('table');
      for (var suit = 0; suit < suits; ++suit)
      {
        var tr0 = document.createElement('tr');
        var tr1 = document.createElement('tr');
        for (var rank = 0; rank < ranks; ++rank)
        {
          var card = tokamak_card_regular(suit, rank);
          var card_name = tokamak_card_name(suit, rank);
          var td00 = document.createElement('td');
          var td01 = document.createElement('td');
          var td10 = document.createElement('td');
          var td11 = document.createElement('td');
          if (tokamak_cardset_contains_card(rank_regular, card))
          {
            if (interactive)
            {
              var input = document.createElement('input');
              input.setAttribute('type', 'button');
              input.setAttribute('value', card_name);
              var f = function(c) {
                input.onclick = function(e) {
                  send('MoveRankRegularToHand: ' + c + '\r\n');
                }
              }
              f(card);
              td11.appendChild(input);
            }
            else
            {
              td11.innerText = card_name;
            }
          }
          if (tokamak_cardset_contains_card(rank_joker, card))
          {
            if (interactive)
            {
              var input = document.createElement('input');
              input.setAttribute('type', 'button');
              input.setAttribute('value', tokamak_joker_name);
              var f = function(c) {
                input.onclick = function(e) {
                  send('MoveRankJokerToHand: ' + c + '\r\n');
                }
              }
              f(card);
              td11.appendChild(input);
            }
            else
            {
              td11.innerText = tokamak_joker_name;
            }
          }
          if (tokamak_cardset_contains_card(suit_regular, card))
          {
            if (interactive)
            {
              var input = document.createElement('input');
              input.setAttribute('type', 'button');
              input.setAttribute('value', card_name);
              var f = function(c) {
                input.onclick = function(e) {
                  send('MoveSuitRegularToHand: ' + c + '\r\n');
                }
              }
              f(card);
              td00.appendChild(input);
            }
            else
            {
              td00.innerText = card_name;
            }
          }
          if (tokamak_cardset_contains_card(suit_joker, card))
          {
            if (interactive)
            {
              var input = document.createElement('input');
              input.setAttribute('type', 'button');
              input.setAttribute('value', tokamak_joker_name);
              var f = function(c) {
                input.onclick = function(e) {
                  send('MoveSuitJokerToHand: ' + c + '\r\n');
                }
              }
              f(card);
              td00.appendChild(input);
            }
            else
            {
              td00.innerText = tokamak_joker_name;
            }
          }
          var innerText = '';
          if (tokamak_cardset_contains_card(tokamak_hand_regular, card))
          {
            if (interactive)
            {
              {
                var input = document.createElement('input');
                input.setAttribute('type', 'button');
                input.setAttribute('value', card_name);
                input.classList.add('rankwise');
                var f = function(c) {
                  input.onclick = function(e) {
                    send('MoveHandToRankRegular: ' + c + '\r\n');
                  }
                }
                f(card);
                td01.appendChild(input);
              }
              {
                var input = document.createElement('input');
                input.setAttribute('type', 'button');
                input.setAttribute('value', card_name);
                input.classList.add('suitwise');
                var f = function(c) {
                  input.onclick = function(e) {
                    send('MoveHandToSuitRegular: ' + c + '\r\n');
                  }
                }
                f(card);
                td01.appendChild(input);
              }
            }
            else
            {
              innerText += card_name;
            }
          }
          if (tokamak_hand_jokers > 0)
          {
            if (interactive)
            {
              {
                var input = document.createElement('input');
                input.setAttribute('type', 'button');
                input.setAttribute('value', tokamak_joker_name);
                input.classList.add('rankwise');
                input.classList.add('joker');
                var f = function(c) {
                  input.onclick = function(e) {
                    send('MoveHandToRankJoker: ' + c + '\r\n');
                  }
                }
                f(card);
                td01.appendChild(input);
              }
              {
                var input = document.createElement('input');
                input.setAttribute('type', 'button');
                input.setAttribute('value', tokamak_joker_name);
                input.classList.add('suitwise');
                input.classList.add('joker');
                var f = function(c) {
                  input.onclick = function(e) {
                    send('MoveHandToSuitJoker: ' + c + '\r\n');
                  }
                }
                f(card);
                td01.appendChild(input);
              }
            }
            else
            {
              if (innerText != '')
              {
                innerText += ' ';
              }
              innerText += tokamak_joker_name;
            }
          }
          if (td00.innerHTML.length > 0)
          {
            td00.classList.add('suitwise');
          }
          if (td11.innerHTML.length > 0)
          {
            td11.classList.add('rankwise');
          }
          tr0.appendChild(td11);
          tr0.appendChild(td10);
          tr1.appendChild(td01);
          tr1.appendChild(td00);
        }
        table.appendChild(tr0);
        table.appendChild(tr1);
      }
      var board = document.getElementById('board');
      while (board.children.length > 0)
      {
        board.removeChild(board.children[0]);
      }
      board.appendChild(table);
      if (interactive)
      {
        if (canrollback)
        {
          var rollback = document.createElement('input');
          rollback.setAttribute('type', 'button');
          rollback.setAttribute('value', 'rollback');
          rollback.onclick = function (e) {
            send('Rollback: 1\r\n');
          };
          board.appendChild(rollback);
        }
        else
        {
          var drawcard = document.createElement('input');
          drawcard.setAttribute('type', 'button');
          drawcard.setAttribute('value', 'draw a card');
          drawcard.onclick = function (e) {
            send('Commit: 1\r\n');
          };
          board.appendChild(drawcard);
        }
        if (cancommit)
        {
          var commit = document.createElement('input');
          commit.setAttribute('type', 'button');
          commit.setAttribute('value', 'commit');
          commit.onclick = function (e) {
            send('Commit: 1\r\n');
          };
          board.appendChild(commit);
        }
      }
    }
    else
    {
      console.log('tokamak: recv: ' + msg.data);
    }
  };
  connection.onerror = function (err) {
    document.getElementById('status').setAttribute('class', 'error');
    connection.close();
  };
  connection.onopen = function () {
    document.getElementById('status').setAttribute('class', 'opening');
  };
  connection.onclose = function () {
    document.getElementById('status').setAttribute('class', 'disconnected');
    if (reconnect)
    {
      setTimeout(tokamak_connect, 1000);
    }
  };
  var inputs = [ 'ranks', 'suits', 'jokers', 'pile', 'deal' ];
  var headers = [ 'Ranks', 'Suits', 'Jokers', 'Pile', 'Deal' ];
  for (var i = 0; i < inputs.length; ++i)
  {
    var f = function (input, header) {
      document.getElementById(input).oninput = function (e) {
        send(header + ': ' + e.target.value + '\r\n');
      };
    };
    f(inputs[i], headers[i]);
  }
  document.getElementById('start').onclick = function (e) {
    send('Start: 1\r\n');
  };
  document.getElementById('stop').onclick = function (e) {
    send('Stop: 1\r\n');
  };
}

function tokamak_load()
{
  document.getElementById('tokamak').innerHTML
    = "<h3 id='status'>status</h3>\r\n"
    + "<p>server uptime: <span id='uptime'>??</span> minutes</p>\r\n"
    + "<h3>options</h3>\r\n"
    + "<form id='options'>\r\n"
    + "<table><tr><td>\r\n"
    + "<label for='ranks'>ranks</label><input type='number' id='ranks' name='ranks' min='2' max='32' value='13'><br>\r\n"
    + "<label for='suits'>suits</label><input type='number' id='suits' name='suits' min='2' max='32' value='4'><br>\r\n"
    + "<label for='jokers'>jokers</label><input type='number' id='jokers' name='jokers' min='0' max='60' value='2'><br>\r\n"
    + "<label for='pile'>pile</label><input type='number' id='pile' name='pile' min='2' max='32' value='3'><br>\r\n"
    + "<label for='deal'>deal</label><input type='number' id='deal' name='deal' min='0' max='32' value='5'><br>\r\n"
    + "</td><td>\r\n"
    + "<ul id='players'>\r\n"
    + "</ul>\r\n"
    + "<input type='button' id='start' name='start' value='start'>\r\n"
    + "<input type='button' id='stop' name='stop' value='stop'>\r\n"
    + "</td></tr></table>\r\n"
    + "</form>\r\n"
    + "<div id='deck'></div>\r\n"
    + "<ol id='hands'></ol>\r\n"
    + "<div id='board'></div>\r\n"
    ;
  tokamak_connect();
}
