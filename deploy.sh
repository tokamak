#!/bin/bash
prefix="/home/tokamak"
bindir="${prefix}/bin"
wwwdir="${prefix}/www"
cp -avft "${bindir}/" tokamak-server-websocket
cp -avft "${wwwdir}/" www/403.html www/404.html www/500.html www/507.html www/index.html www/play.html www/tokamak.css www/tokamak.js www/tokamak.png
